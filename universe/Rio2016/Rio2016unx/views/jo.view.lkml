# View file auto-generated with booker
# 2021-06-09 17:43:56.802026

view: jo {

	sql_table_name: "JO$" ;;

	dimension: code_country {
		label: "Code Country"
		description: ""
		type: string
		sql: ${TABLE}.Code Country ;;
	}
	dimension: country {
		label: "Country"
		description: ""
		type: string
		sql: ${TABLE}.Country ;;
	}
	dimension: medal {
		label: "Medal"
		description: ""
		type: string
		sql: ${TABLE}.Medal ;;
	}
	dimension: sport {
		label: "Sport"
		description: ""
		type: string
		sql: ${TABLE}.Sport ;;
	}
	dimension: event {
		label: "Event"
		description: ""
		type: string
		sql: ${TABLE}.Event ;;
	}
	dimension: gender {
		label: "Gender"
		description: ""
		type: string
		sql: ${TABLE}.Gender ;;
	}
	dimension: athlete {
		label: "Athlete"
		description: ""
		type: string
		sql: ${TABLE}.Name ;;
	}
	dimension: web_link {
		label: "Web Link"
		description: ""
		type: string
		sql: ${TABLE}.Web Link ;;
	}
	dimension: olympic_games {
		label: "Olympic Games"
		description: ""
		type: string
		sql: ${TABLE}.Olympic Games ;;
	}
	dimension: ctry_organizer {
		label: "Ctry Organizer"
		description: ""
		type: string
		sql: ${TABLE}.Ctry Organizer ;;
	}
	dimension: year {
		label: "Year"
		description: ""
		type: number
		sql: ${TABLE}.Year ;;
	}
	dimension: saison {
		label: "Saison"
		description: ""
		type: string
		sql: ${TABLE}.Saison ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F13 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F13 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F14 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F14 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F15 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F15 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F16 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F16 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F17 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F17 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F18 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F18 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: F19 {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.F19 ;;
	}




}
