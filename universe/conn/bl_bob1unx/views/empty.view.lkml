# View file auto-generated with booker
# 2021-06-09 17:43:55.544050# This view contains all the measures and dimensions which could not be mapped to a table. Please review

view: empty {

	sql_table_name: "EMPTY" ;;




	# FIXME: Detected static list of values. Manual review required.
	# FIXME: Cannot convert Static LOV with multiple columns to LookerML
	# FIXME: Columns: ['Color', 'Mood']
	filter: colors {
		label: "colors"
		description: ""
		type: string
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: Cannot convert Static LOV with multiple columns to LookerML
	# FIXME: Columns: ['creature', 'color', 'element', 'affinity']
	filter: moarlov {
		label: "moarlov"
		description: "static values"
		type: string
	}

	parameter: constparamnoprompt {
		label: "constParamNoPrompt"
		description: "Constant parameter without prompt"
		type: number
		allowed_value: {
			value: "1"
		}
		allowed_value: {
			value: "2"
		}
		allowed_value: {
			value: "3"
		}
	}
	parameter: paramwithuserprompt {
		label: "paramWithUserPrompt"
		description: ""
		type: string
	}
}
