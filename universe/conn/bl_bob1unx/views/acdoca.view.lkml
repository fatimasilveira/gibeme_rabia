# View file auto-generated with booker
# 2021-06-09 17:43:54.921507

view: acdoca {

	sql_table_name: "ACDOCA" ;;

	dimension: rclnt {
		label: "Rclnt"
		description: ""
		type: string
		sql: ${TABLE}.RCLNT ;;
	}
	dimension: rldnr {
		label: "Rldnr"
		description: ""
		type: string
		sql: ${TABLE}.RLDNR ;;
	}
	dimension: rbukrs {
		label: "Rbukrs"
		description: ""
		type: string
		sql: ${TABLE}.RBUKRS ;;
	}
	dimension: gjahr {
		label: "Gjahr"
		description: ""
		type: string
		sql: ${TABLE}.GJAHR ;;
	}
	dimension: belnr {
		label: "Belnr"
		description: ""
		type: string
		sql: ${TABLE}.BELNR ;;
	}
	dimension: docln {
		label: "Docln"
		description: ""
		type: string
		sql: ${TABLE}.DOCLN ;;
	}
	dimension: ryear {
		label: "Ryear"
		description: ""
		type: string
		sql: ${TABLE}.RYEAR ;;
	}
	dimension: docnr_ld {
		label: "Docnr Ld"
		description: ""
		type: string
		sql: ${TABLE}.DOCNR_LD ;;
	}
	dimension: rrcty {
		label: "Rrcty"
		description: ""
		type: string
		sql: ${TABLE}.RRCTY ;;
	}
	dimension: rmvct {
		label: "Rmvct"
		description: ""
		type: string
		sql: ${TABLE}.RMVCT ;;
	}
	dimension: vorgn {
		label: "Vorgn"
		description: ""
		type: string
		sql: ${TABLE}.VORGN ;;
	}
	dimension: vrgng {
		label: "Vrgng"
		description: ""
		type: string
		sql: ${TABLE}.VRGNG ;;
	}
	dimension: bttype {
		label: "Bttype"
		description: ""
		type: string
		sql: ${TABLE}.BTTYPE ;;
	}
	dimension: awtyp {
		label: "Awtyp"
		description: ""
		type: string
		sql: ${TABLE}.AWTYP ;;
	}
	dimension: awsys {
		label: "Awsys"
		description: ""
		type: string
		sql: ${TABLE}.AWSYS ;;
	}
	dimension: aworg {
		label: "Aworg"
		description: ""
		type: string
		sql: ${TABLE}.AWORG ;;
	}
	dimension: awref {
		label: "Awref"
		description: ""
		type: string
		sql: ${TABLE}.AWREF ;;
	}
	dimension: awitem {
		label: "Awitem"
		description: ""
		type: string
		sql: ${TABLE}.AWITEM ;;
	}
	dimension: awitgrp {
		label: "Awitgrp"
		description: ""
		type: string
		sql: ${TABLE}.AWITGRP ;;
	}
	dimension: subta {
		label: "Subta"
		description: ""
		type: string
		sql: ${TABLE}.SUBTA ;;
	}
	dimension: xreversing {
		label: "Xreversing"
		description: ""
		type: string
		sql: ${TABLE}.XREVERSING ;;
	}
	dimension: xreversed {
		label: "Xreversed"
		description: ""
		type: string
		sql: ${TABLE}.XREVERSED ;;
	}
	dimension: xtruerev {
		label: "Xtruerev"
		description: ""
		type: string
		sql: ${TABLE}.XTRUEREV ;;
	}
	dimension: awtyp_rev {
		label: "Awtyp Rev"
		description: ""
		type: string
		sql: ${TABLE}.AWTYP_REV ;;
	}
	dimension: aworg_rev {
		label: "Aworg Rev"
		description: ""
		type: string
		sql: ${TABLE}.AWORG_REV ;;
	}
	dimension: awref_rev {
		label: "Awref Rev"
		description: ""
		type: string
		sql: ${TABLE}.AWREF_REV ;;
	}
	dimension: subta_rev {
		label: "Subta Rev"
		description: ""
		type: string
		sql: ${TABLE}.SUBTA_REV ;;
	}
	dimension: xsettling {
		label: "Xsettling"
		description: ""
		type: string
		sql: ${TABLE}.XSETTLING ;;
	}
	dimension: xsettled {
		label: "Xsettled"
		description: ""
		type: string
		sql: ${TABLE}.XSETTLED ;;
	}
	dimension: prec_awtyp {
		label: "Prec Awtyp"
		description: ""
		type: string
		sql: ${TABLE}.PREC_AWTYP ;;
	}
	dimension: prec_awsys {
		label: "Prec Awsys"
		description: ""
		type: string
		sql: ${TABLE}.PREC_AWSYS ;;
	}
	dimension: prec_aworg {
		label: "Prec Aworg"
		description: ""
		type: string
		sql: ${TABLE}.PREC_AWORG ;;
	}
	dimension: prec_awref {
		label: "Prec Awref"
		description: ""
		type: string
		sql: ${TABLE}.PREC_AWREF ;;
	}
	dimension: prec_awitem {
		label: "Prec Awitem"
		description: ""
		type: string
		sql: ${TABLE}.PREC_AWITEM ;;
	}
	dimension: prec_subta {
		label: "Prec Subta"
		description: ""
		type: string
		sql: ${TABLE}.PREC_SUBTA ;;
	}
	# FIXME: BLOB did not match any rules for conversion
	dimension: prec_awmult {
		label: "Prec Awmult"
		description: ""
		type: BLOB
		sql: ${TABLE}.PREC_AWMULT ;;
	}
	dimension: xsecondary {
		label: "Xsecondary"
		description: ""
		type: string
		sql: ${TABLE}.XSECONDARY ;;
	}
	dimension: rtcur {
		label: "Rtcur"
		description: ""
		type: string
		sql: ${TABLE}.RTCUR ;;
	}
	dimension: rwcur {
		label: "Rwcur"
		description: ""
		type: string
		sql: ${TABLE}.RWCUR ;;
	}
	dimension: rhcur {
		label: "Rhcur"
		description: ""
		type: string
		sql: ${TABLE}.RHCUR ;;
	}
	dimension: rkcur {
		label: "Rkcur"
		description: ""
		type: string
		sql: ${TABLE}.RKCUR ;;
	}
	dimension: rocur {
		label: "Rocur"
		description: ""
		type: string
		sql: ${TABLE}.ROCUR ;;
	}
	dimension: rvcur {
		label: "Rvcur"
		description: ""
		type: string
		sql: ${TABLE}.RVCUR ;;
	}
	dimension: rbcur {
		label: "Rbcur"
		description: ""
		type: string
		sql: ${TABLE}.RBCUR ;;
	}
	dimension: rccur {
		label: "Rccur"
		description: ""
		type: string
		sql: ${TABLE}.RCCUR ;;
	}
	dimension: rdcur {
		label: "Rdcur"
		description: ""
		type: string
		sql: ${TABLE}.RDCUR ;;
	}
	dimension: recur {
		label: "Recur"
		description: ""
		type: string
		sql: ${TABLE}.RECUR ;;
	}
	dimension: rfcur {
		label: "Rfcur"
		description: ""
		type: string
		sql: ${TABLE}.RFCUR ;;
	}
	dimension: rgcur {
		label: "Rgcur"
		description: ""
		type: string
		sql: ${TABLE}.RGCUR ;;
	}
	dimension: rco_ocur {
		label: "Rco Ocur"
		description: ""
		type: string
		sql: ${TABLE}.RCO_OCUR ;;
	}
	dimension: runit {
		label: "Runit"
		description: ""
		type: string
		sql: ${TABLE}.RUNIT ;;
	}
	dimension: rvunit {
		label: "Rvunit"
		description: ""
		type: string
		sql: ${TABLE}.RVUNIT ;;
	}
	dimension: rrunit {
		label: "Rrunit"
		description: ""
		type: string
		sql: ${TABLE}.RRUNIT ;;
	}
	dimension: riunit {
		label: "Riunit"
		description: ""
		type: string
		sql: ${TABLE}.RIUNIT ;;
	}
	dimension: qunit1 {
		label: "Qunit1"
		description: ""
		type: string
		sql: ${TABLE}.QUNIT1 ;;
	}
	dimension: qunit2 {
		label: "Qunit2"
		description: ""
		type: string
		sql: ${TABLE}.QUNIT2 ;;
	}
	dimension: qunit3 {
		label: "Qunit3"
		description: ""
		type: string
		sql: ${TABLE}.QUNIT3 ;;
	}
	dimension: co_meinh {
		label: "Co Meinh"
		description: ""
		type: string
		sql: ${TABLE}.CO_MEINH ;;
	}
	dimension: racct {
		label: "Racct"
		description: ""
		type: string
		sql: ${TABLE}.RACCT ;;
	}
	dimension: rcntr {
		label: "Rcntr"
		description: ""
		type: string
		sql: ${TABLE}.RCNTR ;;
	}
	dimension: prctr {
		label: "Prctr"
		description: ""
		type: string
		sql: ${TABLE}.PRCTR ;;
	}
	dimension: rfarea {
		label: "Rfarea"
		description: ""
		type: string
		sql: ${TABLE}.RFAREA ;;
	}
	dimension: rbusa {
		label: "Rbusa"
		description: ""
		type: string
		sql: ${TABLE}.RBUSA ;;
	}
	dimension: kokrs {
		label: "Kokrs"
		description: ""
		type: string
		sql: ${TABLE}.KOKRS ;;
	}
	dimension: segment {
		label: "Segment"
		description: ""
		type: string
		sql: ${TABLE}.SEGMENT ;;
	}
	dimension: scntr {
		label: "Scntr"
		description: ""
		type: string
		sql: ${TABLE}.SCNTR ;;
	}
	dimension: pprctr {
		label: "Pprctr"
		description: ""
		type: string
		sql: ${TABLE}.PPRCTR ;;
	}
	dimension: sfarea {
		label: "Sfarea"
		description: ""
		type: string
		sql: ${TABLE}.SFAREA ;;
	}
	dimension: sbusa {
		label: "Sbusa"
		description: ""
		type: string
		sql: ${TABLE}.SBUSA ;;
	}
	dimension: rassc {
		label: "Rassc"
		description: ""
		type: string
		sql: ${TABLE}.RASSC ;;
	}
	dimension: psegment {
		label: "Psegment"
		description: ""
		type: string
		sql: ${TABLE}.PSEGMENT ;;
	}
	dimension: tsl {
		label: "Tsl"
		description: ""
		type: number
		sql: ${TABLE}.TSL ;;
	}
	dimension: wsl {
		label: "Wsl"
		description: ""
		type: number
		sql: ${TABLE}.WSL ;;
	}
	dimension: wsl2 {
		label: "Wsl2"
		description: ""
		type: number
		sql: ${TABLE}.WSL2 ;;
	}
	dimension: wsl3 {
		label: "Wsl3"
		description: ""
		type: number
		sql: ${TABLE}.WSL3 ;;
	}
	dimension: hsl {
		label: "Hsl"
		description: ""
		type: number
		sql: ${TABLE}.HSL ;;
	}
	dimension: ksl {
		label: "Ksl"
		description: ""
		type: number
		sql: ${TABLE}.KSL ;;
	}
	dimension: osl {
		label: "Osl"
		description: ""
		type: number
		sql: ${TABLE}.OSL ;;
	}
	dimension: vsl {
		label: "Vsl"
		description: ""
		type: number
		sql: ${TABLE}.VSL ;;
	}
	dimension: bsl {
		label: "Bsl"
		description: ""
		type: number
		sql: ${TABLE}.BSL ;;
	}
	dimension: csl {
		label: "Csl"
		description: ""
		type: number
		sql: ${TABLE}.CSL ;;
	}
	dimension: dsl {
		label: "Dsl"
		description: ""
		type: number
		sql: ${TABLE}.DSL ;;
	}
	dimension: esl {
		label: "Esl"
		description: ""
		type: number
		sql: ${TABLE}.ESL ;;
	}
	dimension: fsl {
		label: "Fsl"
		description: ""
		type: number
		sql: ${TABLE}.FSL ;;
	}
	dimension: gsl {
		label: "Gsl"
		description: ""
		type: number
		sql: ${TABLE}.GSL ;;
	}
	dimension: kfsl {
		label: "Kfsl"
		description: ""
		type: number
		sql: ${TABLE}.KFSL ;;
	}
	dimension: kfsl2 {
		label: "Kfsl2"
		description: ""
		type: number
		sql: ${TABLE}.KFSL2 ;;
	}
	dimension: kfsl3 {
		label: "Kfsl3"
		description: ""
		type: number
		sql: ${TABLE}.KFSL3 ;;
	}
	dimension: psl {
		label: "Psl"
		description: ""
		type: number
		sql: ${TABLE}.PSL ;;
	}
	dimension: psl2 {
		label: "Psl2"
		description: ""
		type: number
		sql: ${TABLE}.PSL2 ;;
	}
	dimension: psl3 {
		label: "Psl3"
		description: ""
		type: number
		sql: ${TABLE}.PSL3 ;;
	}
	dimension: pfsl {
		label: "Pfsl"
		description: ""
		type: number
		sql: ${TABLE}.PFSL ;;
	}
	dimension: pfsl2 {
		label: "Pfsl2"
		description: ""
		type: number
		sql: ${TABLE}.PFSL2 ;;
	}
	dimension: pfsl3 {
		label: "Pfsl3"
		description: ""
		type: number
		sql: ${TABLE}.PFSL3 ;;
	}
	dimension: co_osl {
		label: "Co Osl"
		description: ""
		type: number
		sql: ${TABLE}.CO_OSL ;;
	}
	dimension: hsalk3 {
		label: "Hsalk3"
		description: ""
		type: number
		sql: ${TABLE}.HSALK3 ;;
	}
	dimension: ksalk3 {
		label: "Ksalk3"
		description: ""
		type: number
		sql: ${TABLE}.KSALK3 ;;
	}
	dimension: osalk3 {
		label: "Osalk3"
		description: ""
		type: number
		sql: ${TABLE}.OSALK3 ;;
	}
	dimension: vsalk3 {
		label: "Vsalk3"
		description: ""
		type: number
		sql: ${TABLE}.VSALK3 ;;
	}
	dimension: hsalkv {
		label: "Hsalkv"
		description: ""
		type: number
		sql: ${TABLE}.HSALKV ;;
	}
	dimension: ksalkv {
		label: "Ksalkv"
		description: ""
		type: number
		sql: ${TABLE}.KSALKV ;;
	}
	dimension: osalkv {
		label: "Osalkv"
		description: ""
		type: number
		sql: ${TABLE}.OSALKV ;;
	}
	dimension: vsalkv {
		label: "Vsalkv"
		description: ""
		type: number
		sql: ${TABLE}.VSALKV ;;
	}
	dimension: hpvprs {
		label: "Hpvprs"
		description: ""
		type: number
		sql: ${TABLE}.HPVPRS ;;
	}
	dimension: kpvprs {
		label: "Kpvprs"
		description: ""
		type: number
		sql: ${TABLE}.KPVPRS ;;
	}
	dimension: opvprs {
		label: "Opvprs"
		description: ""
		type: number
		sql: ${TABLE}.OPVPRS ;;
	}
	dimension: vpvprs {
		label: "Vpvprs"
		description: ""
		type: number
		sql: ${TABLE}.VPVPRS ;;
	}
	dimension: hstprs {
		label: "Hstprs"
		description: ""
		type: number
		sql: ${TABLE}.HSTPRS ;;
	}
	dimension: kstprs {
		label: "Kstprs"
		description: ""
		type: number
		sql: ${TABLE}.KSTPRS ;;
	}
	dimension: ostprs {
		label: "Ostprs"
		description: ""
		type: number
		sql: ${TABLE}.OSTPRS ;;
	}
	dimension: vstprs {
		label: "Vstprs"
		description: ""
		type: number
		sql: ${TABLE}.VSTPRS ;;
	}
	dimension: hslalt {
		label: "Hslalt"
		description: ""
		type: number
		sql: ${TABLE}.HSLALT ;;
	}
	dimension: kslalt {
		label: "Kslalt"
		description: ""
		type: number
		sql: ${TABLE}.KSLALT ;;
	}
	dimension: oslalt {
		label: "Oslalt"
		description: ""
		type: number
		sql: ${TABLE}.OSLALT ;;
	}
	dimension: vslalt {
		label: "Vslalt"
		description: ""
		type: number
		sql: ${TABLE}.VSLALT ;;
	}
	dimension: hslext {
		label: "Hslext"
		description: ""
		type: number
		sql: ${TABLE}.HSLEXT ;;
	}
	dimension: kslext {
		label: "Kslext"
		description: ""
		type: number
		sql: ${TABLE}.KSLEXT ;;
	}
	dimension: oslext {
		label: "Oslext"
		description: ""
		type: number
		sql: ${TABLE}.OSLEXT ;;
	}
	dimension: vslext {
		label: "Vslext"
		description: ""
		type: number
		sql: ${TABLE}.VSLEXT ;;
	}
	dimension: hvkwrt {
		label: "Hvkwrt"
		description: ""
		type: number
		sql: ${TABLE}.HVKWRT ;;
	}
	dimension: hvksal {
		label: "Hvksal"
		description: ""
		type: number
		sql: ${TABLE}.HVKSAL ;;
	}
	dimension: msl {
		label: "Msl"
		description: ""
		type: number
		sql: ${TABLE}.MSL ;;
	}
	dimension: mfsl {
		label: "Mfsl"
		description: ""
		type: number
		sql: ${TABLE}.MFSL ;;
	}
	dimension: vmsl {
		label: "Vmsl"
		description: ""
		type: number
		sql: ${TABLE}.VMSL ;;
	}
	dimension: vmfsl {
		label: "Vmfsl"
		description: ""
		type: number
		sql: ${TABLE}.VMFSL ;;
	}
	dimension: rmsl {
		label: "Rmsl"
		description: ""
		type: number
		sql: ${TABLE}.RMSL ;;
	}
	dimension: quant1 {
		label: "Quant1"
		description: ""
		type: number
		sql: ${TABLE}.QUANT1 ;;
	}
	dimension: quant2 {
		label: "Quant2"
		description: ""
		type: number
		sql: ${TABLE}.QUANT2 ;;
	}
	dimension: quant3 {
		label: "Quant3"
		description: ""
		type: number
		sql: ${TABLE}.QUANT3 ;;
	}
	dimension: co_megbtr {
		label: "Co Megbtr"
		description: ""
		type: number
		sql: ${TABLE}.CO_MEGBTR ;;
	}
	dimension: co_mefbtr {
		label: "Co Mefbtr"
		description: ""
		type: number
		sql: ${TABLE}.CO_MEFBTR ;;
	}
	dimension: lbkum {
		label: "Lbkum"
		description: ""
		type: number
		sql: ${TABLE}.LBKUM ;;
	}
	dimension: drcrk {
		label: "Drcrk"
		description: ""
		type: string
		sql: ${TABLE}.DRCRK ;;
	}
	dimension: poper {
		label: "Poper"
		description: ""
		type: string
		sql: ${TABLE}.POPER ;;
	}
	dimension: periv {
		label: "Periv"
		description: ""
		type: string
		sql: ${TABLE}.PERIV ;;
	}
	dimension: fiscyearper {
		label: "Fiscyearper"
		description: ""
		type: string
		sql: ${TABLE}.FISCYEARPER ;;
	}
	dimension: budat {
		label: "Budat"
		description: ""
		type: string
		sql: ${TABLE}.BUDAT ;;
	}
	dimension: bldat {
		label: "Bldat"
		description: ""
		type: string
		sql: ${TABLE}.BLDAT ;;
	}
	dimension: blart {
		label: "Blart"
		description: ""
		type: string
		sql: ${TABLE}.BLART ;;
	}
	dimension: buzei {
		label: "Buzei"
		description: ""
		type: string
		sql: ${TABLE}.BUZEI ;;
	}
	dimension: zuonr {
		label: "Zuonr"
		description: ""
		type: string
		sql: ${TABLE}.ZUONR ;;
	}
	dimension: bschl {
		label: "Bschl"
		description: ""
		type: string
		sql: ${TABLE}.BSCHL ;;
	}
	dimension: bstat {
		label: "Bstat"
		description: ""
		type: string
		sql: ${TABLE}.BSTAT ;;
	}
	dimension: linetype {
		label: "Linetype"
		description: ""
		type: string
		sql: ${TABLE}.LINETYPE ;;
	}
	dimension: ktosl {
		label: "Ktosl"
		description: ""
		type: string
		sql: ${TABLE}.KTOSL ;;
	}
	dimension: slalittype {
		label: "Slalittype"
		description: ""
		type: string
		sql: ${TABLE}.SLALITTYPE ;;
	}
	dimension: xsplitmod {
		label: "Xsplitmod"
		description: ""
		type: string
		sql: ${TABLE}.XSPLITMOD ;;
	}
	dimension: usnam {
		label: "Usnam"
		description: ""
		type: string
		sql: ${TABLE}.USNAM ;;
	}
	dimension: timestamp {
		label: "Timestamp"
		description: ""
		type: number
		sql: ${TABLE}.TIMESTAMP ;;
	}
	dimension: eprctr {
		label: "Eprctr"
		description: ""
		type: string
		sql: ${TABLE}.EPRCTR ;;
	}
	dimension: rhoart {
		label: "Rhoart"
		description: ""
		type: string
		sql: ${TABLE}.RHOART ;;
	}
	dimension: glaccount_type {
		label: "Glaccount Type"
		description: ""
		type: string
		sql: ${TABLE}.GLACCOUNT_TYPE ;;
	}
	dimension: ktopl {
		label: "Ktopl"
		description: ""
		type: string
		sql: ${TABLE}.KTOPL ;;
	}
	dimension: lokkt {
		label: "Lokkt"
		description: ""
		type: string
		sql: ${TABLE}.LOKKT ;;
	}
	dimension: ktop2 {
		label: "Ktop2"
		description: ""
		type: string
		sql: ${TABLE}.KTOP2 ;;
	}
	dimension: rebzg {
		label: "Rebzg"
		description: ""
		type: string
		sql: ${TABLE}.REBZG ;;
	}
	dimension: rebzj {
		label: "Rebzj"
		description: ""
		type: string
		sql: ${TABLE}.REBZJ ;;
	}
	dimension: rebzz {
		label: "Rebzz"
		description: ""
		type: string
		sql: ${TABLE}.REBZZ ;;
	}
	dimension: rebzt {
		label: "Rebzt"
		description: ""
		type: string
		sql: ${TABLE}.REBZT ;;
	}
	dimension: rbest {
		label: "Rbest"
		description: ""
		type: string
		sql: ${TABLE}.RBEST ;;
	}
	dimension: ebeln {
		label: "Ebeln"
		description: ""
		type: string
		sql: ${TABLE}.EBELN ;;
	}
	dimension: ebelp {
		label: "Ebelp"
		description: ""
		type: string
		sql: ${TABLE}.EBELP ;;
	}
	dimension: zekkn {
		label: "Zekkn"
		description: ""
		type: string
		sql: ${TABLE}.ZEKKN ;;
	}
	dimension: sgtxt {
		label: "Sgtxt"
		description: ""
		type: string
		sql: ${TABLE}.SGTXT ;;
	}
	dimension: kdauf {
		label: "Kdauf"
		description: ""
		type: string
		sql: ${TABLE}.KDAUF ;;
	}
	dimension: kdpos {
		label: "Kdpos"
		description: ""
		type: string
		sql: ${TABLE}.KDPOS ;;
	}
	dimension: matnr {
		label: "Matnr"
		description: ""
		type: string
		sql: ${TABLE}.MATNR ;;
	}
	dimension: werks {
		label: "Werks"
		description: ""
		type: string
		sql: ${TABLE}.WERKS ;;
	}
	dimension: lifnr {
		label: "Lifnr"
		description: ""
		type: string
		sql: ${TABLE}.LIFNR ;;
	}
	dimension: kunnr {
		label: "Kunnr"
		description: ""
		type: string
		sql: ${TABLE}.KUNNR ;;
	}
	dimension: fbuda {
		label: "Fbuda"
		description: ""
		type: string
		sql: ${TABLE}.FBUDA ;;
	}
	dimension: koart {
		label: "Koart"
		description: ""
		type: string
		sql: ${TABLE}.KOART ;;
	}
	dimension: umskz {
		label: "Umskz"
		description: ""
		type: string
		sql: ${TABLE}.UMSKZ ;;
	}
	dimension: mwskz {
		label: "Mwskz"
		description: ""
		type: string
		sql: ${TABLE}.MWSKZ ;;
	}
	dimension: hbkid {
		label: "Hbkid"
		description: ""
		type: string
		sql: ${TABLE}.HBKID ;;
	}
	dimension: hktid {
		label: "Hktid"
		description: ""
		type: string
		sql: ${TABLE}.HKTID ;;
	}
	dimension: xopvw {
		label: "Xopvw"
		description: ""
		type: string
		sql: ${TABLE}.XOPVW ;;
	}
	dimension: augdt {
		label: "Augdt"
		description: ""
		type: string
		sql: ${TABLE}.AUGDT ;;
	}
	dimension: augbl {
		label: "Augbl"
		description: ""
		type: string
		sql: ${TABLE}.AUGBL ;;
	}
	dimension: auggj {
		label: "Auggj"
		description: ""
		type: string
		sql: ${TABLE}.AUGGJ ;;
	}
	dimension: afabe {
		label: "Afabe"
		description: ""
		type: string
		sql: ${TABLE}.AFABE ;;
	}
	dimension: anln1 {
		label: "Anln1"
		description: ""
		type: string
		sql: ${TABLE}.ANLN1 ;;
	}
	dimension: anln2 {
		label: "Anln2"
		description: ""
		type: string
		sql: ${TABLE}.ANLN2 ;;
	}
	dimension: bzdat {
		label: "Bzdat"
		description: ""
		type: string
		sql: ${TABLE}.BZDAT ;;
	}
	dimension: anbwa {
		label: "Anbwa"
		description: ""
		type: string
		sql: ${TABLE}.ANBWA ;;
	}
	dimension: movcat {
		label: "Movcat"
		description: ""
		type: string
		sql: ${TABLE}.MOVCAT ;;
	}
	dimension: depr_period {
		label: "Depr Period"
		description: ""
		type: string
		sql: ${TABLE}.DEPR_PERIOD ;;
	}
	dimension: anlgr {
		label: "Anlgr"
		description: ""
		type: string
		sql: ${TABLE}.ANLGR ;;
	}
	dimension: anlgr2 {
		label: "Anlgr2"
		description: ""
		type: string
		sql: ${TABLE}.ANLGR2 ;;
	}
	dimension: settlement_rule {
		label: "Settlement Rule"
		description: ""
		type: string
		sql: ${TABLE}.SETTLEMENT_RULE ;;
	}
	dimension: anlkl {
		label: "Anlkl"
		description: ""
		type: string
		sql: ${TABLE}.ANLKL ;;
	}
	dimension: ktogr {
		label: "Ktogr"
		description: ""
		type: string
		sql: ${TABLE}.KTOGR ;;
	}
	dimension: panl1 {
		label: "Panl1"
		description: ""
		type: string
		sql: ${TABLE}.PANL1 ;;
	}
	dimension: panl2 {
		label: "Panl2"
		description: ""
		type: string
		sql: ${TABLE}.PANL2 ;;
	}
	dimension: ubzdt_pn {
		label: "Ubzdt Pn"
		description: ""
		type: string
		sql: ${TABLE}.UBZDT_PN ;;
	}
	dimension: xvabg_pn {
		label: "Xvabg Pn"
		description: ""
		type: string
		sql: ${TABLE}.XVABG_PN ;;
	}
	dimension: prozs_pn {
		label: "Prozs Pn"
		description: ""
		type: number
		sql: ${TABLE}.PROZS_PN ;;
	}
	dimension: xmanpropval_pn {
		label: "Xmanpropval Pn"
		description: ""
		type: string
		sql: ${TABLE}.XMANPROPVAL_PN ;;
	}
	dimension: kalnr {
		label: "Kalnr"
		description: ""
		type: string
		sql: ${TABLE}.KALNR ;;
	}
	dimension: vprsv {
		label: "Vprsv"
		description: ""
		type: string
		sql: ${TABLE}.VPRSV ;;
	}
	dimension: mlast {
		label: "Mlast"
		description: ""
		type: string
		sql: ${TABLE}.MLAST ;;
	}
	dimension: kzbws {
		label: "Kzbws"
		description: ""
		type: string
		sql: ${TABLE}.KZBWS ;;
	}
	dimension: xobew {
		label: "Xobew"
		description: ""
		type: string
		sql: ${TABLE}.XOBEW ;;
	}
	dimension: sobkz {
		label: "Sobkz"
		description: ""
		type: string
		sql: ${TABLE}.SOBKZ ;;
	}
	dimension: vtstamp {
		label: "Vtstamp"
		description: ""
		type: number
		sql: ${TABLE}.VTSTAMP ;;
	}
	dimension: mat_kdauf {
		label: "Mat Kdauf"
		description: ""
		type: string
		sql: ${TABLE}.MAT_KDAUF ;;
	}
	dimension: mat_kdpos {
		label: "Mat Kdpos"
		description: ""
		type: string
		sql: ${TABLE}.MAT_KDPOS ;;
	}
	dimension: mat_pspnr {
		label: "Mat Pspnr"
		description: ""
		type: string
		sql: ${TABLE}.MAT_PSPNR ;;
	}
	dimension: mat_ps_posid {
		label: "Mat Ps Posid"
		description: ""
		type: string
		sql: ${TABLE}.MAT_PS_POSID ;;
	}
	dimension: mat_lifnr {
		label: "Mat Lifnr"
		description: ""
		type: string
		sql: ${TABLE}.MAT_LIFNR ;;
	}
	dimension: bwtar {
		label: "Bwtar"
		description: ""
		type: string
		sql: ${TABLE}.BWTAR ;;
	}
	dimension: bwkey {
		label: "Bwkey"
		description: ""
		type: string
		sql: ${TABLE}.BWKEY ;;
	}
	dimension: hpeinh {
		label: "Hpeinh"
		description: ""
		type: number
		sql: ${TABLE}.HPEINH ;;
	}
	dimension: kpeinh {
		label: "Kpeinh"
		description: ""
		type: number
		sql: ${TABLE}.KPEINH ;;
	}
	dimension: opeinh {
		label: "Opeinh"
		description: ""
		type: number
		sql: ${TABLE}.OPEINH ;;
	}
	dimension: vpeinh {
		label: "Vpeinh"
		description: ""
		type: number
		sql: ${TABLE}.VPEINH ;;
	}
	dimension: mlptyp {
		label: "Mlptyp"
		description: ""
		type: string
		sql: ${TABLE}.MLPTYP ;;
	}
	dimension: mlcateg {
		label: "Mlcateg"
		description: ""
		type: string
		sql: ${TABLE}.MLCATEG ;;
	}
	dimension: qsbvalt {
		label: "Qsbvalt"
		description: ""
		type: string
		sql: ${TABLE}.QSBVALT ;;
	}
	dimension: qsprocess {
		label: "Qsprocess"
		description: ""
		type: string
		sql: ${TABLE}.QSPROCESS ;;
	}
	dimension: perart {
		label: "Perart"
		description: ""
		type: string
		sql: ${TABLE}.PERART ;;
	}
	dimension: mlposnr {
		label: "Mlposnr"
		description: ""
		type: string
		sql: ${TABLE}.MLPOSNR ;;
	}
	dimension: bukrs_sender {
		label: "Bukrs Sender"
		description: ""
		type: string
		sql: ${TABLE}.BUKRS_SENDER ;;
	}
	dimension: racct_sender {
		label: "Racct Sender"
		description: ""
		type: string
		sql: ${TABLE}.RACCT_SENDER ;;
	}
	dimension: accas_sender {
		label: "Accas Sender"
		description: ""
		type: string
		sql: ${TABLE}.ACCAS_SENDER ;;
	}
	dimension: accasty_sender {
		label: "Accasty Sender"
		description: ""
		type: string
		sql: ${TABLE}.ACCASTY_SENDER ;;
	}
	dimension: objnr {
		label: "Objnr"
		description: ""
		type: string
		sql: ${TABLE}.OBJNR ;;
	}
	dimension: hrkft {
		label: "Hrkft"
		description: ""
		type: string
		sql: ${TABLE}.HRKFT ;;
	}
	dimension: hkgrp {
		label: "Hkgrp"
		description: ""
		type: string
		sql: ${TABLE}.HKGRP ;;
	}
	dimension: parob1 {
		label: "Parob1"
		description: ""
		type: string
		sql: ${TABLE}.PAROB1 ;;
	}
	dimension: parobsrc {
		label: "Parobsrc"
		description: ""
		type: string
		sql: ${TABLE}.PAROBSRC ;;
	}
	dimension: uspob {
		label: "Uspob"
		description: ""
		type: string
		sql: ${TABLE}.USPOB ;;
	}
	dimension: co_belkz {
		label: "Co Belkz"
		description: ""
		type: string
		sql: ${TABLE}.CO_BELKZ ;;
	}
	dimension: co_beknz {
		label: "Co Beknz"
		description: ""
		type: string
		sql: ${TABLE}.CO_BEKNZ ;;
	}
	dimension: beltp {
		label: "Beltp"
		description: ""
		type: string
		sql: ${TABLE}.BELTP ;;
	}
	dimension: muvflg {
		label: "Muvflg"
		description: ""
		type: string
		sql: ${TABLE}.MUVFLG ;;
	}
	dimension: gkont {
		label: "Gkont"
		description: ""
		type: string
		sql: ${TABLE}.GKONT ;;
	}
	dimension: gkoar {
		label: "Gkoar"
		description: ""
		type: string
		sql: ${TABLE}.GKOAR ;;
	}
	dimension: erlkz {
		label: "Erlkz"
		description: ""
		type: string
		sql: ${TABLE}.ERLKZ ;;
	}
	dimension: pernr {
		label: "Pernr"
		description: ""
		type: string
		sql: ${TABLE}.PERNR ;;
	}
	dimension: paobjnr {
		label: "Paobjnr"
		description: ""
		type: string
		sql: ${TABLE}.PAOBJNR ;;
	}
	dimension: xpaobjnr_co_rel {
		label: "Xpaobjnr Co Rel"
		description: ""
		type: string
		sql: ${TABLE}.XPAOBJNR_CO_REL ;;
	}
	dimension: scope {
		label: "Scope"
		description: ""
		type: string
		sql: ${TABLE}.SCOPE ;;
	}
	dimension: logsyso {
		label: "Logsyso"
		description: ""
		type: string
		sql: ${TABLE}.LOGSYSO ;;
	}
	dimension: pbukrs {
		label: "Pbukrs"
		description: ""
		type: string
		sql: ${TABLE}.PBUKRS ;;
	}
	dimension: pscope {
		label: "Pscope"
		description: ""
		type: string
		sql: ${TABLE}.PSCOPE ;;
	}
	dimension: logsysp {
		label: "Logsysp"
		description: ""
		type: string
		sql: ${TABLE}.LOGSYSP ;;
	}
	dimension: bwstrat {
		label: "Bwstrat"
		description: ""
		type: string
		sql: ${TABLE}.BWSTRAT ;;
	}
	dimension: objnr_hk {
		label: "Objnr Hk"
		description: ""
		type: string
		sql: ${TABLE}.OBJNR_HK ;;
	}
	dimension: aufnr_org {
		label: "Aufnr Org"
		description: ""
		type: string
		sql: ${TABLE}.AUFNR_ORG ;;
	}
	dimension: ukostl {
		label: "Ukostl"
		description: ""
		type: string
		sql: ${TABLE}.UKOSTL ;;
	}
	dimension: ulstar {
		label: "Ulstar"
		description: ""
		type: string
		sql: ${TABLE}.ULSTAR ;;
	}
	dimension: uprznr {
		label: "Uprznr"
		description: ""
		type: string
		sql: ${TABLE}.UPRZNR ;;
	}
	dimension: uprctr {
		label: "Uprctr"
		description: ""
		type: string
		sql: ${TABLE}.UPRCTR ;;
	}
	dimension: accas {
		label: "Accas"
		description: ""
		type: string
		sql: ${TABLE}.ACCAS ;;
	}
	dimension: accasty {
		label: "Accasty"
		description: ""
		type: string
		sql: ${TABLE}.ACCASTY ;;
	}
	dimension: lstar {
		label: "Lstar"
		description: ""
		type: string
		sql: ${TABLE}.LSTAR ;;
	}
	dimension: aufnr {
		label: "Aufnr"
		description: ""
		type: string
		sql: ${TABLE}.AUFNR ;;
	}
	dimension: autyp {
		label: "Autyp"
		description: ""
		type: string
		sql: ${TABLE}.AUTYP ;;
	}
	dimension: ps_psp_pnr {
		label: "Ps Psp Pnr"
		description: ""
		type: string
		sql: ${TABLE}.PS_PSP_PNR ;;
	}
	dimension: ps_posid {
		label: "Ps Posid"
		description: ""
		type: string
		sql: ${TABLE}.PS_POSID ;;
	}
	dimension: ps_pspid {
		label: "Ps Pspid"
		description: ""
		type: string
		sql: ${TABLE}.PS_PSPID ;;
	}
	dimension: nplnr {
		label: "Nplnr"
		description: ""
		type: string
		sql: ${TABLE}.NPLNR ;;
	}
	dimension: nplnr_vorgn {
		label: "Nplnr Vorgn"
		description: ""
		type: string
		sql: ${TABLE}.NPLNR_VORGN ;;
	}
	dimension: prznr {
		label: "Prznr"
		description: ""
		type: string
		sql: ${TABLE}.PRZNR ;;
	}
	dimension: kstrg {
		label: "Kstrg"
		description: ""
		type: string
		sql: ${TABLE}.KSTRG ;;
	}
	dimension: bemot {
		label: "Bemot"
		description: ""
		type: string
		sql: ${TABLE}.BEMOT ;;
	}
	dimension: rsrce {
		label: "Rsrce"
		description: ""
		type: string
		sql: ${TABLE}.RSRCE ;;
	}
	dimension: qmnum {
		label: "Qmnum"
		description: ""
		type: string
		sql: ${TABLE}.QMNUM ;;
	}
	dimension: erkrs {
		label: "Erkrs"
		description: ""
		type: string
		sql: ${TABLE}.ERKRS ;;
	}
	dimension: paccas {
		label: "Paccas"
		description: ""
		type: string
		sql: ${TABLE}.PACCAS ;;
	}
	dimension: paccasty {
		label: "Paccasty"
		description: ""
		type: string
		sql: ${TABLE}.PACCASTY ;;
	}
	dimension: plstar {
		label: "Plstar"
		description: ""
		type: string
		sql: ${TABLE}.PLSTAR ;;
	}
	dimension: paufnr {
		label: "Paufnr"
		description: ""
		type: string
		sql: ${TABLE}.PAUFNR ;;
	}
	dimension: pautyp {
		label: "Pautyp"
		description: ""
		type: string
		sql: ${TABLE}.PAUTYP ;;
	}
	dimension: pps_posid {
		label: "Pps Posid"
		description: ""
		type: string
		sql: ${TABLE}.PPS_POSID ;;
	}
	dimension: pps_pspid {
		label: "Pps Pspid"
		description: ""
		type: string
		sql: ${TABLE}.PPS_PSPID ;;
	}
	dimension: pkdauf {
		label: "Pkdauf"
		description: ""
		type: string
		sql: ${TABLE}.PKDAUF ;;
	}
	dimension: pkdpos {
		label: "Pkdpos"
		description: ""
		type: string
		sql: ${TABLE}.PKDPOS ;;
	}
	dimension: ppaobjnr {
		label: "Ppaobjnr"
		description: ""
		type: string
		sql: ${TABLE}.PPAOBJNR ;;
	}
	dimension: pnplnr {
		label: "Pnplnr"
		description: ""
		type: string
		sql: ${TABLE}.PNPLNR ;;
	}
	dimension: pnplnr_vorgn {
		label: "Pnplnr Vorgn"
		description: ""
		type: string
		sql: ${TABLE}.PNPLNR_VORGN ;;
	}
	dimension: pprznr {
		label: "Pprznr"
		description: ""
		type: string
		sql: ${TABLE}.PPRZNR ;;
	}
	dimension: pkstrg {
		label: "Pkstrg"
		description: ""
		type: string
		sql: ${TABLE}.PKSTRG ;;
	}
	dimension: co_accasty_n1 {
		label: "Co Accasty N1"
		description: ""
		type: string
		sql: ${TABLE}.CO_ACCASTY_N1 ;;
	}
	dimension: co_accasty_n2 {
		label: "Co Accasty N2"
		description: ""
		type: string
		sql: ${TABLE}.CO_ACCASTY_N2 ;;
	}
	dimension: co_accasty_n3 {
		label: "Co Accasty N3"
		description: ""
		type: string
		sql: ${TABLE}.CO_ACCASTY_N3 ;;
	}
	dimension: co_zlenr {
		label: "Co Zlenr"
		description: ""
		type: string
		sql: ${TABLE}.CO_ZLENR ;;
	}
	dimension: co_belnr {
		label: "Co Belnr"
		description: ""
		type: string
		sql: ${TABLE}.CO_BELNR ;;
	}
	dimension: co_buzei {
		label: "Co Buzei"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI ;;
	}
	dimension: co_buzei1 {
		label: "Co Buzei1"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI1 ;;
	}
	dimension: co_buzei2 {
		label: "Co Buzei2"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI2 ;;
	}
	dimension: co_buzei5 {
		label: "Co Buzei5"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI5 ;;
	}
	dimension: co_buzei6 {
		label: "Co Buzei6"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI6 ;;
	}
	dimension: co_buzei7 {
		label: "Co Buzei7"
		description: ""
		type: string
		sql: ${TABLE}.CO_BUZEI7 ;;
	}
	dimension: co_refbz {
		label: "Co Refbz"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ ;;
	}
	dimension: co_refbz1 {
		label: "Co Refbz1"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ1 ;;
	}
	dimension: co_refbz2 {
		label: "Co Refbz2"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ2 ;;
	}
	dimension: co_refbz5 {
		label: "Co Refbz5"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ5 ;;
	}
	dimension: co_refbz6 {
		label: "Co Refbz6"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ6 ;;
	}
	dimension: co_refbz7 {
		label: "Co Refbz7"
		description: ""
		type: string
		sql: ${TABLE}.CO_REFBZ7 ;;
	}
	dimension: work_item_id {
		label: "Work Item Id"
		description: ""
		type: string
		sql: ${TABLE}.WORK_ITEM_ID ;;
	}
	dimension: arbid {
		label: "Arbid"
		description: ""
		type: string
		sql: ${TABLE}.ARBID ;;
	}
	dimension: vornr {
		label: "Vornr"
		description: ""
		type: string
		sql: ${TABLE}.VORNR ;;
	}
	dimension: aufps {
		label: "Aufps"
		description: ""
		type: string
		sql: ${TABLE}.AUFPS ;;
	}
	dimension: uvorn {
		label: "Uvorn"
		description: ""
		type: string
		sql: ${TABLE}.UVORN ;;
	}
	dimension: equnr {
		label: "Equnr"
		description: ""
		type: string
		sql: ${TABLE}.EQUNR ;;
	}
	dimension: tplnr {
		label: "Tplnr"
		description: ""
		type: string
		sql: ${TABLE}.TPLNR ;;
	}
	dimension: istru {
		label: "Istru"
		description: ""
		type: string
		sql: ${TABLE}.ISTRU ;;
	}
	dimension: ilart {
		label: "Ilart"
		description: ""
		type: string
		sql: ${TABLE}.ILART ;;
	}
	dimension: plknz {
		label: "Plknz"
		description: ""
		type: string
		sql: ${TABLE}.PLKNZ ;;
	}
	dimension: artpr {
		label: "Artpr"
		description: ""
		type: string
		sql: ${TABLE}.ARTPR ;;
	}
	dimension: priok {
		label: "Priok"
		description: ""
		type: string
		sql: ${TABLE}.PRIOK ;;
	}
	dimension: maufnr {
		label: "Maufnr"
		description: ""
		type: string
		sql: ${TABLE}.MAUFNR ;;
	}
	dimension: matkl_mm {
		label: "Matkl Mm"
		description: ""
		type: string
		sql: ${TABLE}.MATKL_MM ;;
	}
	dimension: planned_parts_work {
		label: "Planned Parts Work"
		description: ""
		type: string
		sql: ${TABLE}.PLANNED_PARTS_WORK ;;
	}
	dimension: fkart {
		label: "Fkart"
		description: ""
		type: string
		sql: ${TABLE}.FKART ;;
	}
	dimension: vkorg {
		label: "Vkorg"
		description: ""
		type: string
		sql: ${TABLE}.VKORG ;;
	}
	dimension: vtweg {
		label: "Vtweg"
		description: ""
		type: string
		sql: ${TABLE}.VTWEG ;;
	}
	dimension: spart {
		label: "Spart"
		description: ""
		type: string
		sql: ${TABLE}.SPART ;;
	}
	dimension: matnr_copa {
		label: "Matnr Copa"
		description: ""
		type: string
		sql: ${TABLE}.MATNR_COPA ;;
	}
	dimension: matkl {
		label: "Matkl"
		description: ""
		type: string
		sql: ${TABLE}.MATKL ;;
	}
	dimension: kdgrp {
		label: "Kdgrp"
		description: ""
		type: string
		sql: ${TABLE}.KDGRP ;;
	}
	dimension: land1 {
		label: "Land1"
		description: ""
		type: string
		sql: ${TABLE}.LAND1 ;;
	}
	dimension: brsch {
		label: "Brsch"
		description: ""
		type: string
		sql: ${TABLE}.BRSCH ;;
	}
	dimension: bzirk {
		label: "Bzirk"
		description: ""
		type: string
		sql: ${TABLE}.BZIRK ;;
	}
	dimension: kunre {
		label: "Kunre"
		description: ""
		type: string
		sql: ${TABLE}.KUNRE ;;
	}
	dimension: kunwe {
		label: "Kunwe"
		description: ""
		type: string
		sql: ${TABLE}.KUNWE ;;
	}
	dimension: konzs {
		label: "Konzs"
		description: ""
		type: string
		sql: ${TABLE}.KONZS ;;
	}
	dimension: acdoc_copa_eew_dummy_pa {
		label: "Acdoc Copa Eew Dummy Pa"
		description: ""
		type: string
		sql: ${TABLE}.ACDOC_COPA_EEW_DUMMY_PA ;;
	}
	dimension: re_bukrs {
		label: "Re Bukrs"
		description: ""
		type: string
		sql: ${TABLE}.RE_BUKRS ;;
	}
	dimension: re_account {
		label: "Re Account"
		description: ""
		type: string
		sql: ${TABLE}.RE_ACCOUNT ;;
	}
	dimension: fikrs {
		label: "Fikrs"
		description: ""
		type: string
		sql: ${TABLE}.FIKRS ;;
	}
	dimension: fistl {
		label: "Fistl"
		description: ""
		type: string
		sql: ${TABLE}.FISTL ;;
	}
	dimension: measure {
		label: "Measure"
		description: ""
		type: string
		sql: ${TABLE}.MEASURE ;;
	}
	dimension: rfund {
		label: "Rfund"
		description: ""
		type: string
		sql: ${TABLE}.RFUND ;;
	}
	dimension: rgrant_nbr {
		label: "Rgrant Nbr"
		description: ""
		type: string
		sql: ${TABLE}.RGRANT_NBR ;;
	}
	dimension: rbudget_pd {
		label: "Rbudget Pd"
		description: ""
		type: string
		sql: ${TABLE}.RBUDGET_PD ;;
	}
	dimension: sfund {
		label: "Sfund"
		description: ""
		type: string
		sql: ${TABLE}.SFUND ;;
	}
	dimension: sgrant_nbr {
		label: "Sgrant Nbr"
		description: ""
		type: string
		sql: ${TABLE}.SGRANT_NBR ;;
	}
	dimension: sbudget_pd {
		label: "Sbudget Pd"
		description: ""
		type: string
		sql: ${TABLE}.SBUDGET_PD ;;
	}
	dimension: vname {
		label: "Vname"
		description: ""
		type: string
		sql: ${TABLE}.VNAME ;;
	}
	dimension: egrup {
		label: "Egrup"
		description: ""
		type: string
		sql: ${TABLE}.EGRUP ;;
	}
	dimension: recid {
		label: "Recid"
		description: ""
		type: string
		sql: ${TABLE}.RECID ;;
	}
	dimension: vptnr {
		label: "Vptnr"
		description: ""
		type: string
		sql: ${TABLE}.VPTNR ;;
	}
	dimension: btype {
		label: "Btype"
		description: ""
		type: string
		sql: ${TABLE}.BTYPE ;;
	}
	dimension: etype {
		label: "Etype"
		description: ""
		type: string
		sql: ${TABLE}.ETYPE ;;
	}
	dimension: prodper {
		label: "Prodper"
		description: ""
		type: string
		sql: ${TABLE}.PRODPER ;;
	}
	dimension: swenr {
		label: "Swenr"
		description: ""
		type: string
		sql: ${TABLE}.SWENR ;;
	}
	dimension: sgenr {
		label: "Sgenr"
		description: ""
		type: string
		sql: ${TABLE}.SGENR ;;
	}
	dimension: sgrnr {
		label: "Sgrnr"
		description: ""
		type: string
		sql: ${TABLE}.SGRNR ;;
	}
	dimension: smenr {
		label: "Smenr"
		description: ""
		type: string
		sql: ${TABLE}.SMENR ;;
	}
	dimension: recnnr {
		label: "Recnnr"
		description: ""
		type: string
		sql: ${TABLE}.RECNNR ;;
	}
	dimension: snksl {
		label: "Snksl"
		description: ""
		type: string
		sql: ${TABLE}.SNKSL ;;
	}
	dimension: sempsl {
		label: "Sempsl"
		description: ""
		type: string
		sql: ${TABLE}.SEMPSL ;;
	}
	dimension: dabrz {
		label: "Dabrz"
		description: ""
		type: string
		sql: ${TABLE}.DABRZ ;;
	}
	dimension: pswenr {
		label: "Pswenr"
		description: ""
		type: string
		sql: ${TABLE}.PSWENR ;;
	}
	dimension: psgenr {
		label: "Psgenr"
		description: ""
		type: string
		sql: ${TABLE}.PSGENR ;;
	}
	dimension: psgrnr {
		label: "Psgrnr"
		description: ""
		type: string
		sql: ${TABLE}.PSGRNR ;;
	}
	dimension: psmenr {
		label: "Psmenr"
		description: ""
		type: string
		sql: ${TABLE}.PSMENR ;;
	}
	dimension: precnnr {
		label: "Precnnr"
		description: ""
		type: string
		sql: ${TABLE}.PRECNNR ;;
	}
	dimension: psnksl {
		label: "Psnksl"
		description: ""
		type: string
		sql: ${TABLE}.PSNKSL ;;
	}
	dimension: psempsl {
		label: "Psempsl"
		description: ""
		type: string
		sql: ${TABLE}.PSEMPSL ;;
	}
	dimension: pdabrz {
		label: "Pdabrz"
		description: ""
		type: string
		sql: ${TABLE}.PDABRZ ;;
	}
	dimension: acdoc_eew_dummy {
		label: "Acdoc Eew Dummy"
		description: ""
		type: string
		sql: ${TABLE}.ACDOC_EEW_DUMMY ;;
	}
	dimension: dummy_incl_eew_cobl {
		label: "Dummy Incl Eew Cobl"
		description: ""
		type: string
		sql: ${TABLE}.DUMMY_INCL_EEW_COBL ;;
	}
	dimension: fup_action {
		label: "Fup Action"
		description: ""
		type: string
		sql: ${TABLE}.FUP_ACTION ;;
	}
	dimension: mig_source {
		label: "Mig Source"
		description: ""
		type: string
		sql: ${TABLE}.MIG_SOURCE ;;
	}
	dimension: mig_docln {
		label: "Mig Docln"
		description: ""
		type: string
		sql: ${TABLE}.MIG_DOCLN ;;
	}
	dimension: dataaging {
		label: "Dataaging"
		description: ""
		type: string
		sql: ${TABLE}._DATAAGING ;;
	}
	dimension: src_awtyp {
		label: "Src Awtyp"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWTYP ;;
	}
	dimension: src_awsys {
		label: "Src Awsys"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWSYS ;;
	}
	dimension: src_aworg {
		label: "Src Aworg"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWORG ;;
	}
	dimension: src_awref {
		label: "Src Awref"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWREF ;;
	}
	dimension: src_awitem {
		label: "Src Awitem"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWITEM ;;
	}
	dimension: src_awsubit {
		label: "Src Awsubit"
		description: ""
		type: string
		sql: ${TABLE}.SRC_AWSUBIT ;;
	}
	dimension: xcommitment {
		label: "Xcommitment"
		description: ""
		type: string
		sql: ${TABLE}.XCOMMITMENT ;;
	}
	dimension: obs_reason {
		label: "Obs Reason"
		description: ""
		type: string
		sql: ${TABLE}.OBS_REASON ;;
	}
	dimension: bslalt {
		label: "Bslalt"
		description: ""
		type: number
		sql: ${TABLE}.BSLALT ;;
	}
	dimension: cslalt {
		label: "Cslalt"
		description: ""
		type: number
		sql: ${TABLE}.CSLALT ;;
	}
	dimension: dslalt {
		label: "Dslalt"
		description: ""
		type: number
		sql: ${TABLE}.DSLALT ;;
	}
	dimension: eslalt {
		label: "Eslalt"
		description: ""
		type: number
		sql: ${TABLE}.ESLALT ;;
	}
	dimension: fslalt {
		label: "Fslalt"
		description: ""
		type: number
		sql: ${TABLE}.FSLALT ;;
	}
	dimension: gslalt {
		label: "Gslalt"
		description: ""
		type: number
		sql: ${TABLE}.GSLALT ;;
	}
	dimension: bslext {
		label: "Bslext"
		description: ""
		type: number
		sql: ${TABLE}.BSLEXT ;;
	}
	dimension: cslext {
		label: "Cslext"
		description: ""
		type: number
		sql: ${TABLE}.CSLEXT ;;
	}
	dimension: dslext {
		label: "Dslext"
		description: ""
		type: number
		sql: ${TABLE}.DSLEXT ;;
	}
	dimension: eslext {
		label: "Eslext"
		description: ""
		type: number
		sql: ${TABLE}.ESLEXT ;;
	}
	dimension: fslext {
		label: "Fslext"
		description: ""
		type: number
		sql: ${TABLE}.FSLEXT ;;
	}
	dimension: gslext {
		label: "Gslext"
		description: ""
		type: number
		sql: ${TABLE}.GSLEXT ;;
	}
	dimension: coco_num {
		label: "Coco Num"
		description: ""
		type: string
		sql: ${TABLE}.COCO_NUM ;;
	}
	dimension: ps_prj_pnr {
		label: "Ps Prj Pnr"
		description: ""
		type: string
		sql: ${TABLE}.PS_PRJ_PNR ;;
	}
	dimension: pps_psp_pnr {
		label: "Pps Psp Pnr"
		description: ""
		type: string
		sql: ${TABLE}.PPS_PSP_PNR ;;
	}
	dimension: pps_prj_pnr {
		label: "Pps Prj Pnr"
		description: ""
		type: string
		sql: ${TABLE}.PPS_PRJ_PNR ;;
	}
	dimension: dummy_mrkt_sgmnt_eew_ps {
		label: "Dummy Mrkt Sgmnt Eew Ps"
		description: ""
		type: string
		sql: ${TABLE}.DUMMY_MRKT_SGMNT_EEW_PS ;;
	}
	dimension: billm {
		label: "Billm"
		description: ""
		type: string
		sql: ${TABLE}.BILLM ;;
	}
	dimension: pom {
		label: "Pom"
		description: ""
		type: string
		sql: ${TABLE}.POM ;;
	}
	dimension: cbrunid {
		label: "Cbrunid"
		description: ""
		type: number
		sql: ${TABLE}.CBRUNID ;;
	}
	dimension: jvactivity {
		label: "Jvactivity"
		description: ""
		type: string
		sql: ${TABLE}.JVACTIVITY ;;
	}
	dimension: pvname {
		label: "Pvname"
		description: ""
		type: string
		sql: ${TABLE}.PVNAME ;;
	}
	dimension: pegrup {
		label: "Pegrup"
		description: ""
		type: string
		sql: ${TABLE}.PEGRUP ;;
	}
	dimension: s_recind {
		label: "S Recind"
		description: ""
		type: string
		sql: ${TABLE}.S_RECIND ;;
	}
	dimension: cbracct {
		label: "Cbracct"
		description: ""
		type: string
		sql: ${TABLE}.CBRACCT ;;
	}
	dimension: cbobjnr {
		label: "Cbobjnr"
		description: ""
		type: string
		sql: ${TABLE}.CBOBJNR ;;
	}
	dimension: acrobjtype {
		label: "Acrobjtype"
		description: ""
		type: string
		sql: ${TABLE}.ACROBJTYPE ;;
	}
	dimension: acrobj_id {
		label: "Acrobj Id"
		description: ""
		type: string
		sql: ${TABLE}.ACROBJ_ID ;;
	}
	dimension: acrsobj_id {
		label: "Acrsobj Id"
		description: ""
		type: string
		sql: ${TABLE}.ACRSOBJ_ID ;;
	}
	dimension: acritmtype {
		label: "Acritmtype"
		description: ""
		type: string
		sql: ${TABLE}.ACRITMTYPE ;;
	}
	dimension: valobjtype {
		label: "Valobjtype"
		description: ""
		type: string
		sql: ${TABLE}.VALOBJTYPE ;;
	}
	dimension: valobj_id {
		label: "Valobj Id"
		description: ""
		type: string
		sql: ${TABLE}.VALOBJ_ID ;;
	}
	dimension: valsobj_id {
		label: "Valsobj Id"
		description: ""
		type: string
		sql: ${TABLE}.VALSOBJ_ID ;;
	}
	dimension: netdt {
		label: "Netdt"
		description: ""
		type: string
		sql: ${TABLE}.NETDT ;;
	}
	dimension: risk_class {
		label: "Risk Class"
		description: ""
		type: string
		sql: ${TABLE}.RISK_CLASS ;;
	}




}
