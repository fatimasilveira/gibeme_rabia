# View file auto-generated with booker
# 2021-06-09 17:43:54.920566

view: mara {

	sql_table_name: "MARA" ;;

	dimension: mandt {
		label: "Mandt"
		description: ""
		type: string
		sql: ${TABLE}.MANDT ;;
	}
	dimension: matnr {
		label: "Matnr"
		description: ""
		type: string
		sql: ${TABLE}.MATNR ;;
	}
	dimension: ersda {
		label: "Ersda"
		description: ""
		type: string
		sql: ${TABLE}.ERSDA ;;
	}
	dimension: ernam {
		label: "Ernam"
		description: ""
		type: string
		sql: ${TABLE}.ERNAM ;;
	}
	dimension: laeda {
		label: "Laeda"
		description: ""
		type: string
		sql: ${TABLE}.LAEDA ;;
	}
	dimension: aenam {
		label: "Aenam"
		description: ""
		type: string
		sql: ${TABLE}.AENAM ;;
	}
	dimension: vpsta {
		label: "Vpsta"
		description: ""
		type: string
		sql: ${TABLE}.VPSTA ;;
	}
	dimension: pstat {
		label: "Pstat"
		description: ""
		type: string
		sql: ${TABLE}.PSTAT ;;
	}
	dimension: lvorm {
		label: "Lvorm"
		description: ""
		type: string
		sql: ${TABLE}.LVORM ;;
	}
	dimension: mtart {
		label: "Mtart"
		description: ""
		type: string
		sql: ${TABLE}.MTART ;;
	}
	dimension: mbrsh {
		label: "Mbrsh"
		description: ""
		type: string
		sql: ${TABLE}.MBRSH ;;
	}
	dimension: matkl {
		label: "Matkl"
		description: ""
		type: string
		sql: ${TABLE}.MATKL ;;
	}
	dimension: bismt {
		label: "Bismt"
		description: ""
		type: string
		sql: ${TABLE}.BISMT ;;
	}
	dimension: meins {
		label: "Meins"
		description: ""
		type: string
		sql: ${TABLE}.MEINS ;;
	}
	dimension: bstme {
		label: "Bstme"
		description: ""
		type: string
		sql: ${TABLE}.BSTME ;;
	}
	dimension: zeinr {
		label: "Zeinr"
		description: ""
		type: string
		sql: ${TABLE}.ZEINR ;;
	}
	dimension: zeiar {
		label: "Zeiar"
		description: ""
		type: string
		sql: ${TABLE}.ZEIAR ;;
	}
	dimension: zeivr {
		label: "Zeivr"
		description: ""
		type: string
		sql: ${TABLE}.ZEIVR ;;
	}
	dimension: zeifo {
		label: "Zeifo"
		description: ""
		type: string
		sql: ${TABLE}.ZEIFO ;;
	}
	dimension: aeszn {
		label: "Aeszn"
		description: ""
		type: string
		sql: ${TABLE}.AESZN ;;
	}
	dimension: blatt {
		label: "Blatt"
		description: ""
		type: string
		sql: ${TABLE}.BLATT ;;
	}
	dimension: blanz {
		label: "Blanz"
		description: ""
		type: string
		sql: ${TABLE}.BLANZ ;;
	}
	dimension: ferth {
		label: "Ferth"
		description: ""
		type: string
		sql: ${TABLE}.FERTH ;;
	}
	dimension: formt {
		label: "Formt"
		description: ""
		type: string
		sql: ${TABLE}.FORMT ;;
	}
	dimension: groes {
		label: "Groes"
		description: ""
		type: string
		sql: ${TABLE}.GROES ;;
	}
	dimension: wrkst {
		label: "Wrkst"
		description: ""
		type: string
		sql: ${TABLE}.WRKST ;;
	}
	dimension: normt {
		label: "Normt"
		description: ""
		type: string
		sql: ${TABLE}.NORMT ;;
	}
	dimension: labor {
		label: "Labor"
		description: ""
		type: string
		sql: ${TABLE}.LABOR ;;
	}
	dimension: ekwsl {
		label: "Ekwsl"
		description: ""
		type: string
		sql: ${TABLE}.EKWSL ;;
	}
	dimension: brgew {
		label: "Brgew"
		description: ""
		type: number
		sql: ${TABLE}.BRGEW ;;
	}
	dimension: ntgew {
		label: "Ntgew"
		description: ""
		type: number
		sql: ${TABLE}.NTGEW ;;
	}
	dimension: gewei {
		label: "Gewei"
		description: ""
		type: string
		sql: ${TABLE}.GEWEI ;;
	}
	dimension: volum {
		label: "Volum"
		description: ""
		type: number
		sql: ${TABLE}.VOLUM ;;
	}
	dimension: voleh {
		label: "Voleh"
		description: ""
		type: string
		sql: ${TABLE}.VOLEH ;;
	}
	dimension: behvo {
		label: "Behvo"
		description: ""
		type: string
		sql: ${TABLE}.BEHVO ;;
	}
	dimension: raube {
		label: "Raube"
		description: ""
		type: string
		sql: ${TABLE}.RAUBE ;;
	}
	dimension: tempb {
		label: "Tempb"
		description: ""
		type: string
		sql: ${TABLE}.TEMPB ;;
	}
	dimension: disst {
		label: "Disst"
		description: ""
		type: string
		sql: ${TABLE}.DISST ;;
	}
	dimension: tragr {
		label: "Tragr"
		description: ""
		type: string
		sql: ${TABLE}.TRAGR ;;
	}
	dimension: stoff {
		label: "Stoff"
		description: ""
		type: string
		sql: ${TABLE}.STOFF ;;
	}
	dimension: spart {
		label: "Spart"
		description: ""
		type: string
		sql: ${TABLE}.SPART ;;
	}
	dimension: kunnr {
		label: "Kunnr"
		description: ""
		type: string
		sql: ${TABLE}.KUNNR ;;
	}
	dimension: eannr {
		label: "Eannr"
		description: ""
		type: string
		sql: ${TABLE}.EANNR ;;
	}
	dimension: wesch {
		label: "Wesch"
		description: ""
		type: number
		sql: ${TABLE}.WESCH ;;
	}
	dimension: bwvor {
		label: "Bwvor"
		description: ""
		type: string
		sql: ${TABLE}.BWVOR ;;
	}
	dimension: bwscl {
		label: "Bwscl"
		description: ""
		type: string
		sql: ${TABLE}.BWSCL ;;
	}
	dimension: saiso {
		label: "Saiso"
		description: ""
		type: string
		sql: ${TABLE}.SAISO ;;
	}
	dimension: etiar {
		label: "Etiar"
		description: ""
		type: string
		sql: ${TABLE}.ETIAR ;;
	}
	dimension: etifo {
		label: "Etifo"
		description: ""
		type: string
		sql: ${TABLE}.ETIFO ;;
	}
	dimension: entar {
		label: "Entar"
		description: ""
		type: string
		sql: ${TABLE}.ENTAR ;;
	}
	dimension: ean11 {
		label: "Ean11"
		description: ""
		type: string
		sql: ${TABLE}.EAN11 ;;
	}
	dimension: numtp {
		label: "Numtp"
		description: ""
		type: string
		sql: ${TABLE}.NUMTP ;;
	}
	dimension: laeng {
		label: "Laeng"
		description: ""
		type: number
		sql: ${TABLE}.LAENG ;;
	}
	dimension: breit {
		label: "Breit"
		description: ""
		type: number
		sql: ${TABLE}.BREIT ;;
	}
	dimension: hoehe {
		label: "Hoehe"
		description: ""
		type: number
		sql: ${TABLE}.HOEHE ;;
	}
	dimension: meabm {
		label: "Meabm"
		description: ""
		type: string
		sql: ${TABLE}.MEABM ;;
	}
	dimension: prdha {
		label: "Prdha"
		description: ""
		type: string
		sql: ${TABLE}.PRDHA ;;
	}
	dimension: aeklk {
		label: "Aeklk"
		description: ""
		type: string
		sql: ${TABLE}.AEKLK ;;
	}
	dimension: cadkz {
		label: "Cadkz"
		description: ""
		type: string
		sql: ${TABLE}.CADKZ ;;
	}
	dimension: qmpur {
		label: "Qmpur"
		description: ""
		type: string
		sql: ${TABLE}.QMPUR ;;
	}
	dimension: ergew {
		label: "Ergew"
		description: ""
		type: number
		sql: ${TABLE}.ERGEW ;;
	}
	dimension: ergei {
		label: "Ergei"
		description: ""
		type: string
		sql: ${TABLE}.ERGEI ;;
	}
	dimension: ervol {
		label: "Ervol"
		description: ""
		type: number
		sql: ${TABLE}.ERVOL ;;
	}
	dimension: ervoe {
		label: "Ervoe"
		description: ""
		type: string
		sql: ${TABLE}.ERVOE ;;
	}
	dimension: gewto {
		label: "Gewto"
		description: ""
		type: number
		sql: ${TABLE}.GEWTO ;;
	}
	dimension: volto {
		label: "Volto"
		description: ""
		type: number
		sql: ${TABLE}.VOLTO ;;
	}
	dimension: vabme {
		label: "Vabme"
		description: ""
		type: string
		sql: ${TABLE}.VABME ;;
	}
	dimension: kzrev {
		label: "Kzrev"
		description: ""
		type: string
		sql: ${TABLE}.KZREV ;;
	}
	dimension: kzkfg {
		label: "Kzkfg"
		description: ""
		type: string
		sql: ${TABLE}.KZKFG ;;
	}
	dimension: xchpf {
		label: "Xchpf"
		description: ""
		type: string
		sql: ${TABLE}.XCHPF ;;
	}
	dimension: vhart {
		label: "Vhart"
		description: ""
		type: string
		sql: ${TABLE}.VHART ;;
	}
	dimension: fuelg {
		label: "Fuelg"
		description: ""
		type: number
		sql: ${TABLE}.FUELG ;;
	}
	dimension: stfak {
		label: "Stfak"
		description: ""
		type: number
		sql: ${TABLE}.STFAK ;;
	}
	dimension: magrv {
		label: "Magrv"
		description: ""
		type: string
		sql: ${TABLE}.MAGRV ;;
	}
	dimension: begru {
		label: "Begru"
		description: ""
		type: string
		sql: ${TABLE}.BEGRU ;;
	}
	dimension: datab {
		label: "Datab"
		description: ""
		type: string
		sql: ${TABLE}.DATAB ;;
	}
	dimension: liqdt {
		label: "Liqdt"
		description: ""
		type: string
		sql: ${TABLE}.LIQDT ;;
	}
	dimension: saisj {
		label: "Saisj"
		description: ""
		type: string
		sql: ${TABLE}.SAISJ ;;
	}
	dimension: plgtp {
		label: "Plgtp"
		description: ""
		type: string
		sql: ${TABLE}.PLGTP ;;
	}
	dimension: mlgut {
		label: "Mlgut"
		description: ""
		type: string
		sql: ${TABLE}.MLGUT ;;
	}
	dimension: extwg {
		label: "Extwg"
		description: ""
		type: string
		sql: ${TABLE}.EXTWG ;;
	}
	dimension: satnr {
		label: "Satnr"
		description: ""
		type: string
		sql: ${TABLE}.SATNR ;;
	}
	dimension: attyp {
		label: "Attyp"
		description: ""
		type: string
		sql: ${TABLE}.ATTYP ;;
	}
	dimension: kzkup {
		label: "Kzkup"
		description: ""
		type: string
		sql: ${TABLE}.KZKUP ;;
	}
	dimension: kznfm {
		label: "Kznfm"
		description: ""
		type: string
		sql: ${TABLE}.KZNFM ;;
	}
	dimension: pmata {
		label: "Pmata"
		description: ""
		type: string
		sql: ${TABLE}.PMATA ;;
	}
	dimension: mstae {
		label: "Mstae"
		description: ""
		type: string
		sql: ${TABLE}.MSTAE ;;
	}
	dimension: mstav {
		label: "Mstav"
		description: ""
		type: string
		sql: ${TABLE}.MSTAV ;;
	}
	dimension: mstde {
		label: "Mstde"
		description: ""
		type: string
		sql: ${TABLE}.MSTDE ;;
	}
	dimension: mstdv {
		label: "Mstdv"
		description: ""
		type: string
		sql: ${TABLE}.MSTDV ;;
	}
	dimension: taklv {
		label: "Taklv"
		description: ""
		type: string
		sql: ${TABLE}.TAKLV ;;
	}
	dimension: rbnrm {
		label: "Rbnrm"
		description: ""
		type: string
		sql: ${TABLE}.RBNRM ;;
	}
	dimension: mhdrz {
		label: "Mhdrz"
		description: ""
		type: number
		sql: ${TABLE}.MHDRZ ;;
	}
	dimension: mhdhb {
		label: "Mhdhb"
		description: ""
		type: number
		sql: ${TABLE}.MHDHB ;;
	}
	dimension: mhdlp {
		label: "Mhdlp"
		description: ""
		type: number
		sql: ${TABLE}.MHDLP ;;
	}
	dimension: inhme {
		label: "Inhme"
		description: ""
		type: string
		sql: ${TABLE}.INHME ;;
	}
	dimension: inhal {
		label: "Inhal"
		description: ""
		type: number
		sql: ${TABLE}.INHAL ;;
	}
	dimension: vpreh {
		label: "Vpreh"
		description: ""
		type: number
		sql: ${TABLE}.VPREH ;;
	}
	dimension: etiag {
		label: "Etiag"
		description: ""
		type: string
		sql: ${TABLE}.ETIAG ;;
	}
	dimension: inhbr {
		label: "Inhbr"
		description: ""
		type: number
		sql: ${TABLE}.INHBR ;;
	}
	dimension: cmeth {
		label: "Cmeth"
		description: ""
		type: string
		sql: ${TABLE}.CMETH ;;
	}
	dimension: cuobf {
		label: "Cuobf"
		description: ""
		type: string
		sql: ${TABLE}.CUOBF ;;
	}
	dimension: kzumw {
		label: "Kzumw"
		description: ""
		type: string
		sql: ${TABLE}.KZUMW ;;
	}
	dimension: kosch {
		label: "Kosch"
		description: ""
		type: string
		sql: ${TABLE}.KOSCH ;;
	}
	dimension: sprof {
		label: "Sprof"
		description: ""
		type: string
		sql: ${TABLE}.SPROF ;;
	}
	dimension: nrfhg {
		label: "Nrfhg"
		description: ""
		type: string
		sql: ${TABLE}.NRFHG ;;
	}
	dimension: mfrpn {
		label: "Mfrpn"
		description: ""
		type: string
		sql: ${TABLE}.MFRPN ;;
	}
	dimension: mfrnr {
		label: "Mfrnr"
		description: ""
		type: string
		sql: ${TABLE}.MFRNR ;;
	}
	dimension: bmatn {
		label: "Bmatn"
		description: ""
		type: string
		sql: ${TABLE}.BMATN ;;
	}
	dimension: mprof {
		label: "Mprof"
		description: ""
		type: string
		sql: ${TABLE}.MPROF ;;
	}
	dimension: kzwsm {
		label: "Kzwsm"
		description: ""
		type: string
		sql: ${TABLE}.KZWSM ;;
	}
	dimension: saity {
		label: "Saity"
		description: ""
		type: string
		sql: ${TABLE}.SAITY ;;
	}
	dimension: profl {
		label: "Profl"
		description: ""
		type: string
		sql: ${TABLE}.PROFL ;;
	}
	dimension: ihivi {
		label: "Ihivi"
		description: ""
		type: string
		sql: ${TABLE}.IHIVI ;;
	}
	dimension: iloos {
		label: "Iloos"
		description: ""
		type: string
		sql: ${TABLE}.ILOOS ;;
	}
	dimension: serlv {
		label: "Serlv"
		description: ""
		type: string
		sql: ${TABLE}.SERLV ;;
	}
	dimension: kzgvh {
		label: "Kzgvh"
		description: ""
		type: string
		sql: ${TABLE}.KZGVH ;;
	}
	dimension: xgchp {
		label: "Xgchp"
		description: ""
		type: string
		sql: ${TABLE}.XGCHP ;;
	}
	dimension: kzeff {
		label: "Kzeff"
		description: ""
		type: string
		sql: ${TABLE}.KZEFF ;;
	}
	dimension: compl {
		label: "Compl"
		description: ""
		type: string
		sql: ${TABLE}.COMPL ;;
	}
	dimension: iprkz {
		label: "Iprkz"
		description: ""
		type: string
		sql: ${TABLE}.IPRKZ ;;
	}
	dimension: rdmhd {
		label: "Rdmhd"
		description: ""
		type: string
		sql: ${TABLE}.RDMHD ;;
	}
	dimension: przus {
		label: "Przus"
		description: ""
		type: string
		sql: ${TABLE}.PRZUS ;;
	}
	dimension: mtpos_mara {
		label: "Mtpos Mara"
		description: ""
		type: string
		sql: ${TABLE}.MTPOS_MARA ;;
	}
	dimension: bflme {
		label: "Bflme"
		description: ""
		type: string
		sql: ${TABLE}.BFLME ;;
	}
	dimension: matfi {
		label: "Matfi"
		description: ""
		type: string
		sql: ${TABLE}.MATFI ;;
	}
	dimension: cmrel {
		label: "Cmrel"
		description: ""
		type: string
		sql: ${TABLE}.CMREL ;;
	}
	dimension: bbtyp {
		label: "Bbtyp"
		description: ""
		type: string
		sql: ${TABLE}.BBTYP ;;
	}
	dimension: sled_bbd {
		label: "Sled Bbd"
		description: ""
		type: string
		sql: ${TABLE}.SLED_BBD ;;
	}
	dimension: gtin_variant {
		label: "Gtin Variant"
		description: ""
		type: string
		sql: ${TABLE}.GTIN_VARIANT ;;
	}
	dimension: gennr {
		label: "Gennr"
		description: ""
		type: string
		sql: ${TABLE}.GENNR ;;
	}
	dimension: rmatp {
		label: "Rmatp"
		description: ""
		type: string
		sql: ${TABLE}.RMATP ;;
	}
	dimension: gds_relevant {
		label: "Gds Relevant"
		description: ""
		type: string
		sql: ${TABLE}.GDS_RELEVANT ;;
	}
	dimension: weora {
		label: "Weora"
		description: ""
		type: string
		sql: ${TABLE}.WEORA ;;
	}
	dimension: hutyp_dflt {
		label: "Hutyp Dflt"
		description: ""
		type: string
		sql: ${TABLE}.HUTYP_DFLT ;;
	}
	dimension: pilferable {
		label: "Pilferable"
		description: ""
		type: string
		sql: ${TABLE}.PILFERABLE ;;
	}
	dimension: whstc {
		label: "Whstc"
		description: ""
		type: string
		sql: ${TABLE}.WHSTC ;;
	}
	dimension: whmatgr {
		label: "Whmatgr"
		description: ""
		type: string
		sql: ${TABLE}.WHMATGR ;;
	}
	dimension: hndlcode {
		label: "Hndlcode"
		description: ""
		type: string
		sql: ${TABLE}.HNDLCODE ;;
	}
	dimension: hazmat {
		label: "Hazmat"
		description: ""
		type: string
		sql: ${TABLE}.HAZMAT ;;
	}
	dimension: hutyp {
		label: "Hutyp"
		description: ""
		type: string
		sql: ${TABLE}.HUTYP ;;
	}
	dimension: tare_var {
		label: "Tare Var"
		description: ""
		type: string
		sql: ${TABLE}.TARE_VAR ;;
	}
	dimension: maxc {
		label: "Maxc"
		description: ""
		type: number
		sql: ${TABLE}.MAXC ;;
	}
	dimension: maxc_tol {
		label: "Maxc Tol"
		description: ""
		type: number
		sql: ${TABLE}.MAXC_TOL ;;
	}
	dimension: maxl {
		label: "Maxl"
		description: ""
		type: number
		sql: ${TABLE}.MAXL ;;
	}
	dimension: maxb {
		label: "Maxb"
		description: ""
		type: number
		sql: ${TABLE}.MAXB ;;
	}
	dimension: maxh {
		label: "Maxh"
		description: ""
		type: number
		sql: ${TABLE}.MAXH ;;
	}
	dimension: maxdim_uom {
		label: "Maxdim Uom"
		description: ""
		type: string
		sql: ${TABLE}.MAXDIM_UOM ;;
	}
	dimension: herkl {
		label: "Herkl"
		description: ""
		type: string
		sql: ${TABLE}.HERKL ;;
	}
	dimension: mfrgr {
		label: "Mfrgr"
		description: ""
		type: string
		sql: ${TABLE}.MFRGR ;;
	}
	dimension: qqtime {
		label: "Qqtime"
		description: ""
		type: number
		sql: ${TABLE}.QQTIME ;;
	}
	dimension: qqtimeuom {
		label: "Qqtimeuom"
		description: ""
		type: string
		sql: ${TABLE}.QQTIMEUOM ;;
	}
	dimension: qgrp {
		label: "Qgrp"
		description: ""
		type: string
		sql: ${TABLE}.QGRP ;;
	}
	dimension: serial {
		label: "Serial"
		description: ""
		type: string
		sql: ${TABLE}.SERIAL ;;
	}
	dimension: ps_smartform {
		label: "Ps Smartform"
		description: ""
		type: string
		sql: ${TABLE}.PS_SMARTFORM ;;
	}
	dimension: logunit {
		label: "Logunit"
		description: ""
		type: string
		sql: ${TABLE}.LOGUNIT ;;
	}
	dimension: cwqrel {
		label: "Cwqrel"
		description: ""
		type: string
		sql: ${TABLE}.CWQREL ;;
	}
	dimension: cwqproc {
		label: "Cwqproc"
		description: ""
		type: string
		sql: ${TABLE}.CWQPROC ;;
	}
	dimension: cwqtolgr {
		label: "Cwqtolgr"
		description: ""
		type: string
		sql: ${TABLE}.CWQTOLGR ;;
	}
	dimension: adprof {
		label: "Adprof"
		description: ""
		type: string
		sql: ${TABLE}.ADPROF ;;
	}
	dimension: ipmipproduct {
		label: "Ipmipproduct"
		description: ""
		type: string
		sql: ${TABLE}.IPMIPPRODUCT ;;
	}
	dimension: allow_pmat_igno {
		label: "Allow Pmat Igno"
		description: ""
		type: string
		sql: ${TABLE}.ALLOW_PMAT_IGNO ;;
	}
	dimension: medium {
		label: "Medium"
		description: ""
		type: string
		sql: ${TABLE}.MEDIUM ;;
	}
	dimension: commodity {
		label: "Commodity"
		description: ""
		type: string
		sql: ${TABLE}.COMMODITY ;;
	}
	dimension: animal_origin {
		label: "Animal Origin"
		description: ""
		type: string
		sql: ${TABLE}.ANIMAL_ORIGIN ;;
	}
	dimension: textile_comp_ind {
		label: "Textile Comp Ind"
		description: ""
		type: string
		sql: ${TABLE}.TEXTILE_COMP_IND ;;
	}
	dimension: last_changed_time {
		label: "Last Changed Time"
		description: ""
		type: string
		sql: ${TABLE}.LAST_CHANGED_TIME ;;
	}
	dimension: sgt_csgr {
		label: "Sgt Csgr"
		description: ""
		type: string
		sql: ${TABLE}.SGT_CSGR ;;
	}
	dimension: sgt_covsa {
		label: "Sgt Covsa"
		description: ""
		type: string
		sql: ${TABLE}.SGT_COVSA ;;
	}
	dimension: sgt_stat {
		label: "Sgt Stat"
		description: ""
		type: string
		sql: ${TABLE}.SGT_STAT ;;
	}
	dimension: sgt_scope {
		label: "Sgt Scope"
		description: ""
		type: string
		sql: ${TABLE}.SGT_SCOPE ;;
	}
	dimension: sgt_rel {
		label: "Sgt Rel"
		description: ""
		type: string
		sql: ${TABLE}.SGT_REL ;;
	}
	dimension: anp {
		label: "Anp"
		description: ""
		type: string
		sql: ${TABLE}.ANP ;;
	}
	dimension: psm_code {
		label: "Psm Code"
		description: ""
		type: string
		sql: ${TABLE}.PSM_CODE ;;
	}
	dimension: fsh_mg_at1 {
		label: "Fsh Mg At1"
		description: ""
		type: string
		sql: ${TABLE}.FSH_MG_AT1 ;;
	}
	dimension: fsh_mg_at2 {
		label: "Fsh Mg At2"
		description: ""
		type: string
		sql: ${TABLE}.FSH_MG_AT2 ;;
	}
	dimension: fsh_mg_at3 {
		label: "Fsh Mg At3"
		description: ""
		type: string
		sql: ${TABLE}.FSH_MG_AT3 ;;
	}
	dimension: fsh_sealv {
		label: "Fsh Sealv"
		description: ""
		type: string
		sql: ${TABLE}.FSH_SEALV ;;
	}
	dimension: fsh_seaim {
		label: "Fsh Seaim"
		description: ""
		type: string
		sql: ${TABLE}.FSH_SEAIM ;;
	}
	dimension: fsh_sc_mid {
		label: "Fsh Sc Mid"
		description: ""
		type: string
		sql: ${TABLE}.FSH_SC_MID ;;
	}
	dimension: dummy_prd_incl_eew_ps {
		label: "Dummy Prd Incl Eew Ps"
		description: ""
		type: string
		sql: ${TABLE}.DUMMY_PRD_INCL_EEW_PS ;;
	}
	# FIXME: BLOB did not match any rules for conversion
	dimension: scm_matid_guid16 {
		label: "Scm Matid Guid16"
		description: ""
		type: BLOB
		sql: ${TABLE}.SCM_MATID_GUID16 ;;
	}
	dimension: scm_matid_guid22 {
		label: "Scm Matid Guid22"
		description: ""
		type: string
		sql: ${TABLE}.SCM_MATID_GUID22 ;;
	}
	dimension: scm_maturity_dur {
		label: "Scm Maturity Dur"
		description: ""
		type: number
		sql: ${TABLE}.SCM_MATURITY_DUR ;;
	}
	dimension: scm_shlf_lfe_req_min {
		label: "Scm Shlf Lfe Req Min"
		description: ""
		type: number
		sql: ${TABLE}.SCM_SHLF_LFE_REQ_MIN ;;
	}
	dimension: scm_shlf_lfe_req_max {
		label: "Scm Shlf Lfe Req Max"
		description: ""
		type: number
		sql: ${TABLE}.SCM_SHLF_LFE_REQ_MAX ;;
	}
	dimension: scm_puom {
		label: "Scm Puom"
		description: ""
		type: string
		sql: ${TABLE}.SCM_PUOM ;;
	}
	dimension: cwm_xcwmat {
		label: "Cwm Xcwmat"
		description: ""
		type: string
		sql: ${TABLE}./CWM/XCWMAT ;;
	}
	dimension: cwm_valum {
		label: "Cwm Valum"
		description: ""
		type: string
		sql: ${TABLE}./CWM/VALUM ;;
	}
	dimension: cwm_tolgr {
		label: "Cwm Tolgr"
		description: ""
		type: string
		sql: ${TABLE}./CWM/TOLGR ;;
	}
	dimension: cwm_tara {
		label: "Cwm Tara"
		description: ""
		type: string
		sql: ${TABLE}./CWM/TARA ;;
	}
	dimension: cwm_tarum {
		label: "Cwm Tarum"
		description: ""
		type: string
		sql: ${TABLE}./CWM/TARUM ;;
	}
	dimension: bev1_luleinh {
		label: "Bev1 Luleinh"
		description: ""
		type: string
		sql: ${TABLE}./BEV1/LULEINH ;;
	}
	dimension: bev1_luldegrp {
		label: "Bev1 Luldegrp"
		description: ""
		type: string
		sql: ${TABLE}./BEV1/LULDEGRP ;;
	}
	dimension: bev1_nestruccat {
		label: "Bev1 Nestruccat"
		description: ""
		type: string
		sql: ${TABLE}./BEV1/NESTRUCCAT ;;
	}
	dimension: dsd_sl_toltyp {
		label: "Dsd Sl Toltyp"
		description: ""
		type: string
		sql: ${TABLE}./DSD/SL_TOLTYP ;;
	}
	dimension: dsd_sv_cnt_grp {
		label: "Dsd Sv Cnt Grp"
		description: ""
		type: string
		sql: ${TABLE}./DSD/SV_CNT_GRP ;;
	}
	dimension: dsd_vc_group {
		label: "Dsd Vc Group"
		description: ""
		type: string
		sql: ${TABLE}./DSD/VC_GROUP ;;
	}
	dimension: sapmp_kadu {
		label: "Sapmp Kadu"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/KADU ;;
	}
	dimension: sapmp_abmein {
		label: "Sapmp Abmein"
		description: ""
		type: string
		sql: ${TABLE}./SAPMP/ABMEIN ;;
	}
	dimension: sapmp_kadp {
		label: "Sapmp Kadp"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/KADP ;;
	}
	dimension: sapmp_brad {
		label: "Sapmp Brad"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/BRAD ;;
	}
	dimension: sapmp_spbi {
		label: "Sapmp Spbi"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/SPBI ;;
	}
	dimension: sapmp_trad {
		label: "Sapmp Trad"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/TRAD ;;
	}
	dimension: sapmp_kedu {
		label: "Sapmp Kedu"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/KEDU ;;
	}
	dimension: sapmp_sptr {
		label: "Sapmp Sptr"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/SPTR ;;
	}
	dimension: sapmp_fbdk {
		label: "Sapmp Fbdk"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/FBDK ;;
	}
	dimension: sapmp_fbhk {
		label: "Sapmp Fbhk"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/FBHK ;;
	}
	dimension: sapmp_rili {
		label: "Sapmp Rili"
		description: ""
		type: string
		sql: ${TABLE}./SAPMP/RILI ;;
	}
	dimension: sapmp_fbak {
		label: "Sapmp Fbak"
		description: ""
		type: string
		sql: ${TABLE}./SAPMP/FBAK ;;
	}
	dimension: sapmp_aho {
		label: "Sapmp Aho"
		description: ""
		type: string
		sql: ${TABLE}./SAPMP/AHO ;;
	}
	dimension: sapmp_mifrr {
		label: "Sapmp Mifrr"
		description: ""
		type: number
		sql: ${TABLE}./SAPMP/MIFRR ;;
	}
	dimension: vso_r_tilt_ind {
		label: "Vso R Tilt Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_TILT_IND ;;
	}
	dimension: vso_r_stack_ind {
		label: "Vso R Stack Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_STACK_IND ;;
	}
	dimension: vso_r_bot_ind {
		label: "Vso R Bot Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_BOT_IND ;;
	}
	dimension: vso_r_top_ind {
		label: "Vso R Top Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_TOP_IND ;;
	}
	dimension: vso_r_stack_no {
		label: "Vso R Stack No"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_STACK_NO ;;
	}
	dimension: vso_r_pal_ind {
		label: "Vso R Pal Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_PAL_IND ;;
	}
	dimension: vso_r_pal_ovr_d {
		label: "Vso R Pal Ovr D"
		description: ""
		type: number
		sql: ${TABLE}./VSO/R_PAL_OVR_D ;;
	}
	dimension: vso_r_pal_ovr_w {
		label: "Vso R Pal Ovr W"
		description: ""
		type: number
		sql: ${TABLE}./VSO/R_PAL_OVR_W ;;
	}
	dimension: vso_r_pal_b_ht {
		label: "Vso R Pal B Ht"
		description: ""
		type: number
		sql: ${TABLE}./VSO/R_PAL_B_HT ;;
	}
	dimension: vso_r_pal_min_h {
		label: "Vso R Pal Min H"
		description: ""
		type: number
		sql: ${TABLE}./VSO/R_PAL_MIN_H ;;
	}
	dimension: vso_r_tol_b_ht {
		label: "Vso R Tol B Ht"
		description: ""
		type: number
		sql: ${TABLE}./VSO/R_TOL_B_HT ;;
	}
	dimension: vso_r_no_p_gvh {
		label: "Vso R No P Gvh"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_NO_P_GVH ;;
	}
	dimension: vso_r_quan_unit {
		label: "Vso R Quan Unit"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_QUAN_UNIT ;;
	}
	dimension: vso_r_kzgvh_ind {
		label: "Vso R Kzgvh Ind"
		description: ""
		type: string
		sql: ${TABLE}./VSO/R_KZGVH_IND ;;
	}
	dimension: packcode {
		label: "Packcode"
		description: ""
		type: string
		sql: ${TABLE}.PACKCODE ;;
	}
	dimension: dg_pack_status {
		label: "Dg Pack Status"
		description: ""
		type: string
		sql: ${TABLE}.DG_PACK_STATUS ;;
	}
	dimension: mcond {
		label: "Mcond"
		description: ""
		type: string
		sql: ${TABLE}.MCOND ;;
	}
	dimension: retdelc {
		label: "Retdelc"
		description: ""
		type: string
		sql: ${TABLE}.RETDELC ;;
	}
	dimension: loglev_reto {
		label: "Loglev Reto"
		description: ""
		type: string
		sql: ${TABLE}.LOGLEV_RETO ;;
	}
	dimension: nsnid {
		label: "Nsnid"
		description: ""
		type: string
		sql: ${TABLE}.NSNID ;;
	}
	dimension: ovlpn {
		label: "Ovlpn"
		description: ""
		type: string
		sql: ${TABLE}.OVLPN ;;
	}
	dimension: adspc_spc {
		label: "Adspc Spc"
		description: ""
		type: string
		sql: ${TABLE}.ADSPC_SPC ;;
	}
	# FIXME: BLOB did not match any rules for conversion
	dimension: varid {
		label: "Varid"
		description: ""
		type: BLOB
		sql: ${TABLE}.VARID ;;
	}
	dimension: msbookpartno {
		label: "Msbookpartno"
		description: ""
		type: string
		sql: ${TABLE}.MSBOOKPARTNO ;;
	}
	dimension: dpcbt {
		label: "Dpcbt"
		description: ""
		type: string
		sql: ${TABLE}.DPCBT ;;
	}
	dimension: xgrdt {
		label: "Xgrdt"
		description: ""
		type: string
		sql: ${TABLE}.XGRDT ;;
	}
	dimension: imatn {
		label: "Imatn"
		description: ""
		type: string
		sql: ${TABLE}.IMATN ;;
	}
	dimension: picnum {
		label: "Picnum"
		description: ""
		type: string
		sql: ${TABLE}.PICNUM ;;
	}
	dimension: bstat {
		label: "Bstat"
		description: ""
		type: string
		sql: ${TABLE}.BSTAT ;;
	}
	dimension: color_atinn {
		label: "Color Atinn"
		description: ""
		type: string
		sql: ${TABLE}.COLOR_ATINN ;;
	}
	dimension: size1_atinn {
		label: "Size1 Atinn"
		description: ""
		type: string
		sql: ${TABLE}.SIZE1_ATINN ;;
	}
	dimension: size2_atinn {
		label: "Size2 Atinn"
		description: ""
		type: string
		sql: ${TABLE}.SIZE2_ATINN ;;
	}
	dimension: color {
		label: "Color"
		description: ""
		type: string
		sql: ${TABLE}.COLOR ;;
	}
	dimension: size1 {
		label: "Size1"
		description: ""
		type: string
		sql: ${TABLE}.SIZE1 ;;
	}
	dimension: size2 {
		label: "Size2"
		description: ""
		type: string
		sql: ${TABLE}.SIZE2 ;;
	}
	dimension: free_char {
		label: "Free Char"
		description: ""
		type: string
		sql: ${TABLE}.FREE_CHAR ;;
	}
	dimension: care_code {
		label: "Care Code"
		description: ""
		type: string
		sql: ${TABLE}.CARE_CODE ;;
	}
	dimension: brand_id {
		label: "Brand Id"
		description: ""
		type: string
		sql: ${TABLE}.BRAND_ID ;;
	}
	dimension: fiber_code1 {
		label: "Fiber Code1"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_CODE1 ;;
	}
	dimension: fiber_part1 {
		label: "Fiber Part1"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_PART1 ;;
	}
	dimension: fiber_code2 {
		label: "Fiber Code2"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_CODE2 ;;
	}
	dimension: fiber_part2 {
		label: "Fiber Part2"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_PART2 ;;
	}
	dimension: fiber_code3 {
		label: "Fiber Code3"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_CODE3 ;;
	}
	dimension: fiber_part3 {
		label: "Fiber Part3"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_PART3 ;;
	}
	dimension: fiber_code4 {
		label: "Fiber Code4"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_CODE4 ;;
	}
	dimension: fiber_part4 {
		label: "Fiber Part4"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_PART4 ;;
	}
	dimension: fiber_code5 {
		label: "Fiber Code5"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_CODE5 ;;
	}
	dimension: fiber_part5 {
		label: "Fiber Part5"
		description: ""
		type: string
		sql: ${TABLE}.FIBER_PART5 ;;
	}
	dimension: fashgrd {
		label: "Fashgrd"
		description: ""
		type: string
		sql: ${TABLE}.FASHGRD ;;
	}
	dimension: zz1_customfieldhighris_prd {
		label: "Zz1 Customfieldhighris Prd"
		description: ""
		type: string
		sql: ${TABLE}.ZZ1_CUSTOMFIELDHIGHRIS_PRD ;;
	}
	dimension: zz1_customfieldriskrea_prd {
		label: "Zz1 Customfieldriskrea Prd"
		description: ""
		type: string
		sql: ${TABLE}.ZZ1_CUSTOMFIELDRISKREA_PRD ;;
	}
	dimension: zz1_customfieldriskmit_prd {
		label: "Zz1 Customfieldriskmit Prd"
		description: ""
		type: string
		sql: ${TABLE}.ZZ1_CUSTOMFIELDRISKMIT_PRD ;;
	}
	dimension: srv_dura {
		label: "Srv Dura"
		description: ""
		type: number
		sql: ${TABLE}.SRV_DURA ;;
	}
	dimension: srv_dura_uom {
		label: "Srv Dura Uom"
		description: ""
		type: string
		sql: ${TABLE}.SRV_DURA_UOM ;;
	}
	dimension: srv_serwi {
		label: "Srv Serwi"
		description: ""
		type: string
		sql: ${TABLE}.SRV_SERWI ;;
	}
	dimension: srv_escal {
		label: "Srv Escal"
		description: ""
		type: string
		sql: ${TABLE}.SRV_ESCAL ;;
	}
	dimension: matnr_external {
		label: "Matnr External"
		description: ""
		type: string
		sql: ${TABLE}.MATNR_EXTERNAL ;;
	}
	dimension: rmatp_pb {
		label: "Rmatp Pb"
		description: ""
		type: string
		sql: ${TABLE}.RMATP_PB ;;
	}
	dimension: prod_shape {
		label: "Prod Shape"
		description: ""
		type: string
		sql: ${TABLE}.PROD_SHAPE ;;
	}
	dimension: mo_profile_id {
		label: "Mo Profile Id"
		description: ""
		type: string
		sql: ${TABLE}.MO_PROFILE_ID ;;
	}
	dimension: overhang_tresh {
		label: "Overhang Tresh"
		description: ""
		type: number
		sql: ${TABLE}.OVERHANG_TRESH ;;
	}
	dimension: bridge_tresh {
		label: "Bridge Tresh"
		description: ""
		type: number
		sql: ${TABLE}.BRIDGE_TRESH ;;
	}
	dimension: bridge_max_slope {
		label: "Bridge Max Slope"
		description: ""
		type: number
		sql: ${TABLE}.BRIDGE_MAX_SLOPE ;;
	}
	dimension: height_nonflat {
		label: "Height Nonflat"
		description: ""
		type: number
		sql: ${TABLE}.HEIGHT_NONFLAT ;;
	}
	dimension: height_nonflat_uom {
		label: "Height Nonflat Uom"
		description: ""
		type: string
		sql: ${TABLE}.HEIGHT_NONFLAT_UOM ;;
	}
	dimension: som_cycle {
		label: "Som Cycle"
		description: ""
		type: string
		sql: ${TABLE}.SOM_CYCLE ;;
	}
	dimension: som_cycle_rule {
		label: "Som Cycle Rule"
		description: ""
		type: string
		sql: ${TABLE}.SOM_CYCLE_RULE ;;
	}
	dimension: som_tc_schema {
		label: "Som Tc Schema"
		description: ""
		type: string
		sql: ${TABLE}.SOM_TC_SCHEMA ;;
	}
	dimension: logistical_mat_category {
		label: "Logistical Mat Category"
		description: ""
		type: string
		sql: ${TABLE}.LOGISTICAL_MAT_CATEGORY ;;
	}
	dimension: sales_material {
		label: "Sales Material"
		description: ""
		type: string
		sql: ${TABLE}.SALES_MATERIAL ;;
	}
	dimension: identification_tag_type {
		label: "Identification Tag Type"
		description: ""
		type: string
		sql: ${TABLE}.IDENTIFICATION_TAG_TYPE ;;
	}
	dimension: sttpec_sertype {
		label: "Sttpec Sertype"
		description: ""
		type: number
		sql: ${TABLE}./STTPEC/SERTYPE ;;
	}
	dimension: sttpec_syncact {
		label: "Sttpec Syncact"
		description: ""
		type: string
		sql: ${TABLE}./STTPEC/SYNCACT ;;
	}
	dimension: sttpec_synctime {
		label: "Sttpec Synctime"
		description: ""
		type: number
		sql: ${TABLE}./STTPEC/SYNCTIME ;;
	}
	dimension: sttpec_syncchg {
		label: "Sttpec Syncchg"
		description: ""
		type: string
		sql: ${TABLE}./STTPEC/SYNCCHG ;;
	}
	dimension: sttpec_country_ref {
		label: "Sttpec Country Ref"
		description: ""
		type: string
		sql: ${TABLE}./STTPEC/COUNTRY_REF ;;
	}
	dimension: sttpec_prdcat {
		label: "Sttpec Prdcat"
		description: ""
		type: string
		sql: ${TABLE}./STTPEC/PRDCAT ;;
	}

	# FIXME: add_months("MARA"."ERSDA", 3) did not match any rules for conversion
	# FIXME: Converted from Calculated Column. Please review SQL.
	dimension_group: catittude {
		label: ""
		description: "None"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: add_months("MARA"."ERSDA", 3) ;;
	}

	# FIXME: count("MARA"."MATNR") did not match any rules for conversion
	measure: ameasure {
		description: ""
		type: sum
		sql: count(${TABLE}.MATNR) ;;
	}


}
