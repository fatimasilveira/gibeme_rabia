# View file auto-generated with booker
# 2021-06-09 17:43:55.086451

view: makt {

	sql_table_name: "MAKT" ;;

	dimension: mandt {
		label: "Mandt"
		description: ""
		type: string
		sql: ${TABLE}.MANDT ;;
	}
	dimension: matnr {
		label: "Matnr"
		description: ""
		type: string
		sql: ${TABLE}.MATNR ;;
	}
	dimension: spras {
		label: "Spras"
		description: ""
		type: string
		sql: ${TABLE}.SPRAS ;;
	}
	dimension: maktx {
		label: "Maktx"
		description: ""
		type: string
		sql: ${TABLE}.MAKTX ;;
	}
	dimension: maktg {
		label: "Maktg"
		description: ""
		type: string
		sql: ${TABLE}.MAKTG ;;
	}




}
