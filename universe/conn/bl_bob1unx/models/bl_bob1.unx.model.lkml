# Model file auto-generated with booker
# 2021-06-09 17:43:55.544066
connection: "conn"

# include universe views only
include: "/universe/conn/bl_bob1unx/views/*.view"
include: "/dashboards/**/*.dashboard"


# FIXME: Join mara requires review
	# FIXME: Join mara requires review
explore: makt {

	label: "MAKT"
	view_label: "MAKT"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: mara {
		view_label: "MARA"
		relationship: "many_to_one"
		type: "inner"
		sql_on: MAKT.MANDT=MARA.MANDT ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: mara {
		view_label: "MARA"
		relationship: "many_to_one"
		type: "inner"
		sql_on: MAKT.MATNR=MARA.MATNR ;;
	}
}
# FIXME: Join mara requires review
explore: acdoca {

	label: "ACDOCA"
	view_label: "ACDOCA"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: mara {
		view_label: "MARA"
		relationship: "many_to_one"
		type: "inner"
		sql_on: ACDOCA.RCLNT=MARA.MANDT ;;
	}
}
explore: mara {

	label: "MARA"
	view_label: "MARA"

}
