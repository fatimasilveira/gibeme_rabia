# Model file auto-generated with booker
# 2021-06-09 17:44:05.873980
connection: "efashion-webi"

# include universe views only
include: "/universe/efashionwebi/eFashionunx/views/*.view"
include: "/dashboards/**/*.dashboard"


# FIXME: Join product_promotion_facts requires review
explore: promotion_lookup {

	label: "promotion_lookup"
	view_label: "promotion_lookup"

	# FIXME: Join outer was missing, using default value inner
	join: product_promotion_facts {
		view_label: "product_promotion_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: promotion_lookup.Promotion_id=product_promotion_facts.Promotion_id ;;
	}
}
# FIXME: Join calendar_year_lookup requires review
	# FIXME: Join shop_facts requires review
explore: product_promotion_facts {

	label: "product_promotion_facts"
	view_label: "product_promotion_facts"

	# FIXME: Join outer was missing, using default value inner
	join: calendar_year_lookup {
		view_label: "Calendar_year_lookup"
		relationship: "many_to_one"
		type: "inner"
		sql_on: product_promotion_facts.Week_id=Calendar_year_lookup.Week_id ;;
	}
	# FIXME: Join outer was missing, using default value inner
	join: shop_facts {
		view_label: "Shop_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: product_promotion_facts.Article_id=Shop_facts.Article_id ;;
	}
}
# FIXME: Join shop_facts requires review
explore: outlet_lookup {

	label: "Outlet_Lookup"
	view_label: "Outlet_Lookup"

	# FIXME: Join outer was missing, using default value inner
	join: shop_facts {
		view_label: "Shop_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: Outlet_Lookup.Shop_id=Shop_facts.Shop_id ;;
	}
}
# FIXME: Join article_lookup requires review
explore: article_lookup_criteria {

	label: "Article_Lookup_Criteria"
	view_label: "Article_Lookup_Criteria"

	# FIXME: Join outer was missing, using default value inner
	join: article_lookup {
		view_label: "Article_lookup"
		relationship: "many_to_one"
		type: "inner"
		sql_on: Article_Lookup_Criteria.Article_id=Article_lookup.Article_id ;;
	}
}
# FIXME: Join shop_facts requires review
	# FIXME: Join product_promotion_facts requires review
explore: article_lookup {

	label: "Article_lookup"
	view_label: "Article_lookup"

	# FIXME: Join outer was missing, using default value inner
	join: shop_facts {
		view_label: "Shop_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: Article_lookup.Article_id=Shop_facts.Article_id ;;
	}
	# FIXME: Join outer was missing, using default value inner
	join: product_promotion_facts {
		view_label: "product_promotion_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: Article_lookup.Article_id=product_promotion_facts.Article_id ;;
	}
}
# FIXME: Join shop_facts requires review
explore: article_color_lookup {

	label: "Article_Color_Lookup"
	view_label: "Article_Color_Lookup"

	# FIXME: Join outer was missing, using default value inner
	join: shop_facts {
		view_label: "Shop_facts"
		relationship: "one_to_many"
		type: "inner"
		sql_on: Article_Color_Lookup.Article_id=Shop_facts.Article_id and Article_Color_Lookup.Color_code=Shop_facts.Color_code ;;
	}
}
# FIXME: Join calendar_year_lookup requires review
explore: shop_facts {

	label: "Shop_facts"
	view_label: "Shop_facts"

	# FIXME: Join outer was missing, using default value inner
	join: calendar_year_lookup {
		view_label: "Calendar_year_lookup"
		relationship: "many_to_one"
		type: "inner"
		sql_on: Shop_facts.Week_id=Calendar_year_lookup.Week_id ;;
	}
}
explore: calendar_year_lookup {

	label: "Calendar_year_lookup"
	view_label: "Calendar_year_lookup"

}
explore: agg_yr_qt_rn_st_ln_ca_sr {

	label: "Agg_yr_qt_rn_st_ln_ca_sr"
	view_label: "Agg_yr_qt_rn_st_ln_ca_sr"

}
explore: agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma {

	label: "Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma"
	view_label: "Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma"

}
