# View file auto-generated with booker
# 2021-06-09 17:44:05.872990

view: agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma {

	sql_table_name: "Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: agg1_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.agg1_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Yr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Yr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Qtr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Qtr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Mth {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Mth ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Month_name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Month_name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Wk {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Wk ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: City {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.City ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Store_name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Store_name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Sales_revenue {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Sales_revenue ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Quantity_sold {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Quantity_sold ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Margin {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Margin ;;
	}




}
