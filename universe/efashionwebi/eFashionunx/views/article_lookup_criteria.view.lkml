# View file auto-generated with booker
# 2021-06-09 17:44:05.868747

view: article_lookup_criteria {

	sql_table_name: "Article_Lookup_Criteria" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: Article_lookup_criteria_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_lookup_criteria_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Criteria {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Criteria ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Criteria_type {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Criteria_type ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Criteria_type_label {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Criteria_type_label ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Criteria_label {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Criteria_label ;;
	}




}
