# View file auto-generated with booker
# 2021-06-09 17:44:05.856056

view: shop_facts {

	sql_table_name: "Shop_facts" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: Shop_facts_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Shop_facts_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Color_code {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Color_code ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Week_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Week_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Shop_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Shop_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Margin {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Margin ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Amount_sold {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Amount_sold ;;
	}


	# FIXME: @Aggregate_Aware(
sum(Shop_facts.Quantity_sold * Article_lookup.Sale_price - Shop_facts.Amount_sold),
sum(Shop_facts.Quantity_sold * Article_Color_Lookup.Sale_price - Shop_facts.Amount_sold)) did not match any rules for conversion
	# FIXME: field present in more than one table [{'full_name': '"Shop_facts"', 'name': 'Shop_facts'}, {'full_name': '"Calendar_year_lookup"', 'name': 'Calendar_year_lookup'}]
	measure: discount {
		description: "Total discount of a SKU. Discount= Qty * Unit Price - Revenue.  Negative sums indicate the product was marked up (increased margin). Note discount is a calculated object (it does not exist in the fact table)."
		type: sum
		sql: @Aggregate_Aware( sum(${TABLE}.Quantity_sold * Article_lookup.Sale_price - ${TABLE}.Amount_sold), sum(${TABLE}.Quantity_sold * Article_Color_Lookup.Sale_price - ${TABLE}.Amount_sold)) WHERE ${TABLE}.Week_id=${TABLE}.Week_id ;;
	}


}
