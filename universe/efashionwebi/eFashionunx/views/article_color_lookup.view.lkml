# View file auto-generated with booker
# 2021-06-09 17:44:05.864308

view: article_color_lookup {

	sql_table_name: "Article_Color_Lookup" ;;

	dimension: color {
		label: "Color"
		description: "Color of an article. Each SKU number has many color variations (there is not a unique SKU for each color). Color is not compatible with Product Promotions, only SKU."
		type: string
		sql: ${TABLE}.Color_label ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_color_lookup_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_color_lookup_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Color_code {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Color_code ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_label {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Article_label ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Category {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Category ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Sale_price {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Sale_price ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Family_name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Family_name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Family_code {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Family_code ;;
	}




}
