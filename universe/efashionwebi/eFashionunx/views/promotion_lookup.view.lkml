# View file auto-generated with booker
# 2021-06-09 17:44:05.865468

view: promotion_lookup {

	sql_table_name: "promotion_lookup" ;;

	dimension: promotion_(y/n) {
		label: "Promotion (y/n)"
		description: "Promotion flag (yes or no) by SKU unit."
		type: string
		sql: ${TABLE}.Promotion_flag ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Promotion_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Promotion_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Print_flag {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Print_flag ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Radio_flag {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Radio_flag ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Television_flag {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Television_flag ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Direct_mail_flag {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Direct_mail_flag ;;
	}




}
