# View file auto-generated with booker
# 2021-06-09 17:44:04.695800# This view contains all the measures and dimensions which could not be mapped to a table. Please review

view: empty {

	sql_table_name: "EMPTY" ;;

	# FIXME: @Aggregate_Aware(
Agg_yr_qt_rn_st_ln_ca_sr.Yr,
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Yr,
Calendar_year_lookup.Yr) did not match any rules for conversion
	dimension: year {
		label: "Year"
		description: "Year 2003 - 2006."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_rn_st_ln_ca_sr.Yr, Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Yr, Calendar_year_lookup.Yr) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_rn_st_ln_ca_sr.Qtr,
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Qtr,
{fn concat('Q',Calendar_year_lookup.Qtr)}) did not match any rules for conversion
	dimension: quarter {
		label: "Quarter"
		description: "Quarter number: Q1, Q2, Q3, Q4."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_rn_st_ln_ca_sr.Qtr, Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Qtr, {fn concat('Q',Calendar_year_lookup.Qtr)}) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Mth,
Calendar_year_lookup.Mth) did not match any rules for conversion
	dimension: month {
		label: "Month"
		description: "Month number in year, 1-12."
		type: number
		sql: @Aggregate_Aware( Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Mth, Calendar_year_lookup.Mth) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Wk,
Calendar_year_lookup.Week_In_Year) did not match any rules for conversion
	dimension: week {
		label: "Week"
		description: "Week1-53. Week 53 may overlap with week 1 of the following year."
		type: number
		sql: @Aggregate_Aware( Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Wk, Calendar_year_lookup.Week_In_Year) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_rn_st_ln_ca_sr.State,
Outlet_Lookup.State) did not match any rules for conversion
	dimension: state {
		label: "State"
		description: "State located."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_rn_st_ln_ca_sr.State, Outlet_Lookup.State) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.City,
Outlet_Lookup.City) did not match any rules for conversion
	dimension: city {
		label: "City"
		description: "City located."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.City, Outlet_Lookup.City) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Store_name,
Outlet_Lookup.Shop_name) did not match any rules for conversion
	dimension: store_name {
		label: "Store name"
		description: "Name of store."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Store_name, Outlet_Lookup.Shop_name) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_rn_st_ln_ca_sr.Line,
Article_lookup.Family_name,
Article_Color_Lookup.Family_name) did not match any rules for conversion
	dimension: lines {
		label: "Lines"
		description: "Product line. Each line contains a set of categories."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_rn_st_ln_ca_sr.Line, Article_lookup.Family_name, Article_Color_Lookup.Family_name) ;;
	}
	# FIXME: @Aggregate_Aware(
Agg_yr_qt_rn_st_ln_ca_sr.Category,
Article_lookup.Category,
Article_Color_Lookup.Category) did not match any rules for conversion
	dimension: category {
		label: "Category"
		description: "Each category contains the individual SKU codes (and product descriptions)."
		type: string
		sql: @Aggregate_Aware( Agg_yr_qt_rn_st_ln_ca_sr.Category, Article_lookup.Category, Article_Color_Lookup.Category) ;;
	}
	# FIXME: @Aggregate_Aware(
Article_lookup.Article_id,
Article_Color_Lookup.Article_id) did not match any rules for conversion
	dimension: sku_number {
		label: "SKU number"
		description: "Stock Keeping Unit number (SKU).The lowest level of product description."
		type: number
		sql: @Aggregate_Aware( Article_lookup.Article_id, Article_Color_Lookup.Article_id) ;;
	}
	# FIXME: @Aggregate_Aware(
Article_lookup.Article_label,
Article_Color_Lookup.Article_label) did not match any rules for conversion
	dimension: sku_desc {
		label: "SKU desc"
		description: "Stock Keeping Unit description (SKU). Lowest level of product description. Each product comes in several different colors."
		type: string
		sql: @Aggregate_Aware( Article_lookup.Article_label, Article_Color_Lookup.Article_label) ;;
	}
	# FIXME: @Aggregate_Aware(
Article_lookup.Sale_price,
Article_Color_Lookup.Sale_price) did not match any rules for conversion
	dimension: unit_price_msrp {
		label: "Unit Price MSRP"
		description: "This is the manufacturers suggested retail price per SKU and color."
		type: number
		sql: @Aggregate_Aware( Article_lookup.Sale_price, Article_Color_Lookup.Sale_price) ;;
	}


	# FIXME: @Aggregate_Aware(
sum(Article_lookup.Sale_price),
sum(Article_Color_Lookup.Sale_price)) did not match any rules for conversion
	measure: extended_price {
		description: "The extended price let's you display the sum of prices over another dimension (such as outlet). When used with SKU number all the color variations (of each SKU) are added together."
		type: sum
		sql: @Aggregate_Aware( sum(Article_lookup.Sale_price), sum(Article_Color_Lookup.Sale_price)) ;;
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Select'}
	#FIXME: The expression matched 'iif' and should be reviewed!
	# FIXME: FIXME: Detected parameter Measures\Sales revenue however could not find equivalent parameter or filter in universe eFashion.unx
	# FIXME: Detected parameter Measures\Quantity sold however could not find equivalent parameter or filter in universe eFashion.unx
	# FIXME: Detected parameter Measures\Sales revenue)/@Select(Measures\Quantity sold)) however could not find equivalent parameter or filter in universe eFashion.unx
	measure: sold_at_(unit_price) {
		description: "This is the actual unit price per SKU obtained at sale time (i.e. Revenue/Quantity)"
		type: sum
		sql: IIf(@Select(Measures\Sales revenue)>0,     IIf(@Select(Measures\Quantity sold)>=0,        @Select(Measures\Sales revenue)/@Select(Measures\Quantity sold))) ;;
	}
	# FIXME: @Aggregate_Aware(
sum(Agg_yr_qt_rn_st_ln_ca_sr.Sales_revenue),
sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Sales_revenue),
sum(Shop_facts.Amount_sold)) did not match any rules for conversion
	measure: sales_revenue {
		description: "Sales revenue $ - $ revenue of SKU sold"
		type: sum
		sql: @Aggregate_Aware( sum(Agg_yr_qt_rn_st_ln_ca_sr.Sales_revenue), sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Sales_revenue), sum(Shop_facts.Amount_sold)) ;;
	}
	# FIXME: @Aggregate_Aware(
sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Quantity_sold),
sum(Shop_facts.Quantity_sold)) did not match any rules for conversion
	measure: quantity_sold {
		description: "Quantity sold - number of SKU sold"
		type: sum
		sql: @Aggregate_Aware( sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Quantity_sold), sum(Shop_facts.Quantity_sold)) ;;
	}
	# FIXME: @Aggregate_Aware(
sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Margin),
sum(Shop_facts.Margin)) did not match any rules for conversion
	measure: margin {
		description: "Margin $ = Revenue - Cost of sales"
		type: sum
		sql: @Aggregate_Aware( sum(Agg_yr_qt_mt_mn_wk_rg_cy_sn_sr_qt_ma.Margin), sum(Shop_facts.Margin)) ;;
	}

	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_152"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: libel04c {
		label: "libel04c"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_153"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: libel04d {
		label: "libel04d"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_166"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: ville01v {
		label: "ville01v"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_185"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: month05a {
		label: "month05a"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_186"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: quart05b {
		label: "quart05b"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_187"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: fisca05c {
		label: "fisca05c"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_188"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: year05d {
		label: "year05d"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_196"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: month05l {
		label: "month05l"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_214"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: surfa01t {
		label: "surfa01t"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_216"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: artic066 {
		label: "artic066"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_218"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: state068 {
		label: "state068"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_376"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: nom_m01k {
		label: "nom_m01k"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_468"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: objec0dd {
		label: "objec0dd"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_502"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: year_0ec {
		label: "year_0ec"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="false" name="duplicatedRows" value="false"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="300"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="90000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="OBJ_508"/>
    </resultObjects>
  </queryData>
</querySpecification>

	filter: categ0ei {
		label: "categ0ei"
		description: ""
		type: string
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: STRING did not match any rules for conversion
	filter: static_lov_from_sales_flloor_size_sqft? {
		label: "Static Lov from Sales flloor size sqFt?"
		description: ""
		type: STRING
		suggestions: ['0-99', '100-199', '200-299', '300-399', '400-499', '500+']
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: STRING did not match any rules for conversion
	filter: static_lov_from_choose_a_line_to_analyze? {
		label: "Static Lov from Choose a line to analyze?"
		description: ""
		type: STRING
		suggestions: ['Accessories', 'City Skirts', 'City Trousers', 'Dresses', 'Jackets', 'Leather', 'Outerwear', 'Overcoats', 'Shirt waist', 'Sweaters', 'Sweat-T-Shirts', 'Trousers']
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: STRING did not match any rules for conversion
	filter: static_lov_from_choose_a_category_to_analyze? {
		label: "Static Lov from Choose a category to analyze?"
		description: ""
		type: STRING
		suggestions: ['2 Pocket shirts', 'Belts,bags,wallets', 'Bermudas', 'Boatwear', 'Cardigan', 'Casual dresses', 'Day wear', 'Dry wear', 'Evening wear', 'Fancy fabric', 'Full length', 'Hair accessories', 'Hats,gloves,scarfs', 'Jackets', 'Jeans', 'Jewelry', 'Long lounge pants', 'Long sleeve', 'Lounge wear', 'Mini city', 'Night wear', 'Outdoor', 'Pants', 'Party pants', 'Samples', 'Shirts', 'Short sleeve', 'Skirts', 'Soft fabric', 'Sweater dresses', 'Sweats', 'T-Shirts', 'Turtleneck', 'Wet wear']
	}

	parameter: sales_flloor_size_sqft? {
		label: "Sales flloor size sqFt?"
		description: ""
		type: string
	}
	parameter: choose_a_line_to_analyze? {
		label: "Choose a line to analyze?"
		description: ""
		type: string
	}
	parameter: choose_a_category_to_analyze? {
		label: "Choose a category to analyze?"
		description: ""
		type: string
	}
	parameter: choose_the_product_descriptions_to_analyze: {
		label: "Choose the product descriptions to analyze:"
		description: ""
		type: string
	}
}
