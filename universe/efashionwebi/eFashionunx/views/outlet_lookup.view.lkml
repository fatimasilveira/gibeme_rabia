# View file auto-generated with booker
# 2021-06-09 17:44:04.691116

view: outlet_lookup {

	sql_table_name: "Outlet_Lookup" ;;

	dimension: long_opening_hours {
		label: "Long opening hours"
		description: "Y=Late night opening, N=Normal store hours."
		type: string
		sql: ${TABLE}.Long_opening_hours_flag ;;
	}
	dimension: name_of_manager {
		label: "Name of manager"
		description: "Manager's name."
		type: string
		sql: ${TABLE}.Manager ;;
	}
	dimension: opening_date {
		label: "Opening date"
		description: "Date store opened."
		type: string
		sql: ${TABLE}.Date_open ;;
	}
	dimension: owned_(y/n) {
		label: "Owned (y/n)"
		description: "Y=Wholly owned by e-Fashion, N=Franchise"
		type: string
		sql: ${TABLE}.Owned_outright_flag ;;
	}
	# FIXME: The expression matched 'iif' and should be reviewed!
	dimension: sales_floor_size_group {
		label: "Sales floor size group"
		description: "Sales floor size group: 0-999, 1000-1999, 2000-2999, 3000-3999, 4000-4999, 5000+."
		type: string
		sql: IIf(${TABLE}.Floor_space>=1000, IIf(${TABLE}.Floor_space>=2000, IIf(${TABLE}.Floor_space>=3000, IIf(${TABLE}.Floor_space>=4000, IIf(${TABLE}.Floor_space>=5000, '5000 +','4000-4999'),'3000-3999'), '2000-2999'),'1000-1999') ,'0-999') ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Shop_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Shop_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Shop_name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Shop_name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Address_1 {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Address_1 ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Zip_code {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Zip_code ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: City {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.City ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: State {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.State ;;
	}


	measure: extended_sales_floor_size {
		description: "Summed sales floor size. Can only be mixed with geography information relating to a store or group of stores. It cannot be used to show the SqFt of a store over time for instance."
		type: sum
		sql: SUM(${TABLE}.Floor_space) ;;
	}


}
