# View file auto-generated with booker
# 2021-06-09 17:44:04.703036

view: product_promotion_facts {

	sql_table_name: "product_promotion_facts" ;;

	dimension: duration {
		label: "Duration"
		description: "Duration in weeks of promotion."
		type: number
		sql: ${TABLE}.Duration ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Product_promotion_facts_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Product_promotion_facts_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Week_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Week_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Promotion_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Promotion_id ;;
	}


	measure: promotion_cost_usd {
		description: "Cost of promoting the SKU (in US dollars)."
		type: sum
		sql: SUM(${TABLE}.Promotion_cost) ;;
	}


}
