# View file auto-generated with booker
# 2021-06-09 17:44:05.857010

view: calendar_year_lookup {

	sql_table_name: "Calendar_year_lookup" ;;

	dimension: holiday_(y/n) {
		label: "Holiday (y/n)"
		description: "Holiday flag in week. Y=US public holiday during the time period, N=No holiday. Can be mixed with any time period dimension."
		type: string
		sql: UPPER(${TABLE}.Holiday_Flag) ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Week_In_Year {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Week_In_Year ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Yr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Yr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Fiscal_Period {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Fiscal_Period ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Year_Week {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Year_Week ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Qtr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Qtr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Month_Name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Month_Name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Mth {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Mth ;;
	}


	# FIXME: @Aggregate_Aware(
sum(Shop_facts.Quantity_sold * Article_lookup.Sale_price - Shop_facts.Amount_sold),
sum(Shop_facts.Quantity_sold * Article_Color_Lookup.Sale_price - Shop_facts.Amount_sold)) did not match any rules for conversion
	# FIXME: field present in more than one table [{'full_name': '"Shop_facts"', 'name': 'Shop_facts'}, {'full_name': '"Calendar_year_lookup"', 'name': 'Calendar_year_lookup'}]
	measure: discount {
		description: "Total discount of a SKU. Discount= Qty * Unit Price - Revenue.  Negative sums indicate the product was marked up (increased margin). Note discount is a calculated object (it does not exist in the fact table)."
		type: sum
		sql: @Aggregate_Aware( sum(${TABLE}.Quantity_sold * Article_lookup.Sale_price - ${TABLE}.Amount_sold), sum(${TABLE}.Quantity_sold * Article_Color_Lookup.Sale_price - ${TABLE}.Amount_sold)) WHERE ${TABLE}.Week_id=${TABLE}.Week_id ;;
	}


}
