# View file auto-generated with booker
# 2021-06-09 17:44:05.871509

view: article_lookup {

	sql_table_name: "Article_lookup" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: Article_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Article_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Article_label {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Article_label ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Category {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Category ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Sale_price {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Sale_price ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Family_name {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Family_name ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Family_code {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Family_code ;;
	}




}
