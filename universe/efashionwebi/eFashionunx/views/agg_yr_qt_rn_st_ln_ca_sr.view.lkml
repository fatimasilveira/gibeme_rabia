# View file auto-generated with booker
# 2021-06-09 17:44:05.872197

view: agg_yr_qt_rn_st_ln_ca_sr {

	sql_table_name: "Agg_yr_qt_rn_st_ln_ca_sr" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: agg2_id {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.agg2_id ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Yr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Yr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Qtr {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Qtr ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: State {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.State ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Line {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Line ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Category {
		label: ""
		description: "None"
		type: string
		sql: ${TABLE}.Category ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Sales_revenue {
		label: ""
		description: "None"
		type: number
		sql: ${TABLE}.Sales_revenue ;;
	}




}
