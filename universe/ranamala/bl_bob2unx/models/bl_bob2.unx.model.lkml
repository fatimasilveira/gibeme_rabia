# Model file auto-generated with booker
# 2021-06-09 17:43:56.307082
connection: "ranamala"

# include universe views only
include: "/universe/ranamala/bl_bob2unx/views/*.view"
include: "/dashboards/**/*.dashboard"


# FIXME: Join b_billingitem requires review
	# FIXME: Join d_customer requires review
explore: b_billinghdr {

	label: "B_BillingHdr"
	view_label: "B_BillingHdr"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: b_billingitem {
		view_label: "B_BillingItem"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingHdr.BillingDocument=LIVE.B_BillingItem.BillingDocument ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_customer {
		view_label: "D_Customer"
		relationship: "many_to_one"
		type: "left_outer"
		sql_on: LIVE.B_BillingHdr.SoldToParty=LIVE.D_Customer.CustomerNumber ;;
	}
}
# FIXME: Join d_material requires review
	# FIXME: Join b_billingitem_hevg requires review
explore: b_billingitem {

	label: "B_BillingItem"
	view_label: "B_BillingItem"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.Material=LIVE.D_Material.Material ;;
	}
	# FIXME: Join outer was missing, using default value inner
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: b_billingitem_hevg {
		view_label: "B_BillingItem"
		from: b_billingitem
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.BillingQuantity>0 ;;
	}
}
# FIXME: Join matgroup requires review
	# FIXME: Join d_material_pmwd requires review
explore: d_material {

	label: "D_Material"
	view_label: "D_Material"

	# FIXME: Join outer was missing, using default value inner
	join: matgroup {
		view_label: "MATGROUP"
		relationship: "many_to_many"
		type: "inner"
		sql_on: LIVE.D_Material.MaterialGroup=MATGROUP.MaterialGroup ;;
	}
	# FIXME: Join outer was missing, using default value inner
	# FIXME: SQL contains '@' expression that needs manual revision for {'Prompt'}
	#FIXME: "LIVE"."D_Material"."Material" IN @Prompt(in_material) did not match any rules for conversion
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: d_material_pmwd {
		view_label: "D_Material"
		from: d_material
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.D_Material.Material IN @Prompt(in_material) ;;
	}
}
explore: matgroup {

	label: "MATGROUP"
	view_label: "MATGROUP"

}
explore: d_customer {

	label: "D_Customer"
	view_label: "D_Customer"

}
