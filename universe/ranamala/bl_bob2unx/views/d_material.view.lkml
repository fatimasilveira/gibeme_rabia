# View file auto-generated with booker
# 2021-06-09 17:43:56.300499

view: d_material {

	sql_table_name: "LIVE.D_Material" ;;

	# FIXME: SQL contains '@' expression that needs manual revision for {'Prompt'}
	#FIXME: in @Prompt(in_material) did not match any rules for conversion
	# FIXME: review added parameters
	dimension: matnr {
		label: "MATNR"
		description: "Material"
		type: string
		sql: ${TABLE}.Material WHERE in {%  parameter in_material  %} ;;
	}
	dimension: mtart {
		label: "MTART"
		description: "MaterialType"
		type: string
		sql: ${TABLE}.MaterialType ;;
	}
	dimension: matkl {
		label: "MATKL"
		description: "MaterialGroup"
		type: string
		sql: ${TABLE}.MaterialGroup ;;
	}
	dimension: meins {
		label: "MEINS"
		description: "MaterialBaseUnit"
		type: string
		sql: ${TABLE}.MaterialBaseUnit ;;
	}
	dimension: prdha {
		label: "PRDHA"
		description: "ProductHierarchy"
		type: string
		sql: ${TABLE}.ProductHierarchy ;;
	}
	dimension: attyp {
		label: "ATTYP"
		description: "MaterialCategory"
		type: string
		sql: ${TABLE}.MaterialCategory ;;
	}
	dimension: txtmd {
		label: "TXTMD"
		description: "MaterialName"
		type: string
		sql: ${TABLE}.MaterialName ;;
	}
	dimension: mbrsh {
		label: "MBRSH"
		description: "IndustrySector"
		type: string
		sql: ${TABLE}.IndustrySector ;;
	}
	dimension: wrkst {
		label: "WRKST"
		description: "BasicMaterial"
		type: string
		sql: ${TABLE}.BasicMaterial ;;
	}
	dimension: normt {
		label: "NORMT"
		description: "IndustryStandardName"
		type: string
		sql: ${TABLE}.IndustryStandardName ;;
	}
	dimension: gewei {
		label: "GEWEI"
		description: "MaterialWeightUnit"
		type: string
		sql: ${TABLE}.MaterialWeightUnit ;;
	}
	dimension: ntgew {
		label: "NTGEW"
		description: "MaterialNetWeight"
		type: number
		sql: ${TABLE}.MaterialNetWeight ;;
	}
	dimension: extwg {
		label: "EXTWG"
		description: "MaterialExternalGroup"
		type: string
		sql: ${TABLE}.MaterialExternalGroup ;;
	}
	dimension: spart {
		label: "SPART"
		description: "Division"
		type: string
		sql: ${TABLE}.Division ;;
	}
	dimension: txtsh {
		label: "TXTSH"
		description: "MaterialGroupText"
		type: string
		sql: ${TABLE}.MaterialGroupText ;;
	}




	parameter: in_material {
		label: "in_material"
		description: ""
		type: string
	}
}
