# View file auto-generated with booker
# 2021-06-09 17:43:55.664330

view: b_billingitem {

	sql_table_name: "LIVE.B_BillingItem" ;;

	dimension: vbeln {
		label: "VBELN"
		description: "BillingDocument"
		type: string
		sql: ${TABLE}.BillingDocument ;;
	}
	dimension: posnr {
		label: "POSNR"
		description: "BillingDocumentItem"
		type: string
		sql: ${TABLE}.BillingDocumentItem ;;
	}
	dimension: vrkme {
		label: "VRKME"
		description: "BillingQuantityUnit"
		type: string
		sql: ${TABLE}.BillingQuantityUnit ;;
	}
	dimension: umvkz {
		label: "UMVKZ"
		description: "BillingToBaseQuantityNumrtr"
		type: number
		sql: ${TABLE}.BillingToBaseQuantityNumrtr ;;
	}
	dimension: umvkn {
		label: "UMVKN"
		description: "BillingToBaseQuantityDnmntr"
		type: number
		sql: ${TABLE}.BillingToBaseQuantityDnmntr ;;
	}
	dimension: meins {
		label: "MEINS"
		description: "BaseUnit"
		type: string
		sql: ${TABLE}.BaseUnit ;;
	}
	dimension: gewei {
		label: "GEWEI"
		description: "ItemWeightUnit"
		type: string
		sql: ${TABLE}.ItemWeightUnit ;;
	}
	dimension: voleh {
		label: "VOLEH"
		description: "ItemVolumeUnit"
		type: string
		sql: ${TABLE}.ItemVolumeUnit ;;
	}
	dimension: prsdt {
		label: "PRSDT"
		description: "PricingDate"
		type: string
		sql: ${TABLE}.PricingDate ;;
	}
	dimension: fbuda {
		label: "FBUDA"
		description: "ServicesRenderedDate"
		type: string
		sql: ${TABLE}.ServicesRenderedDate ;;
	}
	dimension: kursk {
		label: "KURSK"
		description: "PriceDetnExchangeRate"
		type: number
		sql: ${TABLE}.PriceDetnExchangeRate ;;
	}
	dimension: vgbel {
		label: "VGBEL"
		description: "ReferenceSDDocument"
		type: string
		sql: ${TABLE}.ReferenceSDDocument ;;
	}
	dimension: aubel {
		label: "AUBEL"
		description: "SalesDocument"
		type: string
		sql: ${TABLE}.SalesDocument ;;
	}
	dimension: aupos {
		label: "AUPOS"
		description: "SalesDocumentItem"
		type: string
		sql: ${TABLE}.SalesDocumentItem ;;
	}
	dimension: matnr {
		label: "MATNR"
		description: "Material"
		type: string
		sql: ${TABLE}.Material ;;
	}
	dimension: charg {
		label: "CHARG"
		description: "Batch"
		type: string
		sql: ${TABLE}.Batch ;;
	}
	dimension: matkl {
		label: "MATKL"
		description: "MaterialGroup"
		type: string
		sql: ${TABLE}.MaterialGroup ;;
	}
	dimension: pstyv {
		label: "PSTYV"
		description: "SalesDocumentItemCategory"
		type: string
		sql: ${TABLE}.SalesDocumentItemCategory ;;
	}
	dimension: posar {
		label: "POSAR"
		description: "SalesDocumentItemType"
		type: string
		sql: ${TABLE}.SalesDocumentItemType ;;
	}
	dimension: prodh {
		label: "PRODH"
		description: "ProductHierarchyNode"
		type: string
		sql: ${TABLE}.ProductHierarchyNode ;;
	}
	dimension: vbtyp {
		label: "Vbtyp"
		description: "VBTYP"
		type: string
		sql: ${TABLE}.VBTYP ;;
	}
	dimension: bonba {
		label: "BONBA"
		description: "RebateBasisAmount"
		type: number
		sql: ${TABLE}.RebateBasisAmount ;;
	}
	dimension: brgew {
		label: "BRGEW"
		description: "ItemGrossWeight"
		type: number
		sql: ${TABLE}.ItemGrossWeight ;;
	}
	dimension: brtwr {
		label: "BRTWR"
		description: "ItemGrossAmountOfBillingDoc"
		type: number
		sql: ${TABLE}.ItemGrossAmountOfBillingDoc ;;
	}
	dimension: fkimg {
		label: "FKIMG"
		description: "BillingQuantity"
		type: number
		sql: ${TABLE}.BillingQuantity ;;
	}
	dimension: fklmg {
		label: "FKLMG"
		description: "BillingQuantityInBaseUnit"
		type: number
		sql: ${TABLE}.BillingQuantityInBaseUnit ;;
	}
	dimension: kzwi1 {
		label: "KZWI1"
		description: "Subtotal1Amount"
		type: number
		sql: ${TABLE}.Subtotal1Amount ;;
	}
	dimension: kzwi2 {
		label: "KZWI2"
		description: "Subtotal2Amount"
		type: number
		sql: ${TABLE}.Subtotal2Amount ;;
	}
	dimension: kzwi3 {
		label: "KZWI3"
		description: "Subtotal3Amount"
		type: number
		sql: ${TABLE}.Subtotal3Amount ;;
	}
	dimension: kzwi4 {
		label: "KZWI4"
		description: "Subtotal4Amount"
		type: number
		sql: ${TABLE}.Subtotal4Amount ;;
	}
	dimension: kzwi5 {
		label: "KZWI5"
		description: "Subtotal5Amount"
		type: number
		sql: ${TABLE}.Subtotal5Amount ;;
	}
	dimension: kzwi6 {
		label: "KZWI6"
		description: "Subtotal6Amount"
		type: number
		sql: ${TABLE}.Subtotal6Amount ;;
	}
	dimension: lmeng {
		label: "LMENG"
		description: "MRPRequiredQuantityInBaseUnit"
		type: number
		sql: ${TABLE}.MRPRequiredQuantityInBaseUnit ;;
	}
	dimension: mwsbp {
		label: "MWSBP"
		description: "TaxAmount"
		type: number
		sql: ${TABLE}.TaxAmount ;;
	}
	dimension: netwr {
		label: "NETWR"
		description: "ItemNetAmountOfBillingDoc"
		type: number
		sql: ${TABLE}.ItemNetAmountOfBillingDoc ;;
	}
	dimension: ntgew {
		label: "NTGEW"
		description: "ItemNetWeight"
		type: number
		sql: ${TABLE}.ItemNetWeight ;;
	}
	dimension: skfbp {
		label: "SKFBP"
		description: "EligibleAmountForCashDiscount"
		type: number
		sql: ${TABLE}.EligibleAmountForCashDiscount ;;
	}
	dimension: wavwr {
		label: "WAVWR"
		description: "CostAmount"
		type: number
		sql: ${TABLE}.CostAmount ;;
	}
	dimension: smeng {
		label: "SMENG"
		description: "PricingScaleQuantityInBaseUnit"
		type: number
		sql: ${TABLE}.PricingScaleQuantityInBaseUnit ;;
	}
	dimension: volum {
		label: "VOLUM"
		description: "ItemVolume"
		type: number
		sql: ${TABLE}.ItemVolume ;;
	}
	dimension: anzfkpos {
		label: "Anzfkpos"
		description: "ANZFKPOS"
		type: number
		sql: ${TABLE}.ANZFKPOS ;;
	}
	dimension: calc_rebate {
		label: "Calc Rebate"
		description: ""
		type: number
		sql: ${TABLE}.calc_rebate ;;
	}




}
