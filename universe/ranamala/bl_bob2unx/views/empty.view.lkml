# View file auto-generated with booker
# 2021-06-09 17:43:56.307067# This view contains all the measures and dimensions which could not be mapped to a table. Please review

view: empty {

	sql_table_name: "EMPTY" ;;




	filter: lov_sql {
		label: "lov_sql"
		description: "List of values based on SQL"
		type: string
		drill_fields: ['Material']
		sql: SELECT "Material"
FROM "LIVE"."D_Material" ;;
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: Cannot convert Static LOV with multiple columns to LookerML
	# FIXME: Columns: ['Col 0', 'Col 1', 'Col 2']
	filter: lov_hardcoded {
		label: "lov_hardcoded"
		description: ""
		type: string
	}
	filter: lov_countries {
		label: "lov_countries"
		description: ""
		type: string
		drill_fields: ['Country']
		sql: SELECT "Country"
FROM  "LIVE"."D_Customer" ;;
	}

}
