# View file auto-generated with booker
# 2021-06-09 17:43:56.857858# This view contains all the measures and dimensions which could not be mapped to a table. Please review

view: empty {

	sql_table_name: "EMPTY" ;;

	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."ActualGIdate" , NULL) did not match any rules for conversion
	dimension: actual_gidate {
		label: "Actual Gidate"
		description: "ActualGIdate"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.ActualGIdate , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."BWTransactionkey" , NULL) did not match any rules for conversion
	dimension: bwtransactionkey {
		label: "Bwtransactionkey"
		description: "BWTransactionkey"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.BWTransactionkey , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."BaseUOM" , NULL) did not match any rules for conversion
	dimension: base_uom {
		label: "Base Uom"
		description: "BaseUOM"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.BaseUOM , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Bill-toparty" , NULL) did not match any rules for conversion
	dimension: bill_toparty {
		label: "Bill Toparty"
		description: "Bill-toparty"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Bill-toparty , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_AGIyear" , NULL) did not match any rules for conversion
	dimension: cc_agiyear {
		label: "Cc Agiyear"
		description: ""
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_AGIyear , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_Material" , NULL) did not match any rules for conversion
	dimension: cc_material {
		label: "Cc Material"
		description: "CC_Material"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_Material , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_MaterialGroup" , NULL) did not match any rules for conversion
	dimension: cc_material_group {
		label: "Cc Material Group"
		description: "CC_MaterialGroup"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_MaterialGroup , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_SalesOrg" , NULL) did not match any rules for conversion
	dimension: cc_sales_org {
		label: "Cc Sales Org"
		description: "CC_SalesOrg"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_SalesOrg , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_ShipToParty" , NULL) did not match any rules for conversion
	dimension: cc_ship_to_party {
		label: "Cc Ship To Party"
		description: "CC_ShipToParty"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_ShipToParty , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_SoldToParty" , NULL) did not match any rules for conversion
	dimension: cc_sold_to_party {
		label: "Cc Sold To Party"
		description: "CC_SoldToParty"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_SoldToParty , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Cancellationindicator" , NULL) did not match any rules for conversion
	dimension: cancellationindicator {
		label: "Cancellationindicator"
		description: "Cancellationindicator"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Cancellationindicator , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."ConfirmedDeliverydate" , NULL) did not match any rules for conversion
	dimension: confirmed_deliverydate {
		label: "Confirmed Deliverydate"
		description: "ConfirmedDeliverydate"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.ConfirmedDeliverydate , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Customergroup" , NULL) did not match any rules for conversion
	dimension: customergroup {
		label: "Customergroup"
		description: "Customergroup"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Customergroup , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Delivery" , NULL) did not match any rules for conversion
	dimension: delivery {
		label: "Delivery"
		description: "Delivery"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Delivery , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."DeliveryItem" , NULL) did not match any rules for conversion
	dimension: delivery_item {
		label: "Delivery Item"
		description: "DeliveryItem"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.DeliveryItem , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Deliveryblock" , NULL) did not match any rules for conversion
	dimension: deliveryblock {
		label: "Deliveryblock"
		description: "Deliveryblock"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Deliveryblock , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."HDR_PickStatus" , NULL) did not match any rules for conversion
	dimension: hdr_pick_status {
		label: "Hdr Pick Status"
		description: "HDR_PickStatus"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.HDR_PickStatus , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Item_Pickconfirmation" , NULL) did not match any rules for conversion
	dimension: item_pickconfirmation {
		label: "Item Pickconfirmation"
		description: "Item_Pickconfirmation"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Item_Pickconfirmation , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Item_Pickingstatus" , NULL) did not match any rules for conversion
	dimension: item_pickingstatus {
		label: "Item Pickingstatus"
		description: "Item_Pickingstatus"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Item_Pickingstatus , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Payer" , NULL) did not match any rules for conversion
	dimension: payer {
		label: "Payer"
		description: "Payer"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Payer , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."PlannedGIDate" , NULL) did not match any rules for conversion
	dimension: planned_gidate {
		label: "Planned Gidate"
		description: "PlannedGIDate"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.PlannedGIDate , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Referencedocument" , NULL) did not match any rules for conversion
	dimension: referencedocument {
		label: "Referencedocument"
		description: "Referencedocument"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Referencedocument , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Referenceitem" , NULL) did not match any rules for conversion
	dimension: referenceitem {
		label: "Referenceitem"
		description: "Referenceitem"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Referenceitem , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Salesdocumentcategory" , NULL) did not match any rules for conversion
	dimension: salesdocumentcategory {
		label: "Salesdocumentcategory"
		description: "Salesdocumentcategory"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Salesdocumentcategory , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."Vendor" , NULL) did not match any rules for conversion
	dimension: vendor {
		label: "Vendor"
		description: "Vendor"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.Vendor , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."BasicMaterial" , NULL) did not match any rules for conversion
	dimension: wrkst {
		label: "WRKST"
		description: "BasicMaterial"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.BasicMaterial , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."Division" , NULL) did not match any rules for conversion
	dimension: spart {
		label: "SPART"
		description: "Division"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.Division , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."IndustrySector" , NULL) did not match any rules for conversion
	dimension: mbrsh {
		label: "MBRSH"
		description: "IndustrySector"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.IndustrySector , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."IndustryStandardName" , NULL) did not match any rules for conversion
	dimension: normt {
		label: "NORMT"
		description: "IndustryStandardName"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.IndustryStandardName , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."Material" , NULL) did not match any rules for conversion
	dimension: matnr {
		label: "MATNR"
		description: "Material"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.Material , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialBaseUnit" , NULL) did not match any rules for conversion
	dimension: meins {
		label: "MEINS"
		description: "MaterialBaseUnit"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialBaseUnit , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialCategory" , NULL) did not match any rules for conversion
	dimension: attyp {
		label: "ATTYP"
		description: "MaterialCategory"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialCategory , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialExternalGroup" , NULL) did not match any rules for conversion
	dimension: extwg {
		label: "EXTWG"
		description: "MaterialExternalGroup"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialExternalGroup , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialGroup" , NULL) did not match any rules for conversion
	dimension: matkl {
		label: "MATKL"
		description: "MaterialGroup"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialGroup , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialGroupText" , NULL) did not match any rules for conversion
	dimension: txtsh {
		label: "TXTSH"
		description: "MaterialGroupText"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialGroupText , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialGroup" , NULL) did not match any rules for conversion
	dimension: matkl {
		label: "MATKL"
		description: "MaterialGroup"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialGroup , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."ProductHierarchy" , NULL) did not match any rules for conversion
	dimension: prdha {
		label: "PRDHA"
		description: "ProductHierarchy"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.ProductHierarchy , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."IndustrySector" , NULL) did not match any rules for conversion
	dimension: mbrsh {
		label: "MBRSH"
		description: "IndustrySector"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.IndustrySector , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialCategory" , NULL) did not match any rules for conversion
	dimension: attyp {
		label: "ATTYP"
		description: "MaterialCategory"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialCategory , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."Material" , NULL) did not match any rules for conversion
	dimension: matnr {
		label: "MATNR"
		description: "Material"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.Material , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialName" , NULL) did not match any rules for conversion
	dimension: txtmd {
		label: "TXTMD"
		description: "MaterialName"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialName , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."IndustrySector" , NULL) did not match any rules for conversion
	dimension: mbrsh {
		label: "MBRSH"
		description: "IndustrySector"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.IndustrySector , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."ProductHierarchy" , NULL) did not match any rules for conversion
	dimension: prdha {
		label: "PRDHA"
		description: "ProductHierarchy"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.ProductHierarchy , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialType" , NULL) did not match any rules for conversion
	dimension: mtart {
		label: "MTART"
		description: "MaterialType"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialType , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialName" , NULL) did not match any rules for conversion
	dimension: txtmd {
		label: "TXTMD"
		description: "MaterialName"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialName , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialGroupText" , NULL) did not match any rules for conversion
	dimension: txtsh {
		label: "TXTSH"
		description: "MaterialGroupText"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialGroupText , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialType" , NULL) did not match any rules for conversion
	dimension: mtart {
		label: "MTART"
		description: "MaterialType"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialType , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."MaterialWeightUnit" , NULL) did not match any rules for conversion
	dimension: gewei {
		label: "GEWEI"
		description: "MaterialWeightUnit"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.MaterialWeightUnit , NULL) ;;
	}
	# FIXME: @Aggregate_Aware("LIVE"."Q_Material"."ProductHierarchy" , NULL) did not match any rules for conversion
	dimension: prdha {
		label: "PRDHA"
		description: "ProductHierarchy"
		type: string
		sql: @Aggregate_Aware(LIVE.Q_Material.ProductHierarchy , NULL) ;;
	}

	# FIXME: @Aggregate_Aware("LIVE"."Q_Sales_Deliveries_List"."CC_AGI_Date" , NULL) did not match any rules for conversion
	dimension_group: actual_gi_date {
		label: "Actual GI Date"
		description: "CC_AGI_Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: @Aggregate_Aware(LIVE.Q_Sales_Deliveries_List.CC_AGI_Date , NULL) ;;
	}


	filter: lov_sql {
		label: "lov_sql"
		description: "List of values based on SQL"
		type: string
		drill_fields: ['Material']
		sql: SELECT "Material"
FROM "LIVE"."D_Material" ;;
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: Cannot convert Static LOV with multiple columns to LookerML
	# FIXME: Columns: ['Col 0', 'Col 1', 'Col 2']
	filter: lov_hardcoded {
		label: "lov_hardcoded"
		description: ""
		type: string
	}
	filter: lov_countries {
		label: "lov_countries"
		description: ""
		type: string
		drill_fields: ['Country']
		sql: SELECT "Country"
FROM  "LIVE"."D_Customer" ;;
	}

	parameter: in_material {
		label: "in_material"
		description: ""
		type: string
	}
}
