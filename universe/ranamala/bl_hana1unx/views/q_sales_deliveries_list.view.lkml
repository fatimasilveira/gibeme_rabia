# View file auto-generated with booker
# 2021-06-09 17:43:56.854177

view: q_sales_deliveries_list {

	sql_table_name: "LIVE.Q_Sales_Deliveries_List" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: Cancellationindicator {
		label: ""
		description: "Cancellationindicator"
		type: string
		sql: ${TABLE}.Cancellationindicator ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Delivery {
		label: ""
		description: "Delivery"
		type: string
		sql: ${TABLE}.Delivery ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: DeliveryItem {
		label: ""
		description: "DeliveryItem"
		type: string
		sql: ${TABLE}.DeliveryItem ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Customergroup {
		label: ""
		description: "Customergroup"
		type: string
		sql: ${TABLE}.Customergroup ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: ConfirmedDeliverydate {
		label: ""
		description: "ConfirmedDeliverydate"
		type: string
		sql: ${TABLE}.ConfirmedDeliverydate ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Deliveryblock {
		label: ""
		description: "Deliveryblock"
		type: string
		sql: ${TABLE}.Deliveryblock ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Vendor {
		label: ""
		description: "Vendor"
		type: string
		sql: ${TABLE}.Vendor ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Salesdocumentcategory {
		label: ""
		description: "Salesdocumentcategory"
		type: string
		sql: ${TABLE}.Salesdocumentcategory ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: BWTransactionkey {
		label: ""
		description: "BWTransactionkey"
		type: string
		sql: ${TABLE}.BWTransactionkey ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: BaseUOM {
		label: ""
		description: "BaseUOM"
		type: string
		sql: ${TABLE}.BaseUOM ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Item_Pickingstatus {
		label: ""
		description: "Item_Pickingstatus"
		type: string
		sql: ${TABLE}.Item_Pickingstatus ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Item_Pickconfirmation {
		label: ""
		description: "Item_Pickconfirmation"
		type: string
		sql: ${TABLE}.Item_Pickconfirmation ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: ActualGIdate {
		label: ""
		description: "ActualGIdate"
		type: string
		sql: ${TABLE}.ActualGIdate ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Referencedocument {
		label: ""
		description: "Referencedocument"
		type: string
		sql: ${TABLE}.Referencedocument ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Referenceitem {
		label: ""
		description: "Referenceitem"
		type: string
		sql: ${TABLE}.Referenceitem ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: HDR_PickStatus {
		label: ""
		description: "HDR_PickStatus"
		type: string
		sql: ${TABLE}.HDR_PickStatus ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Bill-toparty {
		label: ""
		description: "Bill-toparty"
		type: string
		sql: ${TABLE}.Bill-toparty ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Payer {
		label: ""
		description: "Payer"
		type: string
		sql: ${TABLE}.Payer ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_SalesOrg {
		label: ""
		description: "CC_SalesOrg"
		type: string
		sql: ${TABLE}.CC_SalesOrg ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_Material {
		label: ""
		description: "CC_Material"
		type: string
		sql: ${TABLE}.CC_Material ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_SoldToParty {
		label: ""
		description: "CC_SoldToParty"
		type: string
		sql: ${TABLE}.CC_SoldToParty ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_MaterialGroup {
		label: ""
		description: "CC_MaterialGroup"
		type: string
		sql: ${TABLE}.CC_MaterialGroup ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_ShipToParty {
		label: ""
		description: "CC_ShipToParty"
		type: string
		sql: ${TABLE}.CC_ShipToParty ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: PlannedGIDate {
		label: ""
		description: "PlannedGIDate"
		type: string
		sql: ${TABLE}.PlannedGIDate ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: CC_AGIyear {
		label: ""
		description: ""
		type: string
		sql: ${TABLE}.CC_AGIyear ;;
	}
	# FIXME: Converted from Calculated Column. Please review SQL.
	dimension: calc_delv_quan {
		label: ""
		description: "None"
		type: number
		sql: "Q_Sales_Deliveries_List"."Deliveryquantity" + 10 ;;
	}

	# FIXME: No match from BLX, converted from DFX
	dimension_group: CC_AGI_Date {
		label: ""
		description: "Actual GI Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: ${TABLE}.CC_AGI_Date ;;
	}

	measure: actualdeliveryquantity {
		description: "Actualdeliveryquantity"
		type: sum
		sql: SUM(${TABLE}.Actualdeliveryquantity) ;;
	}
	# FIXME: DELEGATED did not match any rules for conversion
	# FIXME: COUNT("LIVE"."Q_Sales_Deliveries_List"."CC_CountDelMissed") did not match any rules for conversion
	measure: cc_count_del_missed {
		description: ""
		type: DELEGATED
		sql: COUNT(${TABLE}.CC_CountDelMissed) ;;
	}
	measure: delay_agi_pgi {
		description: "Delay_AGI_PGI"
		type: sum
		sql: SUM(${TABLE}.Delay_AGI_PGI) ;;
	}
	measure: deliveryquantity {
		description: "Deliveryquantity"
		type: sum
		sql: SUM(${TABLE}.Deliveryquantity) ;;
	}


}
