# View file auto-generated with booker
# 2021-06-09 17:43:57.500710

view: d_customer {

	sql_table_name: "LIVE.D_Customer" ;;

	dimension: kunnr {
		label: "KUNNR"
		description: "CustomerNumber"
		type: string
		sql: ${TABLE}.CustomerNumber ;;
	}
	dimension: adrnr {
		label: "ADRNR"
		description: "Address"
		type: string
		sql: ${TABLE}.Address ;;
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Prompt'}
	#FIXME: @Prompt(in_country) did not match any rules for conversion
	# FIXME: review added parameters
	dimension: land1 {
		label: "LAND1"
		description: "Country"
		type: string
		sql: ${TABLE}.Country WHERE {%  parameter in_country  %} ;;
	}
	dimension: lifnr {
		label: "LIFNR"
		description: "Vendor"
		type: string
		sql: ${TABLE}.Vendor ;;
	}
	dimension: loevm {
		label: "LOEVM"
		description: "DeletionFlagforMasterRecord"
		type: string
		sql: ${TABLE}.DeletionFlagforMasterRecord ;;
	}
	dimension: regio {
		label: "REGIO"
		description: "Region"
		type: string
		sql: ${TABLE}.Region ;;
	}
	dimension: werks {
		label: "WERKS"
		description: "Plant"
		type: string
		sql: ${TABLE}.Plant ;;
	}
	dimension: ktokd {
		label: "KTOKD"
		description: "CustomerAccountGroup"
		type: string
		sql: ${TABLE}.CustomerAccountGroup ;;
	}
	dimension: cityc {
		label: "CITYC"
		description: "City"
		type: string
		sql: ${TABLE}.City ;;
	}
	dimension: txtmd {
		label: "TXTMD"
		description: "CustomerName"
		type: string
		sql: ${TABLE}.CustomerName ;;
	}




	parameter: in_country {
		label: "in_country"
		description: ""
		type: string
	}
}
