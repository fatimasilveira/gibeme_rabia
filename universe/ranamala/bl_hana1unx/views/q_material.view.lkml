# View file auto-generated with booker
# 2021-06-09 17:43:56.856533

view: q_material {

	sql_table_name: "LIVE.Q_Material" ;;

	# FIXME: No match from BLX, converted from DFX
	dimension: Material {
		label: ""
		description: "MATNR"
		type: string
		sql: ${TABLE}.Material ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialType {
		label: ""
		description: "MTART"
		type: string
		sql: ${TABLE}.MaterialType ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialGroup {
		label: ""
		description: "MATKL"
		type: string
		sql: ${TABLE}.MaterialGroup ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialBaseUnit {
		label: ""
		description: "MEINS"
		type: string
		sql: ${TABLE}.MaterialBaseUnit ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: ProductHierarchy {
		label: ""
		description: "PRDHA"
		type: string
		sql: ${TABLE}.ProductHierarchy ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialCategory {
		label: ""
		description: "ATTYP"
		type: string
		sql: ${TABLE}.MaterialCategory ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialName {
		label: ""
		description: "TXTMD"
		type: string
		sql: ${TABLE}.MaterialName ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: IndustrySector {
		label: ""
		description: "MBRSH"
		type: string
		sql: ${TABLE}.IndustrySector ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: BasicMaterial {
		label: ""
		description: "WRKST"
		type: string
		sql: ${TABLE}.BasicMaterial ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: IndustryStandardName {
		label: ""
		description: "NORMT"
		type: string
		sql: ${TABLE}.IndustryStandardName ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialWeightUnit {
		label: ""
		description: "GEWEI"
		type: string
		sql: ${TABLE}.MaterialWeightUnit ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialExternalGroup {
		label: ""
		description: "EXTWG"
		type: string
		sql: ${TABLE}.MaterialExternalGroup ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: Division {
		label: ""
		description: "SPART"
		type: string
		sql: ${TABLE}.Division ;;
	}
	# FIXME: No match from BLX, converted from DFX
	dimension: MaterialGroupText {
		label: ""
		description: "TXTSH"
		type: string
		sql: ${TABLE}.MaterialGroupText ;;
	}


	measure: material_count {
		description: "MaterialCount"
		type: sum
		sql: SUM(${TABLE}.MaterialCount) ;;
	}
	measure: ntgew {
		description: "MaterialNetWeight"
		type: sum
		sql: SUM(${TABLE}.MaterialNetWeight) ;;
	}


}
