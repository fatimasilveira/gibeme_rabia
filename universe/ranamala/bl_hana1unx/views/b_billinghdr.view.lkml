# View file auto-generated with booker
# 2021-06-09 17:43:57.467465

view: b_billinghdr {

	sql_table_name: "LIVE.B_BillingHdr" ;;

	dimension: vbeln {
		label: "VBELN"
		description: "BillingDocument"
		type: string
		sql: ${TABLE}.BillingDocument ;;
	}
	dimension: bukrs {
		label: "BUKRS"
		description: "CompanyCode"
		type: string
		sql: ${TABLE}.CompanyCode ;;
	}
	dimension: bzirk {
		label: "BZIRK"
		description: "SalesDistrict"
		type: string
		sql: ${TABLE}.SalesDistrict ;;
	}
	dimension: erdat {
		label: "ERDAT"
		description: "CreationDate"
		type: string
		sql: ${TABLE}.CreationDate ;;
	}
	dimension: ernam {
		label: "ERNAM"
		description: "CreatedByUser"
		type: string
		sql: ${TABLE}.CreatedByUser ;;
	}
	dimension: fkart {
		label: "FKART"
		description: "BillingDocumentType"
		type: string
		sql: ${TABLE}.BillingDocumentType ;;
	}
	dimension: fkdat {
		label: "FKDAT"
		description: "InvoiceListBillingDate"
		type: string
		sql: ${TABLE}.InvoiceListBillingDate ;;
	}
	dimension: fktyp {
		label: "FKTYP"
		description: "BillingDocumentCategory"
		type: string
		sql: ${TABLE}.BillingDocumentCategory ;;
	}
	dimension: inco1 {
		label: "INCO1"
		description: "IncotermsClassification"
		type: string
		sql: ${TABLE}.IncotermsClassification ;;
	}
	dimension: inco2 {
		label: "INCO2"
		description: "IncotermsTransferLocation"
		type: string
		sql: ${TABLE}.IncotermsTransferLocation ;;
	}
	dimension: kdgrp {
		label: "KDGRP"
		description: "CustomerGroup"
		type: string
		sql: ${TABLE}.CustomerGroup ;;
	}
	dimension: kunag {
		label: "KUNAG"
		description: "SoldToParty"
		type: string
		sql: ${TABLE}.SoldToParty ;;
	}
	dimension: kunrg {
		label: "KUNRG"
		description: "PayerParty"
		type: string
		sql: ${TABLE}.PayerParty ;;
	}
	dimension: kurrf {
		label: "KURRF"
		description: "AccountingExchangeRate"
		type: number
		sql: ${TABLE}.AccountingExchangeRate ;;
	}
	dimension: kurst {
		label: "KURST"
		description: "ExchangeRateType"
		type: string
		sql: ${TABLE}.ExchangeRateType ;;
	}
	dimension: spart {
		label: "SPART"
		description: "Division"
		type: string
		sql: ${TABLE}.Division ;;
	}
	dimension: stwae {
		label: "STWAE"
		description: "StatisticsCurrency"
		type: string
		sql: ${TABLE}.StatisticsCurrency ;;
	}
	dimension: vbtyp {
		label: "VBTYP"
		description: "SDDocumentCategory"
		type: string
		sql: ${TABLE}.SDDocumentCategory ;;
	}
	dimension: vkorg {
		label: "VKORG"
		description: "SalesOrganization"
		type: string
		sql: ${TABLE}.SalesOrganization ;;
	}
	dimension: vtweg {
		label: "VTWEG"
		description: "DistributionChannel"
		type: string
		sql: ${TABLE}.DistributionChannel ;;
	}
	dimension: waerk {
		label: "WAERK"
		description: "TransactionCurrency"
		type: string
		sql: ${TABLE}.TransactionCurrency ;;
	}
	dimension: netwr {
		label: "Netwr"
		description: "NETWR"
		type: number
		sql: ${TABLE}.NETWR ;;
	}




}
