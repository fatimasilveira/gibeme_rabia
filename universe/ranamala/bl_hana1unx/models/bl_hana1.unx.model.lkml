# Model file auto-generated with booker
# 2021-06-09 17:43:57.519403
connection: "ranamala"

# include universe views only
include: "/universe/ranamala/bl_hana1unx/views/*.view"
include: "/dashboards/**/*.dashboard"


# FIXME: Join q_material_rpno requires review
	# FIXME: Join b_billingitem requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join b_billingitem requires review
	# FIXME: Join d_material requires review
	# FIXME: Join matgroup requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join b_billinghdr requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
	# FIXME: Join d_material requires review
explore: q_material {

	label: "Q_Material"
	view_label: "Q_Material"

	# FIXME: Join outer was missing, using default value inner
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: q_material_rpno {
		view_label: "Q_Material"
		from: q_material
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.BasicMaterial=LIVE.Q_Material.BasicMaterial ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: b_billingitem {
		view_label: "B_BillingItem"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.Material=LIVE.B_BillingItem.Material ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.Material=LIVE.D_Material.Material ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialType=LIVE.D_Material.MaterialType ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: b_billingitem {
		view_label: "B_BillingItem"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialGroup=LIVE.B_BillingItem.MaterialGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialGroup=LIVE.D_Material.MaterialGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: matgroup {
		view_label: "MATGROUP"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialGroup=MATGROUP.MaterialGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialBaseUnit=LIVE.D_Material.MaterialBaseUnit ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.ProductHierarchy=LIVE.D_Material.ProductHierarchy ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialCategory=LIVE.D_Material.MaterialCategory ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialName=LIVE.D_Material.MaterialName ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.IndustrySector=LIVE.D_Material.IndustrySector ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.BasicMaterial=LIVE.D_Material.BasicMaterial ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialWeightUnit=LIVE.D_Material.MaterialWeightUnit ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialExternalGroup=LIVE.D_Material.MaterialExternalGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: b_billinghdr {
		view_label: "B_BillingHdr"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.Division=LIVE.B_BillingHdr.Division ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.Division=LIVE.D_Material.Division ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialGroupText=LIVE.D_Material.MaterialGroupText ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Material.MaterialNetWeight=LIVE.D_Material.MaterialNetWeight ;;
	}
}
# FIXME: Join q_sales_deliveries_list_xyzw requires review
	# FIXME: Join d_customer requires review
explore: q_sales_deliveries_list {

	label: "Q_Sales_Deliveries_List"
	view_label: "Q_Sales_Deliveries_List"

	# FIXME: Join outer was missing, using default value inner
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: q_sales_deliveries_list_xyzw {
		view_label: "Q_Sales_Deliveries_List"
		from: q_sales_deliveries_list
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.Q_Sales_Deliveries_List.ActualGIdate=LIVE.Q_Sales_Deliveries_List.ActualGIdate ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_customer {
		view_label: "D_Customer"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.Q_Sales_Deliveries_List.Vendor=LIVE.D_Customer.Vendor ;;
	}
}
# FIXME: Join b_billingitem requires review
	# FIXME: Join d_customer requires review
	# FIXME: Join q_sales_deliveries_list requires review
	# FIXME: Join d_material requires review
explore: b_billinghdr {

	label: "B_BillingHdr"
	view_label: "B_BillingHdr"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: b_billingitem {
		view_label: "B_BillingItem"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingHdr.BillingDocument=LIVE.B_BillingItem.BillingDocument ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_customer {
		view_label: "D_Customer"
		relationship: "many_to_one"
		type: "left_outer"
		sql_on: LIVE.B_BillingHdr.SoldToParty=LIVE.D_Customer.CustomerNumber ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	# FIXME: OUTER_RIGHT cardinality inverted to OUTER_LEFT.
# Original right table: B_BillingHdr.
# Original left table: Q_Sales_Deliveries_List
	join: q_sales_deliveries_list {
		view_label: "Q_Sales_Deliveries_List"
		relationship: "many_to_one"
		type: "left_outer"
		sql_on: LIVE.Q_Sales_Deliveries_List.Customergroup=LIVE.B_BillingHdr.CustomerGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingHdr.Division=LIVE.D_Material.Division ;;
	}
}
# FIXME: Join d_material requires review
	# FIXME: Join b_billingitem_wobd requires review
	# FIXME: Join d_material requires review
	# FIXME: Join matgroup requires review
explore: b_billingitem {

	label: "B_BillingItem"
	view_label: "B_BillingItem"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.Material=LIVE.D_Material.Material ;;
	}
	# FIXME: Join outer was missing, using default value inner
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: b_billingitem_wobd {
		view_label: "B_BillingItem"
		from: b_billingitem
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.BillingQuantity>0 ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: d_material {
		view_label: "D_Material"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.MaterialGroup=LIVE.D_Material.MaterialGroup ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	join: matgroup {
		view_label: "MATGROUP"
		relationship: "many_to_one"
		type: "inner"
		sql_on: LIVE.B_BillingItem.MaterialGroup=MATGROUP.MaterialGroup ;;
	}
}
# FIXME: Join matgroup requires review
	# FIXME: Join d_material_ikkc requires review
	# FIXME: Join q_material requires review
explore: d_material {

	label: "D_Material"
	view_label: "D_Material"

	# FIXME: Join outer was missing, using default value inner
	join: matgroup {
		view_label: "MATGROUP"
		relationship: "many_to_many"
		type: "inner"
		sql_on: LIVE.D_Material.MaterialGroup=MATGROUP.MaterialGroup ;;
	}
	# FIXME: Join outer was missing, using default value inner
	# FIXME: SQL contains '@' expression that needs manual revision for {'Prompt'}
	#FIXME: "LIVE"."D_Material"."Material" IN @Prompt(in_material) did not match any rules for conversion
	# FIXME: Left and right tables are the same in this join. This is possibly a filter.
	join: d_material_ikkc {
		view_label: "D_Material"
		from: d_material
		relationship: "one_to_one"
		type: "inner"
		sql_on: LIVE.D_Material.Material IN @Prompt(in_material) ;;
	}
	# FIXME: Cardinality was missing, using default value many_to_one
	# FIXME: OUTER_RIGHT cardinality inverted to OUTER_LEFT.
# Original right table: D_Material.
# Original left table: Q_Material
	join: q_material {
		view_label: "Q_Material"
		relationship: "many_to_one"
		type: "left_outer"
		sql_on: LIVE.Q_Material.IndustryStandardName=LIVE.D_Material.IndustryStandardName ;;
	}
}
explore: matgroup {

	label: "MATGROUP"
	view_label: "MATGROUP"

}
explore: d_customer {

	label: "D_Customer"
	view_label: "D_Customer"

}
