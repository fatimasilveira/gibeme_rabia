# Model file auto-generated with booker
# 2021-06-09 17:44:01.993144
connection: "BI platform CMS system database"

# include universe views only
include: "/universe/BI_platform_CMS_system_database/BIplatformCMSsystemdatabaseunx/views/*.view"
include: "/dashboards/**/*.dashboard"


# FIXME: Join level1 requires review
explore: properties {

	label: "Properties"
	view_label: "Properties"

	# FIXME: Join outer was missing, using default value inner
	join: level1 {
		view_label: "Level1"
		relationship: "many_to_many"
		type: "inner"
		sql_on: Properties.join=Level1.join ;;
	}
}
# FIXME: Join level2 requires review
explore: level1 {

	label: "Level1"
	view_label: "Level1"

	# FIXME: Join outer was missing, using default value inner
	join: level2 {
		view_label: "Level2"
		relationship: "many_to_many"
		type: "inner"
		sql_on: Level1.join=Level2.join ;;
	}
}
# FIXME: Join level3 requires review
explore: level2 {

	label: "Level2"
	view_label: "Level2"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: level3 {
		view_label: "Level3"
		relationship: "many_to_one"
		type: "inner"
		sql_on: Level2.join=Level3.join ;;
	}
}
# FIXME: Join level4 requires review
explore: level3 {

	label: "Level3"
	view_label: "Level3"

	# FIXME: Cardinality was missing, using default value many_to_one
	join: level4 {
		view_label: "Level4"
		relationship: "many_to_one"
		type: "inner"
		sql_on: Level3.join=Level4.join ;;
	}
}
explore: level4 {

	label: "Level4"
	view_label: "Level4"

}
