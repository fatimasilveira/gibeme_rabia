# View file auto-generated with booker
# 2021-06-09 17:43:58.510350

view: level1 {

	sql_table_name: "Level1" ;;

	dimension: cuid_(l1) {
		label: "CUID (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_cuid ;;
	}
	# FIXME: int(Level1.si_id) did not match any rules for conversion
	dimension: id_(l1) {
		label: "Id (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.si_id) ;;
	}
	dimension: name_(l1) {
		label: "Name (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_name ;;
	}
	dimension: kind_(l1) {
		label: "Kind (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_kind ;;
	}
	# FIXME: folderPath(Level1.si_path) did not match any rules for conversion
	dimension: folderpath_(l1) {
		label: "FolderPath (L1)"
		description: ""
		type: string
		sql: folderPath(${TABLE}.si_path) ;;
	}
	dimension: user_fullname_(l1) {
		label: "User Fullname (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_userfullname ;;
	}
	# FIXME: flag(Level1.si_nameduser) did not match any rules for conversion
	dimension: is_named_user_(l1) {
		label: "Is Named User (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_nameduser) ;;
	}
	# FIXME: flag(Level1.si_passwordexpire) did not match any rules for conversion
	dimension: is_password_expire_(l1) {
		label: "Is Password Expire (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_passwordexpire) ;;
	}
	# FIXME: flag(Level1.PasswordNeverExpires) did not match any rules for conversion
	dimension: password_never_expires_(l1) {
		label: "Password Never Expires (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.PasswordNeverExpires) ;;
	}
	# FIXME: flag(Level1.SI_FORCE_PASSWORD_CHANGE) did not match any rules for conversion
	dimension: force_password_change_(l1) {
		label: "Force Password Change (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_FORCE_PASSWORD_CHANGE) ;;
	}
	# FIXME: flag(Level1.SI_CHANGEPASSWORD) did not match any rules for conversion
	dimension: change_password_(l1) {
		label: "Change Password (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CHANGEPASSWORD) ;;
	}
	dimension: email_(l1) {
		label: "EMail (L1)"
		description: ""
		type: string
		sql: ${TABLE}.SI_EMAIL_ADDRESS ;;
	}
	# FIXME: ValueSecurityInfo(Level1.principal) did not match any rules for conversion
	dimension: principalname_(l1) {
		label: "PrincipalName (L1)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principal) ;;
	}
	# FIXME: ValueSecurityInfo(Level1.principalKind) did not match any rules for conversion
	dimension: principalkind_(l1) {
		label: "PrincipalKind (L1)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principalKind) ;;
	}
	# FIXME: ValueSecurityInfo(Level1.access) did not match any rules for conversion
	dimension: access_(l1) {
		label: "Access (L1)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.access) ;;
	}
	dimension: serverkind_(l1) {
		label: "ServerKind (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_server_kind ;;
	}
	dimension: description_(l1) {
		label: "Description (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_description ;;
	}
	# FIXME: Text(Level1.SI_CURRENT_COMMAND_LINE,"500") did not match any rules for conversion
	dimension: current_command_line_(l1) {
		label: "Current Command Line (L1)"
		description: ""
		type: string
		sql: Text(${TABLE}.SI_CURRENT_COMMAND_LINE,500) ;;
	}
	# FIXME: flag(Level1.si_disabled_target) did not match any rules for conversion
	dimension: disabled_target_(l1) {
		label: "Disabled target (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_disabled_target) ;;
	}
	# FIXME: flag(Level1.SI_CURRENT_DISABLED_STATE) did not match any rules for conversion
	dimension: current_disabled_state_(l1) {
		label: "Current Disabled State (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CURRENT_DISABLED_STATE) ;;
	}
	# FIXME: int(Level1.si_server_is_alive) did not match any rules for conversion
	dimension: is_alive_(l1) {
		label: "Is Alive (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.si_server_is_alive) ;;
	}
	# FIXME: int(Level1.SI_EXPECTED_RUN_STATE) did not match any rules for conversion
	dimension: expected_run_state_(l1) {
		label: "Expected Run State (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_EXPECTED_RUN_STATE) ;;
	}
	# FIXME: text(Level1.si_webi_doc_properties,"500") did not match any rules for conversion
	dimension: webi_properties_(l1) {
		label: "Webi_Properties (L1)"
		description: ""
		type: string
		sql: text(${TABLE}.si_webi_doc_properties,500) ;;
	}
	# FIXME: flag(Level1.SI_INSTANCE) did not match any rules for conversion
	dimension: scheduling_instance_(l1) {
		label: "Scheduling Instance (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_INSTANCE) ;;
	}
	# FIXME: int(Level1.si_children) did not match any rules for conversion
	dimension: number_of_children_(l1) {
		label: "Number of Children (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.si_children) ;;
	}
	dimension: template_cuid_(l1) {
		label: "Template_CUID (L1)"
		description: ""
		type: string
		sql: ${TABLE}.si_template_cuid ;;
	}
	# FIXME: int(Level1.si_new_job_id) did not match any rules for conversion
	dimension: new_job_id_(l1) {
		label: "New Job Id (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.si_new_job_id) ;;
	}
	# FIXME: flag(Level1.Is_Recurring) did not match any rules for conversion
	dimension: is_recurring_(l1) {
		label: "Is Recurring (L1)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.Is_Recurring) ;;
	}
	# FIXME: int(Level1.SI_SCHEDULE_STATUS) did not match any rules for conversion
	dimension: schedule_status_(l1) {
		label: "Schedule Status (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_SCHEDULE_STATUS) ;;
	}
	# FIXME: int(Level1.SI_UISTATUS) did not match any rules for conversion
	dimension: publication_status_(l1) {
		label: "Publication Status (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_UISTATUS) ;;
	}
	# FIXME: int(Level1.SI_NUM_SUCCEEDED_RECIPIENTS) did not match any rules for conversion
	dimension: succeeded_recipients_(l1) {
		label: "Succeeded Recipients (L1)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_NUM_SUCCEEDED_RECIPIENTS) ;;
	}
	# FIXME: relationship(Level1.si_parent_folder) did not match any rules for conversion
	dimension: parentfolder_(l1) {
		label: "ParentFolder (L1)"
		description: ""
		type: number
		sql: relationship(${TABLE}.si_parent_folder) ;;
	}
	# FIXME: relationships(Level1.SI_CORPORATE_CATEGORIES) did not match any rules for conversion
	dimension: tocorporatecategory_(l1) {
		label: "To-CorporateCategory (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CORPORATE_CATEGORIES) ;;
	}
	# FIXME: relationships(Level1.SI_PERSONAL_CATEGORIES) did not match any rules for conversion
	dimension: topersonalcategory_(l1) {
		label: "To-PersonalCategory (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONAL_CATEGORIES) ;;
	}
	# FIXME: relationships(Level1.SI_DOCUMENTS) did not match any rules for conversion
	dimension: todoc_(l1) {
		label: "To-Doc (L1)"
		description: "Corporate Category, Personal Category"
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS) ;;
	}
	# FIXME: relationships(Level1.SI_CONNUNIVERSE) did not match any rules for conversion
	dimension: contounv_(l1) {
		label: "Con-To-Unv (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CONNUNIVERSE) ;;
	}
	# FIXME: relationships(Level1.si_webi) did not match any rules for conversion
	dimension: unvtowebi_(l1) {
		label: "Unv-To-Webi (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_webi) ;;
	}
	# FIXME: relationships(Level1.si_dataconnection) did not match any rules for conversion
	dimension: unvtocnx_(l1) {
		label: "Unv-To-Cnx (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_dataconnection) ;;
	}
	# FIXME: relationships(Level1.si_sl_documents) did not match any rules for conversion
	dimension: unxtowebi_(l1) {
		label: "Unx-To-Webi (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_sl_documents) ;;
	}
	# FIXME: relationships(Level1.SI_SL_UNIVERSE_TO_CONNECTIONS) did not match any rules for conversion
	dimension: unxtocnx_(l1) {
		label: "Unx-To-Cnx (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SL_UNIVERSE_TO_CONNECTIONS) ;;
	}
	# FIXME: relationships(Level1.SI_UNIVERSE) did not match any rules for conversion
	dimension: webitounv_(l1) {
		label: "Webi-To-Unv (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_UNIVERSE) ;;
	}
	# FIXME: relationships(Level1.SI_DSL_UNIVERSE) did not match any rules for conversion
	dimension: webitounx_(l1) {
		label: "Webi-To-Unx (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DSL_UNIVERSE) ;;
	}
	# FIXME: relationships(Level1.si_doc_common_connection) did not match any rules for conversion
	dimension: webitoolapcnx_(l1) {
		label: "Webi-To-OLAP-Cnx (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_doc_common_connection) ;;
	}
	# FIXME: relationships(Level1.SI_EXCEL_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitoexcel_(l1) {
		label: "Webi-To-Excel (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_EXCEL_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level1.SI_SHARED_DOC) did not match any rules for conversion
	dimension: webitoshareddoc_(l1) {
		label: "Webi-To-SharedDoc (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHARED_DOC) ;;
	}
	# FIXME: relationships(Level1.SI_SHAREDELEMENTS_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitosharedelementsofdoc_(l1) {
		label: "Webi-To-SharedElementsOfDoc (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHAREDELEMENTS_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level1.SI_REFERRING_DOCS) did not match any rules for conversion
	dimension: webitoreferringdoc_(l1) {
		label: "Webi-To-ReferringDoc (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_REFERRING_DOCS) ;;
	}
	# FIXME: relationships(Level1.SI_DOCUMENTS_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodoc_(l1) {
		label: "SharedElement-To-Doc (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level1.SI_DATASOURCES_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodatasource_(l1) {
		label: "SharedElement-To-Datasource (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DATASOURCES_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level1.SI_PERSONALCATEGORY_USER) did not match any rules for conversion
	dimension: personalcategorytouser_(l1) {
		label: "PersonalCategory-To-User (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONALCATEGORY_USER) ;;
	}
	# FIXME: relationships(Level1.si_principals) did not match any rules for conversion
	dimension: publicationtoprincipals_(l1) {
		label: "Publication-To-Principals (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_principals) ;;
	}
	# FIXME: relationships(Level1.si_publication_documents) did not match any rules for conversion
	dimension: publicationtodocuments_(l1) {
		label: "Publication-To-Documents (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_publication_documents) ;;
	}
	# FIXME: relationships(Level1.si_group_members) did not match any rules for conversion
	dimension: grouptomembers_(l1) {
		label: "Group-To-Members (L1)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_group_members) ;;
	}

	# FIXME: datetime(Level1.si_creation_time) did not match any rules for conversion
	dimension_group: creation_timestamp_(l1) {
		label: "Creation Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datePart(Level1.si_creation_time) did not match any rules for conversion
	dimension_group: creation_date_(l1) {
		label: "Creation Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datetime(Level1.si_update_ts) did not match any rules for conversion
	dimension_group: update_timestamp_(l1) {
		label: "Update Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datePart(Level1.si_update_ts) did not match any rules for conversion
	dimension_group: update_date_(l1) {
		label: "Update Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datetime(Level1.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_timestamp_(l1) {
		label: "LastLogon Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datePart(Level1.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_date_(l1) {
		label: "LastLogon Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datetime(Level1.SI_EXPECTED_RUN_STATE_TS) did not match any rules for conversion
	dimension_group: expected_run_state_timestamp_(l1) {
		label: "Expected Run State Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_EXPECTED_RUN_STATE_TS) ;;
	}
	# FIXME: date(Level1.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_date_(l1) {
		label: "Next Runtime Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: date(${TABLE}.SI_NEXTRUNTIME) ;;
	}
	# FIXME: datetime(Level1.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_timestamp_(l1) {
		label: "Next Runtime Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_NEXTRUNTIME) ;;
	}

	# FIXME: mint(Level1.si_size) did not match any rules for conversion
	measure: size_(l1) {
		description: "Size of the document"
		type: sum
		sql: mint(${TABLE}.si_size) ;;
	}


}
