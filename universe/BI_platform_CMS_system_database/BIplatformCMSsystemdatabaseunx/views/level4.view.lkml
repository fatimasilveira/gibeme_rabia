# View file auto-generated with booker
# 2021-06-09 17:43:58.512551

view: level4 {

	sql_table_name: "Level4" ;;

	dimension: cuid_(l4) {
		label: "CUID (L4)"
		description: ""
		type: string
		sql: ${TABLE}.si_cuid ;;
	}
	dimension: user_fullname_(l4) {
		label: "User Fullname (L4)"
		description: ""
		type: string
		sql: ${TABLE}.si_userfullname ;;
	}
	# FIXME: flag(Level4.si_nameduser) did not match any rules for conversion
	dimension: is_named_user_(l4) {
		label: "Is Named User (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_nameduser) ;;
	}
	# FIXME: flag(Level4.si_passwordexpire) did not match any rules for conversion
	dimension: is_password_expire_(l4) {
		label: "Is Password Expire (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_passwordexpire) ;;
	}
	# FIXME: flag(Level4.PasswordNeverExpires) did not match any rules for conversion
	dimension: password_never_expires_(l4) {
		label: "Password Never Expires (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.PasswordNeverExpires) ;;
	}
	# FIXME: flag(Level4.SI_FORCE_PASSWORD_CHANGE) did not match any rules for conversion
	dimension: force_password_change_(l4) {
		label: "Force Password Change (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_FORCE_PASSWORD_CHANGE) ;;
	}
	# FIXME: flag(Level4.SI_CHANGEPASSWORD) did not match any rules for conversion
	dimension: change_password_(l4) {
		label: "Change Password (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CHANGEPASSWORD) ;;
	}
	dimension: email_(l4) {
		label: "EMail (L4)"
		description: ""
		type: string
		sql: ${TABLE}.SI_EMAIL_ADDRESS ;;
	}
	# FIXME: ValueSecurityInfo(Level4.principal) did not match any rules for conversion
	dimension: principalname_(l4) {
		label: "PrincipalName (L4)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principal) ;;
	}
	# FIXME: ValueSecurityInfo(Level4.principalKind) did not match any rules for conversion
	dimension: principalkind_(l4) {
		label: "PrincipalKind (L4)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principalKind) ;;
	}
	# FIXME: ValueSecurityInfo(Level4.access) did not match any rules for conversion
	dimension: access_(l4) {
		label: "Access (L4)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.access) ;;
	}
	dimension: serverkind_(l4) {
		label: "ServerKind (L4)"
		description: ""
		type: string
		sql: ${TABLE}.si_server_kind ;;
	}
	dimension: description_(l4) {
		label: "Description (L4)"
		description: ""
		type: string
		sql: ${TABLE}.si_description ;;
	}
	# FIXME: Text(Level4.SI_CURRENT_COMMAND_LINE,"500") did not match any rules for conversion
	dimension: current_command_line_(l4) {
		label: "Current Command Line (L4)"
		description: ""
		type: string
		sql: Text(${TABLE}.SI_CURRENT_COMMAND_LINE,500) ;;
	}
	# FIXME: flag(Level4.si_disabled_target) did not match any rules for conversion
	dimension: disabled_target_(l4) {
		label: "Disabled target (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_disabled_target) ;;
	}
	# FIXME: flag(Level4.SI_CURRENT_DISABLED_STATE) did not match any rules for conversion
	dimension: current_disabled_state_(l4) {
		label: "Current Disabled State (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CURRENT_DISABLED_STATE) ;;
	}
	# FIXME: int(Level4.si_server_is_alive) did not match any rules for conversion
	dimension: is_alive_(l4) {
		label: "Is Alive (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.si_server_is_alive) ;;
	}
	# FIXME: int(Level4.SI_EXPECTED_RUN_STATE) did not match any rules for conversion
	dimension: expected_run_state_(l4) {
		label: "Expected Run State (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_EXPECTED_RUN_STATE) ;;
	}
	# FIXME: text(Level4.si_webi_doc_properties,"500") did not match any rules for conversion
	dimension: webi_properties_(l4) {
		label: "Webi_Properties (L4)"
		description: ""
		type: string
		sql: text(${TABLE}.si_webi_doc_properties,500) ;;
	}
	# FIXME: flag(Level4.SI_INSTANCE) did not match any rules for conversion
	dimension: scheduling_instance_(l4) {
		label: "Scheduling Instance (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_INSTANCE) ;;
	}
	# FIXME: int(Level4.si_children) did not match any rules for conversion
	dimension: number_of_children_(l4) {
		label: "Number of Children (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.si_children) ;;
	}
	dimension: template_cuid_(l4) {
		label: "Template_CUID (L4)"
		description: ""
		type: string
		sql: ${TABLE}.si_template_cuid ;;
	}
	# FIXME: int(Level4.si_new_job_id) did not match any rules for conversion
	dimension: new_job_id_(l4) {
		label: "New Job Id (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.si_new_job_id) ;;
	}
	# FIXME: flag(Level4.Is_Recurring) did not match any rules for conversion
	dimension: is_recurring_(l4) {
		label: "Is Recurring (L4)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.Is_Recurring) ;;
	}
	# FIXME: int(Level4.SI_SCHEDULE_STATUS) did not match any rules for conversion
	dimension: schedule_status_(l4) {
		label: "Schedule Status (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_SCHEDULE_STATUS) ;;
	}
	# FIXME: int(Level4.SI_UISTATUS) did not match any rules for conversion
	dimension: publication_status_(l4) {
		label: "Publication Status (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_UISTATUS) ;;
	}
	# FIXME: int(Level4.SI_NUM_SUCCEEDED_RECIPIENTS) did not match any rules for conversion
	dimension: succeeded_recipients_(l4) {
		label: "Succeeded Recipients (L4)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_NUM_SUCCEEDED_RECIPIENTS) ;;
	}

	# FIXME: datetime(Level4.si_creation_time) did not match any rules for conversion
	dimension_group: creation_timestamp_(l4) {
		label: "Creation Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datePart(Level4.si_creation_time) did not match any rules for conversion
	dimension_group: creation_date_(l4) {
		label: "Creation Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datetime(Level4.si_update_ts) did not match any rules for conversion
	dimension_group: update_timestamp_(l4) {
		label: "Update Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datePart(Level4.si_update_ts) did not match any rules for conversion
	dimension_group: update_date_(l4) {
		label: "Update Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datetime(Level4.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_timestamp_(l4) {
		label: "LastLogon Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datePart(Level4.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_date_(l4) {
		label: "LastLogon Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datetime(Level4.SI_EXPECTED_RUN_STATE_TS) did not match any rules for conversion
	dimension_group: expected_run_state_timestamp_(l4) {
		label: "Expected Run State Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_EXPECTED_RUN_STATE_TS) ;;
	}
	# FIXME: date(Level4.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_date_(l4) {
		label: "Next Runtime Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: date(${TABLE}.SI_NEXTRUNTIME) ;;
	}
	# FIXME: datetime(Level4.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_timestamp_(l4) {
		label: "Next Runtime Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_NEXTRUNTIME) ;;
	}

	# FIXME: mint(Level4.si_size) did not match any rules for conversion
	measure: size_(l4) {
		description: "Size of the document"
		type: sum
		sql: mint(${TABLE}.si_size) ;;
	}


}
