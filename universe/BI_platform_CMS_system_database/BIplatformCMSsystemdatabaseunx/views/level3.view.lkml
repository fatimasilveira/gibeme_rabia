# View file auto-generated with booker
# 2021-06-09 17:43:58.511843

view: level3 {

	sql_table_name: "Level3" ;;

	dimension: cuid_(l3) {
		label: "CUID (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_cuid ;;
	}
	# FIXME: int(Level3.si_id) did not match any rules for conversion
	dimension: id_(l3) {
		label: "Id (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.si_id) ;;
	}
	dimension: name_(l3) {
		label: "Name (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_name ;;
	}
	dimension: kind_(l3) {
		label: "Kind (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_kind ;;
	}
	# FIXME: folderPath(Level3.si_path) did not match any rules for conversion
	dimension: folderpath_(l3) {
		label: "FolderPath (L3)"
		description: ""
		type: string
		sql: folderPath(${TABLE}.si_path) ;;
	}
	dimension: user_fullname_(l3) {
		label: "User Fullname (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_userfullname ;;
	}
	# FIXME: flag(Level3.si_nameduser) did not match any rules for conversion
	dimension: is_named_user_(l3) {
		label: "Is Named User (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_nameduser) ;;
	}
	# FIXME: flag(Level3.si_passwordexpire) did not match any rules for conversion
	dimension: is_password_expire_(l3) {
		label: "Is Password Expire (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_passwordexpire) ;;
	}
	# FIXME: flag(Level3.PasswordNeverExpires) did not match any rules for conversion
	dimension: password_never_expires_(l3) {
		label: "Password Never Expires (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.PasswordNeverExpires) ;;
	}
	# FIXME: flag(Level3.SI_FORCE_PASSWORD_CHANGE) did not match any rules for conversion
	dimension: force_password_change_(l3) {
		label: "Force Password Change (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_FORCE_PASSWORD_CHANGE) ;;
	}
	# FIXME: flag(Level3.SI_CHANGEPASSWORD) did not match any rules for conversion
	dimension: change_password_(l3) {
		label: "Change Password (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CHANGEPASSWORD) ;;
	}
	dimension: email_(l3) {
		label: "EMail (L3)"
		description: ""
		type: string
		sql: ${TABLE}.SI_EMAIL_ADDRESS ;;
	}
	# FIXME: ValueSecurityInfo(Level3.principal) did not match any rules for conversion
	dimension: principalname_(l3) {
		label: "PrincipalName (L3)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principal) ;;
	}
	# FIXME: ValueSecurityInfo(Level3.principalKind) did not match any rules for conversion
	dimension: principalkind_(l3) {
		label: "PrincipalKind (L3)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principalKind) ;;
	}
	# FIXME: ValueSecurityInfo(Level3.access) did not match any rules for conversion
	dimension: access_(l3) {
		label: "Access (L3)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.access) ;;
	}
	dimension: serverkind_(l3) {
		label: "ServerKind (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_server_kind ;;
	}
	dimension: description_(l3) {
		label: "Description (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_description ;;
	}
	# FIXME: Text(Level3.SI_CURRENT_COMMAND_LINE,"500") did not match any rules for conversion
	dimension: current_command_line_(l3) {
		label: "Current Command Line (L3)"
		description: ""
		type: string
		sql: Text(${TABLE}.SI_CURRENT_COMMAND_LINE,500) ;;
	}
	# FIXME: flag(Level3.si_disabled_target) did not match any rules for conversion
	dimension: disabled_target_(l3) {
		label: "Disabled target (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_disabled_target) ;;
	}
	# FIXME: flag(Level3.SI_CURRENT_DISABLED_STATE) did not match any rules for conversion
	dimension: current_disabled_state_(l3) {
		label: "Current Disabled State (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CURRENT_DISABLED_STATE) ;;
	}
	# FIXME: int(Level3.si_server_is_alive) did not match any rules for conversion
	dimension: is_alive_(l3) {
		label: "Is Alive (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.si_server_is_alive) ;;
	}
	# FIXME: int(Level3.SI_EXPECTED_RUN_STATE) did not match any rules for conversion
	dimension: expected_run_state_(l3) {
		label: "Expected Run State (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_EXPECTED_RUN_STATE) ;;
	}
	# FIXME: text(Level3.si_webi_doc_properties,"500") did not match any rules for conversion
	dimension: webi_properties_(l3) {
		label: "Webi_Properties (L3)"
		description: ""
		type: string
		sql: text(${TABLE}.si_webi_doc_properties,500) ;;
	}
	# FIXME: flag(Level3.SI_INSTANCE) did not match any rules for conversion
	dimension: scheduling_instance_(l3) {
		label: "Scheduling Instance (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_INSTANCE) ;;
	}
	# FIXME: int(Level3.si_children) did not match any rules for conversion
	dimension: number_of_children_(l3) {
		label: "Number of Children (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.si_children) ;;
	}
	dimension: template_cuid_(l3) {
		label: "Template_CUID (L3)"
		description: ""
		type: string
		sql: ${TABLE}.si_template_cuid ;;
	}
	# FIXME: int(Level3.si_new_job_id) did not match any rules for conversion
	dimension: new_job_id_(l3) {
		label: "New Job Id (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.si_new_job_id) ;;
	}
	# FIXME: flag(Level3.Is_Recurring) did not match any rules for conversion
	dimension: is_recurring_(l3) {
		label: "Is Recurring (L3)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.Is_Recurring) ;;
	}
	# FIXME: int(Level3.SI_SCHEDULE_STATUS) did not match any rules for conversion
	dimension: schedule_status_(l3) {
		label: "Schedule Status (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_SCHEDULE_STATUS) ;;
	}
	# FIXME: int(Level3.SI_UISTATUS) did not match any rules for conversion
	dimension: publication_status_(l3) {
		label: "Publication Status (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_UISTATUS) ;;
	}
	# FIXME: int(Level3.SI_NUM_SUCCEEDED_RECIPIENTS) did not match any rules for conversion
	dimension: succeeded_recipients_(l3) {
		label: "Succeeded Recipients (L3)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_NUM_SUCCEEDED_RECIPIENTS) ;;
	}
	# FIXME: relationship(Level3.si_parent_folder) did not match any rules for conversion
	dimension: parentfolder_(l3) {
		label: "ParentFolder (L3)"
		description: ""
		type: number
		sql: relationship(${TABLE}.si_parent_folder) ;;
	}
	# FIXME: relationships(Level3.SI_CORPORATE_CATEGORIES) did not match any rules for conversion
	dimension: tocorporatecategory_(l3) {
		label: "To-CorporateCategory (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CORPORATE_CATEGORIES) ;;
	}
	# FIXME: relationships(Level3.SI_PERSONAL_CATEGORIES) did not match any rules for conversion
	dimension: topersonalcategory_(l3) {
		label: "To-PersonalCategory (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONAL_CATEGORIES) ;;
	}
	# FIXME: relationships(Level3.SI_DOCUMENTS) did not match any rules for conversion
	dimension: todoc_(l3) {
		label: "To-Doc (L3)"
		description: "Corporate Category, Personal Category"
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS) ;;
	}
	# FIXME: relationships(Level3.SI_CONNUNIVERSE) did not match any rules for conversion
	dimension: contounv_(l3) {
		label: "Con-To-Unv (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CONNUNIVERSE) ;;
	}
	# FIXME: relationships(Level3.si_webi) did not match any rules for conversion
	dimension: unvtowebi_(l3) {
		label: "Unv-To-Webi (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_webi) ;;
	}
	# FIXME: relationships(Level3.si_dataconnection) did not match any rules for conversion
	dimension: unvtocnx_(l3) {
		label: "Unv-To-Cnx (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_dataconnection) ;;
	}
	# FIXME: relationships(Level3.si_sl_documents) did not match any rules for conversion
	dimension: unxtowebi_(l3) {
		label: "Unx-To-Webi (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_sl_documents) ;;
	}
	# FIXME: relationships(Level3.SI_SL_UNIVERSE_TO_CONNECTIONS) did not match any rules for conversion
	dimension: unxtocnx_(l3) {
		label: "Unx-To-Cnx (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SL_UNIVERSE_TO_CONNECTIONS) ;;
	}
	# FIXME: relationships(Level3.SI_UNIVERSE) did not match any rules for conversion
	dimension: webitounv_(l3) {
		label: "Webi-To-Unv (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_UNIVERSE) ;;
	}
	# FIXME: relationships(Level3.SI_DSL_UNIVERSE) did not match any rules for conversion
	dimension: webitounx_(l3) {
		label: "Webi-To-Unx (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DSL_UNIVERSE) ;;
	}
	# FIXME: relationships(Level3.si_doc_common_connection) did not match any rules for conversion
	dimension: webitoolapcnx_(l3) {
		label: "Webi-To-OLAP-Cnx (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_doc_common_connection) ;;
	}
	# FIXME: relationships(Level3.SI_EXCEL_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitoexcel_(l3) {
		label: "Webi-To-Excel (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_EXCEL_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level3.SI_SHARED_DOC) did not match any rules for conversion
	dimension: webitoshareddoc_(l3) {
		label: "Webi-To-SharedDoc (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHARED_DOC) ;;
	}
	# FIXME: relationships(Level3.SI_SHAREDELEMENTS_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitosharedelementsofdoc_(l3) {
		label: "Webi-To-SharedElementsOfDoc (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHAREDELEMENTS_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level3.SI_REFERRING_DOCS) did not match any rules for conversion
	dimension: webitoreferringdoc_(l3) {
		label: "Webi-To-ReferringDoc (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_REFERRING_DOCS) ;;
	}
	# FIXME: relationships(Level3.SI_DOCUMENTS_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodoc_(l3) {
		label: "SharedElement-To-Doc (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level3.SI_DATASOURCES_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodatasource_(l3) {
		label: "SharedElement-To-Datasource (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DATASOURCES_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level3.SI_PERSONALCATEGORY_USER) did not match any rules for conversion
	dimension: personalcategorytouser_(l3) {
		label: "PersonalCategory-To-User (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONALCATEGORY_USER) ;;
	}
	# FIXME: relationships(Level3.si_principals) did not match any rules for conversion
	dimension: publicationtoprincipals_(l3) {
		label: "Publication-To-Principals (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_principals) ;;
	}
	# FIXME: relationships(Level3.si_publication_documents) did not match any rules for conversion
	dimension: publicationtodocuments_(l3) {
		label: "Publication-To-Documents (L3)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_publication_documents) ;;
	}

	# FIXME: datetime(Level3.si_creation_time) did not match any rules for conversion
	dimension_group: creation_timestamp_(l3) {
		label: "Creation Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datePart(Level3.si_creation_time) did not match any rules for conversion
	dimension_group: creation_date_(l3) {
		label: "Creation Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datetime(Level3.si_update_ts) did not match any rules for conversion
	dimension_group: update_timestamp_(l3) {
		label: "Update Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datePart(Level3.si_update_ts) did not match any rules for conversion
	dimension_group: update_date_(l3) {
		label: "Update Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datetime(Level3.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_timestamp_(l3) {
		label: "LastLogon Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datePart(Level3.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_date_(l3) {
		label: "LastLogon Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datetime(Level3.SI_EXPECTED_RUN_STATE_TS) did not match any rules for conversion
	dimension_group: expected_run_state_timestamp_(l3) {
		label: "Expected Run State Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_EXPECTED_RUN_STATE_TS) ;;
	}
	# FIXME: date(Level3.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_date_(l3) {
		label: "Next Runtime Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: date(${TABLE}.SI_NEXTRUNTIME) ;;
	}
	# FIXME: datetime(Level3.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_timestamp_(l3) {
		label: "Next Runtime Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_NEXTRUNTIME) ;;
	}

	# FIXME: mint(Level3.si_size) did not match any rules for conversion
	measure: size_(l3) {
		description: "Size of the document"
		type: sum
		sql: mint(${TABLE}.si_size) ;;
	}


}
