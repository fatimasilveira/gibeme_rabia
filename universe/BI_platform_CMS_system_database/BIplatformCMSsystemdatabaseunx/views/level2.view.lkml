# View file auto-generated with booker
# 2021-06-09 17:43:58.510949

view: level2 {

	sql_table_name: "Level2" ;;

	dimension: cuid_(l2) {
		label: "CUID (L2)"
		description: ""
		type: string
		sql: ${TABLE}.si_cuid ;;
	}
	# FIXME: int(Level2.si_id) did not match any rules for conversion
	dimension: id_(l2) {
		label: "Id (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.si_id) ;;
	}
	# FIXME: folderPath(Level2.si_path) did not match any rules for conversion
	dimension: folderpath_(l2) {
		label: "FolderPath (L2)"
		description: ""
		type: string
		sql: folderPath(${TABLE}.si_path) ;;
	}
	dimension: user_fullname_(l2) {
		label: "User Fullname (L2)"
		description: ""
		type: string
		sql: ${TABLE}.si_userfullname ;;
	}
	# FIXME: flag(Level2.si_nameduser) did not match any rules for conversion
	dimension: is_named_user_(l2) {
		label: "Is Named User (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_nameduser) ;;
	}
	# FIXME: flag(Level2.si_passwordexpire) did not match any rules for conversion
	dimension: is_password_expire_(l2) {
		label: "Is Password Expire (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_passwordexpire) ;;
	}
	# FIXME: flag(Level2.PasswordNeverExpires) did not match any rules for conversion
	dimension: password_never_expires_(l2) {
		label: "Password Never Expires (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.PasswordNeverExpires) ;;
	}
	# FIXME: flag(Level2.SI_FORCE_PASSWORD_CHANGE) did not match any rules for conversion
	dimension: force_password_change_(l2) {
		label: "Force Password Change (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_FORCE_PASSWORD_CHANGE) ;;
	}
	# FIXME: flag(Level2.SI_CHANGEPASSWORD) did not match any rules for conversion
	dimension: change_password_(l2) {
		label: "Change Password (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CHANGEPASSWORD) ;;
	}
	dimension: email_(l2) {
		label: "EMail (L2)"
		description: ""
		type: string
		sql: ${TABLE}.SI_EMAIL_ADDRESS ;;
	}
	# FIXME: ValueSecurityInfo(Level2.principal) did not match any rules for conversion
	dimension: principalname_(l2) {
		label: "PrincipalName (L2)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principal) ;;
	}
	# FIXME: ValueSecurityInfo(Level2.principalKind) did not match any rules for conversion
	dimension: principalkind_(l2) {
		label: "PrincipalKind (L2)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principalKind) ;;
	}
	# FIXME: ValueSecurityInfo(Level2.access) did not match any rules for conversion
	dimension: access_(l2) {
		label: "Access (L2)"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.access) ;;
	}
	dimension: serverkind_(l2) {
		label: "ServerKind (L2)"
		description: ""
		type: string
		sql: ${TABLE}.si_server_kind ;;
	}
	dimension: description_(l2) {
		label: "Description (L2)"
		description: ""
		type: string
		sql: ${TABLE}.si_description ;;
	}
	# FIXME: Text(Level2.SI_CURRENT_COMMAND_LINE,"500") did not match any rules for conversion
	dimension: current_command_line_(l2) {
		label: "Current Command Line (L2)"
		description: ""
		type: string
		sql: Text(${TABLE}.SI_CURRENT_COMMAND_LINE,500) ;;
	}
	# FIXME: flag(Level2.si_disabled_target) did not match any rules for conversion
	dimension: disabled_target_(l2) {
		label: "Disabled target (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_disabled_target) ;;
	}
	# FIXME: flag(Level2.SI_CURRENT_DISABLED_STATE) did not match any rules for conversion
	dimension: current_disabled_state_(l2) {
		label: "Current Disabled State (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CURRENT_DISABLED_STATE) ;;
	}
	# FIXME: int(Level2.si_server_is_alive) did not match any rules for conversion
	dimension: is_alive_(l2) {
		label: "Is Alive (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.si_server_is_alive) ;;
	}
	# FIXME: int(Level2.SI_EXPECTED_RUN_STATE) did not match any rules for conversion
	dimension: expected_run_state_(l2) {
		label: "Expected Run State (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_EXPECTED_RUN_STATE) ;;
	}
	# FIXME: text(Level2.si_webi_doc_properties,"500") did not match any rules for conversion
	dimension: webi_properties_(l2) {
		label: "Webi_Properties (L2)"
		description: ""
		type: string
		sql: text(${TABLE}.si_webi_doc_properties,500) ;;
	}
	# FIXME: flag(Level2.SI_INSTANCE) did not match any rules for conversion
	dimension: scheduling_instance_(l2) {
		label: "Scheduling Instance (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_INSTANCE) ;;
	}
	# FIXME: int(Level2.si_children) did not match any rules for conversion
	dimension: number_of_children_(l2) {
		label: "Number of Children (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.si_children) ;;
	}
	dimension: template_cuid_(l2) {
		label: "Template_CUID (L2)"
		description: ""
		type: string
		sql: ${TABLE}.si_template_cuid ;;
	}
	# FIXME: int(Level2.si_new_job_id) did not match any rules for conversion
	dimension: new_job_id_(l2) {
		label: "New Job Id (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.si_new_job_id) ;;
	}
	# FIXME: flag(Level2.Is_Recurring) did not match any rules for conversion
	dimension: is_recurring_(l2) {
		label: "Is Recurring (L2)"
		description: ""
		type: yesno
		sql: flag(${TABLE}.Is_Recurring) ;;
	}
	# FIXME: int(Level2.SI_SCHEDULE_STATUS) did not match any rules for conversion
	dimension: schedule_status_(l2) {
		label: "Schedule Status (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_SCHEDULE_STATUS) ;;
	}
	# FIXME: int(Level2.SI_UISTATUS) did not match any rules for conversion
	dimension: publication_status_(l2) {
		label: "Publication Status (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_UISTATUS) ;;
	}
	# FIXME: int(Level2.SI_NUM_SUCCEEDED_RECIPIENTS) did not match any rules for conversion
	dimension: succeeded_recipients_(l2) {
		label: "Succeeded Recipients (L2)"
		description: ""
		type: number
		sql: int(${TABLE}.SI_NUM_SUCCEEDED_RECIPIENTS) ;;
	}
	# FIXME: relationship(Level2.si_parent_folder) did not match any rules for conversion
	dimension: parentfolder_(l2) {
		label: "ParentFolder (L2)"
		description: ""
		type: number
		sql: relationship(${TABLE}.si_parent_folder) ;;
	}
	# FIXME: relationships(Level2.SI_CORPORATE_CATEGORIES) did not match any rules for conversion
	dimension: tocorporatecategory_(l2) {
		label: "To-CorporateCategory (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CORPORATE_CATEGORIES) ;;
	}
	# FIXME: relationships(Level2.SI_PERSONAL_CATEGORIES) did not match any rules for conversion
	dimension: topersonalcategory_(l2) {
		label: "To-PersonalCategory (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONAL_CATEGORIES) ;;
	}
	# FIXME: relationships(Level2.SI_DOCUMENTS) did not match any rules for conversion
	dimension: todoc_(l2) {
		label: "To-Doc (L2)"
		description: "Corporate Category, Personal Category"
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS) ;;
	}
	# FIXME: relationships(Level2.SI_CONNUNIVERSE) did not match any rules for conversion
	dimension: contounv_(l2) {
		label: "Con-To-Unv (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CONNUNIVERSE) ;;
	}
	# FIXME: relationships(Level2.si_webi) did not match any rules for conversion
	dimension: unvtowebi_(l2) {
		label: "Unv-To-Webi (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_webi) ;;
	}
	# FIXME: relationships(Level2.si_dataconnection) did not match any rules for conversion
	dimension: unvtocnx_(l2) {
		label: "Unv-To-Cnx (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_dataconnection) ;;
	}
	# FIXME: relationships(Level2.si_sl_documents) did not match any rules for conversion
	dimension: unxtowebi_(l2) {
		label: "Unx-To-Webi (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_sl_documents) ;;
	}
	# FIXME: relationships(Level2.SI_SL_UNIVERSE_TO_CONNECTIONS) did not match any rules for conversion
	dimension: unxtocnx_(l2) {
		label: "Unx-To-Cnx (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SL_UNIVERSE_TO_CONNECTIONS) ;;
	}
	# FIXME: relationships(Level2.SI_UNIVERSE) did not match any rules for conversion
	dimension: webitounv_(l2) {
		label: "Webi-To-Unv (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_UNIVERSE) ;;
	}
	# FIXME: relationships(Level2.SI_DSL_UNIVERSE) did not match any rules for conversion
	dimension: webitounx_(l2) {
		label: "Webi-To-Unx (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DSL_UNIVERSE) ;;
	}
	# FIXME: relationships(Level2.si_doc_common_connection) did not match any rules for conversion
	dimension: webitoolapcnx_(l2) {
		label: "Webi-To-OLAP-Cnx (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_doc_common_connection) ;;
	}
	# FIXME: relationships(Level2.SI_EXCEL_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitoexcel_(l2) {
		label: "Webi-To-Excel (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_EXCEL_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level2.SI_SHARED_DOC) did not match any rules for conversion
	dimension: webitoshareddoc_(l2) {
		label: "Webi-To-SharedDoc (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHARED_DOC) ;;
	}
	# FIXME: relationships(Level2.SI_SHAREDELEMENTS_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitosharedelementsofdoc_(l2) {
		label: "Webi-To-SharedElementsOfDoc (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHAREDELEMENTS_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Level2.SI_REFERRING_DOCS) did not match any rules for conversion
	dimension: webitoreferringdoc_(l2) {
		label: "Webi-To-ReferringDoc (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_REFERRING_DOCS) ;;
	}
	# FIXME: relationships(Level2.SI_DOCUMENTS_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodoc_(l2) {
		label: "SharedElement-To-Doc (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level2.SI_DATASOURCES_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodatasource_(l2) {
		label: "SharedElement-To-Datasource (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DATASOURCES_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Level2.SI_PERSONALCATEGORY_USER) did not match any rules for conversion
	dimension: personalcategorytouser_(l2) {
		label: "PersonalCategory-To-User (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONALCATEGORY_USER) ;;
	}
	# FIXME: relationships(Level2.si_principals) did not match any rules for conversion
	dimension: publicationtoprincipals_(l2) {
		label: "Publication-To-Principals (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_principals) ;;
	}
	# FIXME: relationships(Level2.si_publication_documents) did not match any rules for conversion
	dimension: publicationtodocuments_(l2) {
		label: "Publication-To-Documents (L2)"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_publication_documents) ;;
	}

	# FIXME: datetime(Level2.si_creation_time) did not match any rules for conversion
	dimension_group: creation_timestamp_(l2) {
		label: "Creation Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datePart(Level2.si_creation_time) did not match any rules for conversion
	dimension_group: creation_date_(l2) {
		label: "Creation Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datetime(Level2.si_update_ts) did not match any rules for conversion
	dimension_group: update_timestamp_(l2) {
		label: "Update Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datePart(Level2.si_update_ts) did not match any rules for conversion
	dimension_group: update_date_(l2) {
		label: "Update Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datetime(Level2.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_timestamp_(l2) {
		label: "LastLogon Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datePart(Level2.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_date_(l2) {
		label: "LastLogon Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datetime(Level2.SI_EXPECTED_RUN_STATE_TS) did not match any rules for conversion
	dimension_group: expected_run_state_timestamp_(l2) {
		label: "Expected Run State Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_EXPECTED_RUN_STATE_TS) ;;
	}
	# FIXME: date(Level2.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_date_(l2) {
		label: "Next Runtime Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: date(${TABLE}.SI_NEXTRUNTIME) ;;
	}
	# FIXME: datetime(Level2.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_timestamp_(l2) {
		label: "Next Runtime Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_NEXTRUNTIME) ;;
	}

	# FIXME: mint(Level2.si_size) did not match any rules for conversion
	measure: size_(l2) {
		description: "Size of the document"
		type: sum
		sql: mint(${TABLE}.si_size) ;;
	}


}
