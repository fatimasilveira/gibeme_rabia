# View file auto-generated with booker
# 2021-06-09 17:43:58.509822# This view contains all the measures and dimensions which could not be mapped to a table. Please review

view: empty {

	sql_table_name: "EMPTY" ;;

	# FIXME: int(Properties.si_scheduleinfo.SI_PROGRESS) did not match any rules for conversion
	dimension: scheduling_progress_status {
		label: "Scheduling Progress Status"
		description: ""
		type: number
		sql: int(Properties.si_scheduleinfo.SI_PROGRESS) ;;
	}
	dimension: top {
		label: "TOP"
		description: ""
		type: string
		sql: TOP ;;
	}
	# FIXME: int(Level1.si_scheduleinfo.SI_PROGRESS) did not match any rules for conversion
	dimension: scheduling_progress_status_(l1) {
		label: "Scheduling Progress Status (L1)"
		description: ""
		type: number
		sql: int(Level1.si_scheduleinfo.SI_PROGRESS) ;;
	}
	# FIXME: int(Level1.si_scheduleinfo.SI_SCHEDULE_TYPE) did not match any rules for conversion
	dimension: schedule_type_(l1) {
		label: "Schedule Type (L1)"
		description: ""
		type: number
		sql: int(Level1.si_scheduleinfo.SI_SCHEDULE_TYPE) ;;
	}
	# FIXME: flag(Level1.si_scheduleinfo.si_has_prompts) did not match any rules for conversion
	dimension: processinginfo_hasprompts_(l1) {
		label: "ProcessingInfo_HasPrompts (L1)"
		description: ""
		type: yesno
		sql: flag(Level1.si_scheduleinfo.si_has_prompts) ;;
	}
	# FIXME: int(Level1.si_scheduleinfo.si_objid) did not match any rules for conversion
	dimension: processinginfo_objectid_(l1) {
		label: "ProcessingInfo_ObjectId (L1)"
		description: ""
		type: number
		sql: int(Level1.si_scheduleinfo.si_objid) ;;
	}
	# FIXME: relationships(Level1.si_scheduleinfo.si_dependants) did not match any rules for conversion
	dimension: scheduletodependants_(l1) {
		label: "Schedule-To-Dependants (L1)"
		description: ""
		type: number
		sql: relationships(Level1.si_scheduleinfo.si_dependants) ;;
	}
	# FIXME: relationships(Level1.si_scheduleinfo.SI_DEPENDENCIES) did not match any rules for conversion
	dimension: scheduletodependencies_(l1) {
		label: "Schedule-To-Dependencies (L1)"
		description: ""
		type: number
		sql: relationships(Level1.si_scheduleinfo.SI_DEPENDENCIES) ;;
	}
	dimension: name_(l2) {
		label: "Name (L2)"
		description: ""
		type: string
		sql: Level2.si_name ;;
	}
	dimension: kind_(l2) {
		label: "Kind (L2)"
		description: ""
		type: string
		sql: Level2.si_kind ;;
	}
	# FIXME: int(Level2.si_scheduleinfo.SI_PROGRESS) did not match any rules for conversion
	dimension: scheduling_progress_status_(l2) {
		label: "Scheduling Progress Status (L2)"
		description: ""
		type: number
		sql: int(Level2.si_scheduleinfo.SI_PROGRESS) ;;
	}
	# FIXME: int(Level2.si_scheduleinfo.SI_SCHEDULE_TYPE) did not match any rules for conversion
	dimension: schedule_type_(l2) {
		label: "Schedule Type (L2)"
		description: ""
		type: number
		sql: int(Level2.si_scheduleinfo.SI_SCHEDULE_TYPE) ;;
	}
	# FIXME: flag(Level2.si_scheduleinfo.si_has_prompts) did not match any rules for conversion
	dimension: processinginfo_hasprompts_(l2) {
		label: "ProcessingInfo_HasPrompts (L2)"
		description: ""
		type: yesno
		sql: flag(Level2.si_scheduleinfo.si_has_prompts) ;;
	}
	# FIXME: int(Level2.si_scheduleinfo.si_objid) did not match any rules for conversion
	dimension: processinginfo_objectid_(l2) {
		label: "ProcessingInfo_ObjectId (L2)"
		description: ""
		type: number
		sql: int(Level2.si_scheduleinfo.si_objid) ;;
	}
	# FIXME: relationships(Level2.si_scheduleinfo.si_dependants) did not match any rules for conversion
	dimension: scheduletodependants_(l2) {
		label: "Schedule-To-Dependants (L2)"
		description: ""
		type: number
		sql: relationships(Level2.si_scheduleinfo.si_dependants) ;;
	}
	# FIXME: relationships(Level2.si_scheduleinfo.SI_DEPENDENCIES) did not match any rules for conversion
	dimension: scheduletodependencies_(l2) {
		label: "Schedule-To-Dependencies (L2)"
		description: ""
		type: number
		sql: relationships(Level2.si_scheduleinfo.SI_DEPENDENCIES) ;;
	}
	# FIXME: int(Level3.si_scheduleinfo.SI_PROGRESS) did not match any rules for conversion
	dimension: scheduling_progress_status_(l3) {
		label: "Scheduling Progress Status (L3)"
		description: ""
		type: number
		sql: int(Level3.si_scheduleinfo.SI_PROGRESS) ;;
	}
	# FIXME: int(Level3.si_scheduleinfo.SI_SCHEDULE_TYPE) did not match any rules for conversion
	dimension: schedule_type_(l3) {
		label: "Schedule Type (L3)"
		description: ""
		type: number
		sql: int(Level3.si_scheduleinfo.SI_SCHEDULE_TYPE) ;;
	}
	# FIXME: flag(Level3.si_scheduleinfo.si_has_prompts) did not match any rules for conversion
	dimension: processinginfo_hasprompts_(l3) {
		label: "ProcessingInfo_HasPrompts (L3)"
		description: ""
		type: yesno
		sql: flag(Level3.si_scheduleinfo.si_has_prompts) ;;
	}
	# FIXME: int(Level3.si_scheduleinfo.si_objid) did not match any rules for conversion
	dimension: processinginfo_objectid_(l3) {
		label: "ProcessingInfo_ObjectId (L3)"
		description: ""
		type: number
		sql: int(Level3.si_scheduleinfo.si_objid) ;;
	}
	# FIXME: relationships(Level3.si_scheduleinfo.si_dependants) did not match any rules for conversion
	dimension: scheduletodependants_(l3) {
		label: "Schedule-To-Dependants (L3)"
		description: ""
		type: number
		sql: relationships(Level3.si_scheduleinfo.si_dependants) ;;
	}
	# FIXME: relationships(Level3.si_scheduleinfo.SI_DEPENDENCIES) did not match any rules for conversion
	dimension: scheduletodependencies_(l3) {
		label: "Schedule-To-Dependencies (L3)"
		description: ""
		type: number
		sql: relationships(Level3.si_scheduleinfo.SI_DEPENDENCIES) ;;
	}
	# FIXME: int(Level4.si_id) did not match any rules for conversion
	dimension: id_(l4) {
		label: "Id (L4)"
		description: ""
		type: number
		sql: int(Level4.si_id) ;;
	}
	dimension: name_(l4) {
		label: "Name (L4)"
		description: ""
		type: string
		sql: Level4.si_name ;;
	}
	dimension: kind_(l4) {
		label: "Kind (L4)"
		description: ""
		type: string
		sql: Level4.si_kind ;;
	}
	# FIXME: folderPath(Level4.si_path) did not match any rules for conversion
	dimension: folderpath_(l4) {
		label: "FolderPath (L4)"
		description: ""
		type: string
		sql: folderPath(Level4.si_path) ;;
	}
	# FIXME: int(Level4.si_scheduleinfo.SI_PROGRESS) did not match any rules for conversion
	dimension: scheduling_progress_status_(l4) {
		label: "Scheduling Progress Status (L4)"
		description: ""
		type: number
		sql: int(Level4.si_scheduleinfo.SI_PROGRESS) ;;
	}
	# FIXME: int(Level4.si_scheduleinfo.SI_SCHEDULE_TYPE) did not match any rules for conversion
	dimension: schedule_type_(l4) {
		label: "Schedule Type (L4)"
		description: ""
		type: number
		sql: int(Level4.si_scheduleinfo.SI_SCHEDULE_TYPE) ;;
	}
	# FIXME: flag(Level4.si_scheduleinfo.si_has_prompts) did not match any rules for conversion
	dimension: processinginfo_hasprompts_(l4) {
		label: "ProcessingInfo_HasPrompts (L4)"
		description: ""
		type: yesno
		sql: flag(Level4.si_scheduleinfo.si_has_prompts) ;;
	}
	# FIXME: int(Level4.si_scheduleinfo.si_objid) did not match any rules for conversion
	dimension: processinginfo_objectid_(l4) {
		label: "ProcessingInfo_ObjectId (L4)"
		description: ""
		type: number
		sql: int(Level4.si_scheduleinfo.si_objid) ;;
	}

	# FIXME: SQL contains '@' expression that needs manual revision for {'Select'}
	#FIXME: datePart(@Select(InfoObjects\Timestamps\Update Timestamp)) did not match any rules for conversion
	# FIXME: FIXME: Detected parameter InfoObjects\Timestamps\Update Timestamp) however could not find equivalent parameter or filter in universe BI platform CMS system database.unx
	dimension_group: update_date {
		label: "Update Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(@Select(InfoObjects\Timestamps\Update Timestamp)) ;;
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Select'}
	#FIXME: date(@Select(InfoObjects\Scheduling\Next Runtime Timestamp)) did not match any rules for conversion
	# FIXME: FIXME: Detected parameter InfoObjects\Scheduling\Next Runtime Timestamp) however could not find equivalent parameter or filter in universe BI platform CMS system database.unx
	dimension_group: next_runtime_date {
		label: "Next Runtime Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: date(@Select(InfoObjects\Scheduling\Next Runtime Timestamp)) ;;
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Select'}
	#FIXME: datePart(@Select(InfoObjects\Scheduling\End Timestamp)) did not match any rules for conversion
	# FIXME: FIXME: Detected parameter InfoObjects\Scheduling\End Timestamp) however could not find equivalent parameter or filter in universe BI platform CMS system database.unx
	dimension_group: end_date {
		label: "End Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(@Select(InfoObjects\Scheduling\End Timestamp)) ;;
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Select'}
	#FIXME: datePart(@Select(InfoObjects\Scheduling\Start Timestamp)) did not match any rules for conversion
	# FIXME: FIXME: Detected parameter InfoObjects\Scheduling\Start Timestamp) however could not find equivalent parameter or filter in universe BI platform CMS system database.unx
	dimension_group: start_date {
		label: "Start Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(@Select(InfoObjects\Scheduling\Start Timestamp)) ;;
	}
	# FIXME: datePart(Level1.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_date_(l1) {
		label: "End Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level1.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datetime(Level1.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_timestamp_(l1) {
		label: "End Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level1.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datePart(Level1.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_date_(l1) {
		label: "Start Date (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level1.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datetime(Level1.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_timestamp_(l1) {
		label: "Start Timestamp (L1)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level1.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datePart(Level2.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_date_(l2) {
		label: "End Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level2.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datetime(Level2.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_timestamp_(l2) {
		label: "End Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level2.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datePart(Level2.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_date_(l2) {
		label: "Start Date (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level2.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datetime(Level2.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_timestamp_(l2) {
		label: "Start Timestamp (L2)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level2.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datePart(Level3.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_date_(l3) {
		label: "End Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level3.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datetime(Level3.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_timestamp_(l3) {
		label: "End Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level3.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datePart(Level3.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_date_(l3) {
		label: "Start Date (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level3.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datetime(Level3.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_timestamp_(l3) {
		label: "Start Timestamp (L3)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level3.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datePart(Level4.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_date_(l4) {
		label: "End Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level4.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datetime(Level4.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_timestamp_(l4) {
		label: "End Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level4.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datePart(Level4.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_date_(l4) {
		label: "Start Date (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(Level4.si_scheduleinfo.si_starttime) ;;
	}
	# FIXME: datetime(Level4.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_timestamp_(l4) {
		label: "Start Timestamp (L4)"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(Level4.si_scheduleinfo.si_starttime) ;;
	}

	# FIXME: NONE did not match any rules for conversion
	measure: webitounxcount {
		description: ""
		type: NONE
		sql: Properties.SI_DSL_UNIVERSE.SI_TOTAL ;;
	}
	# FIXME: NONE did not match any rules for conversion
	measure: webitounvcount {
		description: ""
		type: NONE
		sql: Properties.SI_UNIVERSE.SI_TOTAL ;;
	}

	# FIXME: Detected static list of values. Manual review required.
	# FIXME: STRING did not match any rules for conversion
	filter: kind_lov {
		label: "Kind_Lov"
		description: "List of possible values for 'Kind' of InfoObjects"
		type: STRING
		suggestions: ['AdminTool', 'Agnostic', 'AlertingApp', 'AlertNotification', 'AO.Plugin', 'AppFoundation', 'AuditEventInfo', 'AuditEventInfo2', 'BEX.BExWeb', 'BICommentaryApplication', 'BIonBI', 'BIP.CafApplication', 'BIVariant', 'BIWidgets', 'busobjReporter', 'Calendar', 'Category', 'CCIS.DataConnection', 'ClientAction', 'ClientActionSet', 'ClientActionUsage', 'CMC', 'CommonConnection', 'Connection', 'CRConfig', 'CryptographicKey', 'CrystalReport', 'CustomMappedAttribute', 'CustomRole', 'DataSearchUniverseDataAccessProvider', 'DependencyRule', 'DeploymentFiles', 'Designer', 'DFS.Parameters', 'Discussions', 'DSL.MetaDataFile', 'EnterpriseNode', 'Event', 'Excel', 'FavoritesFolder', 'Folder', 'HANAAuthentication', 'HotBackup', 'Hyperlink', 'Inbox', 'InformationControlCenter', 'InformationDesigner', 'InfoView', 'Install', 'LCM', 'LCMJob', 'LCMOverride', 'LCMSettings', 'LicenseKey', 'LicenseRestriction', 'LogicalGroup', 'LumiraApp', 'Manifest', 'MetaData.BusinessElement', 'MetaData.BusinessField', 'MetaData.BusinessView', 'MetaData.DataConnection', 'MetaData.DataDBField', 'MetaData.DataFoundation', 'MetaData.DataProcedure', 'MetaData.MetaDataRepositoryInfo', 'MetricDescriptions', 'MOB_Mobile', 'MON.ManagedEntityStatus', 'MON.MBeanConfig', 'MON.MonAppDataStore', 'MON.Monitoring', 'MON.Probe', 'MON.Subscription', 'MultitenancyManager', 'NotificationSchedule', 'OpenDocument', 'Pdf', 'PersonalCategory', 'Pioneer', 'PlatformSearchApplication', 'PlatformSearchApplicationStatus', 'PlatformSearchContentExtractor', 'PlatformSearchContentStore', 'PlatformSearchContentSurrogate', 'PlatformSearchDeltaIndex', 'PlatformSearchIndexEngine', 'PlatformSearchQueue', 'PlatformSearchScheduling', 'PlatformSearchSearchAgent', 'PlatformSearchServiceSession', 'PolestarApp', 'Powerpoint', 'RecycleBin', 'RecycleBinApplication', 'ReportConvTool', 'RepositoryPromptGroup', 'RestWebService', 'Server', 'Service', 'ServiceCategory', 'ServiceContainer', 'SharedElement', 'StreamWorkIntegration', 'TransMgr', 'Universe', 'UpgradeManagementTool', 'User', 'UserGroup', 'VisualDiff', 'VisualDiffApp', 'VMS', 'Webi', 'WebIntelligence', 'WebService', 'Word', 'XL.XcelsiusApplication']
	}
	# FIXME: Detected static list of values. Manual review required.
	# FIXME: STRING did not match any rules for conversion
	filter: serverkind_lov {
		label: "ServerKind_Lov"
		description: "List of possible values for 'ServerKind' Server InfoObject"
		type: STRING
		suggestions: ['aps', 'cacheserver', 'connectionserver', 'dpscacheXL.Query', 'dpsprocXL.Query', 'eventserver', 'explorerExploration', 'explorerIndexing', 'explorerMaster', 'explorerSearch', 'fileserver', 'jobserver', 'pageserver', 'pjs', 'rptappserver', 'ServerKind', 'webiserver']
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="true" name="duplicatedRows" value="true"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="0"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="0"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="_ubzl0dYVEeWrC9CB9_kc5g" path="InfoObjects\Id"/>
      <resultObject id="_ubzl0NYVEeWrC9CB9_kc5g" path="InfoObjects\Name"/>
      <resultObject id="_oPZE4dYrEeWlJMmESc31JQ" path="InfoObjects\Kind"/>
    </resultObjects>
    <filterPart>
      <comparisonFilter id="_oPZE4dYrEeWlJMmESc31JQ" operator="EqualTo" path="InfoObjects\Kind">
        <constantOperand searchPattern="false">
          <value>
            <caption type="String">webi</caption>
          </value>
        </constantOperand>
      </comparisonFilter>
    </filterPart>
  </queryData>
</querySpecification>

	filter: webi_lov {
		label: "WebI_Lov"
		description: ""
		type: string
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="true" name="duplicatedRows" value="true"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="600"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="5000"/>
    <queryOption activated="false" name="samplingResultSetSize" value="200"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="_ubzl0dYVEeWrC9CB9_kc5g" path="InfoObjects\Id"/>
    </resultObjects>
    <filterPart>
      <comparisonFilter id="_oPZE4dYrEeWlJMmESc31JQ" operator="EqualTo" path="InfoObjects\Kind">
        <parameterOperand>
          <parameter optional="false">
            <id>_3YI98DL3EeaO2u4TAUVLMg</id>
          </parameter>
        </parameterOperand>
      </comparisonFilter>
    </filterPart>
  </queryData>
</querySpecification>

	filter: id_lov_filered_by_kind_-_qp_query {
		label: "Id Lov filered by Kind - QP query"
		description: "Lov based on Kind prompt"
		type: string
	}
	# FIXME: SQL contains '@' expression that needs manual revision for {'Prompt'}
	#FIXME: SELECT Properties.si_id
FROM Properties
where Properties.si_kind in @Prompt(InfoObject Kind Parameter) did not match any rules for conversion
	filter: id_filtered_by_kind_-_sql_query {
		label: "Id filtered by Kind - SQL Query"
		description: ""
		type: number
		drill_fields: ['Properties.si_id.']
		sql: SELECT Properties.si_id
FROM Properties
where Properties.si_kind in @Prompt(InfoObject Kind Parameter) ;;
	}
	# FIXME: Cannot translate business query to LookerML
	# FIXME: Query: <?xml version="1.0" encoding="UTF-8"?><querySpecification xmlns="http://www.sap.com/rws/sl/universe" version="1.0">
  <queryOptions>
    <queryOption activated="true" name="duplicatedRows" value="true"/>
    <queryOption activated="false" name="maxRetrievalTimeInSeconds" value="0"/>
    <queryOption activated="false" name="maxRowsRetrieved" value="0"/>
    <queryOption activated="false" name="samplingResultSetSize" value="0"/>
    <queryOption activated="false" name="samplingResultSetFixed" value="false"/>
  </queryOptions>
  <queryData>
    <resultObjects>
      <resultObject id="_ubzl0dYVEeWrC9CB9_kc5g" path="InfoObjects\Id"/>
      <resultObject id="_ubzl0NYVEeWrC9CB9_kc5g" path="InfoObjects\Name"/>
    </resultObjects>
    <filterPart>
      <and>
        <comparisonFilter id="_ubzl0dYVEeWrC9CB9_kc5g" operator="InList" path="InfoObjects\Id">
          <constantOperand searchPattern="false">
            <value>
              <caption type="Numeric">4</caption>
            </value>
            <value>
              <caption type="Numeric">18</caption>
            </value>
            <value>
              <caption type="Numeric">19</caption>
            </value>
            <value>
              <caption type="Numeric">20</caption>
            </value>
            <value>
              <caption type="Numeric">21</caption>
            </value>
            <value>
              <caption type="Numeric">538</caption>
            </value>
            <value>
              <caption type="Numeric">539</caption>
            </value>
            <value>
              <caption type="Numeric">579</caption>
            </value>
            <value>
              <caption type="Numeric">3376</caption>
            </value>
            <value>
              <caption type="Numeric">4273</caption>
            </value>
            <value>
              <caption type="Numeric">22</caption>
            </value>
            <value>
              <caption type="Numeric">23</caption>
            </value>
            <value>
              <caption type="Numeric">25</caption>
            </value>
            <value>
              <caption type="Numeric">26</caption>
            </value>
            <value>
              <caption type="Numeric">27</caption>
            </value>
            <value>
              <caption type="Numeric">28</caption>
            </value>
            <value>
              <caption type="Numeric">29</caption>
            </value>
            <value>
              <caption type="Numeric">30</caption>
            </value>
            <value>
              <caption type="Numeric">41</caption>
            </value>
            <value>
              <caption type="Numeric">42</caption>
            </value>
            <value>
              <caption type="Numeric">43</caption>
            </value>
            <value>
              <caption type="Numeric">95</caption>
            </value>
            <value>
              <caption type="Numeric">542</caption>
            </value>
            <value>
              <caption type="Numeric">558</caption>
            </value>
            <value>
              <caption type="Numeric">4342</caption>
            </value>
            <value>
              <caption type="Numeric">4633</caption>
            </value>
            <value>
              <caption type="Numeric">96</caption>
            </value>
            <value>
              <caption type="Numeric">97</caption>
            </value>
            <value>
              <caption type="Numeric">98</caption>
            </value>
            <value>
              <caption type="Numeric">4214</caption>
            </value>
            <value>
              <caption type="Numeric">99</caption>
            </value>
            <value>
              <caption type="Numeric">523</caption>
            </value>
            <value>
              <caption type="Numeric">4177</caption>
            </value>
            <value>
              <caption type="Numeric">4186</caption>
            </value>
            <value>
              <caption type="Numeric">4192</caption>
            </value>
            <value>
              <caption type="Numeric">4195</caption>
            </value>
            <value>
              <caption type="Numeric">4205</caption>
            </value>
            <value>
              <caption type="Numeric">4309</caption>
            </value>
            <value>
              <caption type="Numeric">4207</caption>
            </value>
            <value>
              <caption type="Numeric">4209</caption>
            </value>
            <value>
              <caption type="Numeric">4221</caption>
            </value>
            <value>
              <caption type="Numeric">4232</caption>
            </value>
            <value>
              <caption type="Numeric">4233</caption>
            </value>
            <value>
              <caption type="Numeric">4237</caption>
            </value>
            <value>
              <caption type="Numeric">4340</caption>
            </value>
            <value>
              <caption type="Numeric">4324</caption>
            </value>
            <value>
              <caption type="Numeric">4377</caption>
            </value>
            <value>
              <caption type="Numeric">4538</caption>
            </value>
            <value>
              <caption type="Numeric">4580</caption>
            </value>
            <value>
              <caption type="Numeric">4582</caption>
            </value>
            <value>
              <caption type="Numeric">4628</caption>
            </value>
            <value>
              <caption type="Numeric">4630</caption>
            </value>
            <value>
              <caption type="Numeric">4703</caption>
            </value>
            <value>
              <caption type="Numeric">4777</caption>
            </value>
            <value>
              <caption type="Numeric">4779</caption>
            </value>
            <value>
              <caption type="Numeric">4398</caption>
            </value>
            <value>
              <caption type="Numeric">4442</caption>
            </value>
            <value>
              <caption type="Numeric">4546</caption>
            </value>
            <value>
              <caption type="Numeric">6000</caption>
            </value>
            <value>
              <caption type="Numeric">6002</caption>
            </value>
            <value>
              <caption type="Numeric">6102</caption>
            </value>
            <value>
              <caption type="Numeric">11923</caption>
            </value>
            <value>
              <caption type="Numeric">44</caption>
            </value>
            <value>
              <caption type="Numeric">45</caption>
            </value>
            <value>
              <caption type="Numeric">47</caption>
            </value>
            <value>
              <caption type="Numeric">48</caption>
            </value>
            <value>
              <caption type="Numeric">49</caption>
            </value>
            <value>
              <caption type="Numeric">50</caption>
            </value>
            <value>
              <caption type="Numeric">56</caption>
            </value>
            <value>
              <caption type="Numeric">57</caption>
            </value>
            <value>
              <caption type="Numeric">58</caption>
            </value>
            <value>
              <caption type="Numeric">62</caption>
            </value>
            <value>
              <caption type="Numeric">16</caption>
            </value>
            <value>
              <caption type="Numeric">17</caption>
            </value>
            <value>
              <caption type="Numeric">52</caption>
            </value>
            <value>
              <caption type="Numeric">53</caption>
            </value>
            <value>
              <caption type="Numeric">55</caption>
            </value>
            <value>
              <caption type="Numeric">59</caption>
            </value>
            <value>
              <caption type="Numeric">61</caption>
            </value>
            <value>
              <caption type="Numeric">63</caption>
            </value>
            <value>
              <caption type="Numeric">64</caption>
            </value>
            <value>
              <caption type="Numeric">67</caption>
            </value>
            <value>
              <caption type="Numeric">516</caption>
            </value>
            <value>
              <caption type="Numeric">525</caption>
            </value>
            <value>
              <caption type="Numeric">530</caption>
            </value>
            <value>
              <caption type="Numeric">548</caption>
            </value>
            <value>
              <caption type="Numeric">561</caption>
            </value>
            <value>
              <caption type="Numeric">564</caption>
            </value>
            <value>
              <caption type="Numeric">4182</caption>
            </value>
            <value>
              <caption type="Numeric">4235</caption>
            </value>
            <value>
              <caption type="Numeric">4289</caption>
            </value>
            <value>
              <caption type="Numeric">4345</caption>
            </value>
            <value>
              <caption type="Numeric">11921</caption>
            </value>
          </constantOperand>
        </comparisonFilter>
        <comparisonFilter id="_gHuCgff-EeWXF8Zx0hs-1w" operator="NotInList" path="InfoObjects\ParentId">
          <constantOperand searchPattern="false">
            <value>
              <caption type="String">65</caption>
            </value>
            <value>
              <caption type="String">66</caption>
            </value>
            <value>
              <caption type="String">24</caption>
            </value>
            <value>
              <caption type="String">46</caption>
            </value>
            <value>
              <caption type="String">60</caption>
            </value>
            <value>
              <caption type="String">549</caption>
            </value>
          </constantOperand>
        </comparisonFilter>
      </and>
    </filterPart>
  </queryData>
</querySpecification>

	filter: list_of_ancestor {
		label: "List of Ancestor"
		description: ""
		type: string
	}

	parameter: infoobject_serverkind_parameter {
		label: "InfoObject ServerKind Parameter"
		description: ""
		type: string
		allowed_value: {
			value: "webiserver"
		}
	}
	parameter: infoobject_kind_parameter {
		label: "InfoObject Kind Parameter"
		description: ""
		type: string
		allowed_value: {
			value: "Webi"
		}
	}
}
