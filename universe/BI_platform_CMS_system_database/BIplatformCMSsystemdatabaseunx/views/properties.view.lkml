# View file auto-generated with booker
# 2021-06-09 17:43:58.509166

view: properties {

	sql_table_name: "Properties" ;;

	dimension: cuid {
		label: "CUID"
		description: ""
		type: string
		sql: ${TABLE}.si_cuid ;;
	}
	# FIXME: int(Properties.si_id) did not match any rules for conversion
	dimension: id {
		label: "Id"
		description: ""
		type: number
		sql: int(${TABLE}.si_id) ;;
	}
	dimension: name {
		label: "Name"
		description: ""
		type: string
		sql: ${TABLE}.si_name ;;
	}
	dimension: kind {
		label: "Kind"
		description: ""
		type: string
		sql: ${TABLE}.si_kind ;;
	}
	dimension: specifickind {
		label: "SpecificKind"
		description: ""
		type: string
		sql: ${TABLE}.si_specific_kind ;;
	}
	# FIXME: int(Properties.si_parentid) did not match any rules for conversion
	dimension: parentid {
		label: "ParentId"
		description: ""
		type: number
		sql: int(${TABLE}.si_parentid) ;;
	}
	dimension: parentcuid {
		label: "ParentCUID"
		description: ""
		type: string
		sql: ${TABLE}.si_parent_cuid ;;
	}
	dimension: owner {
		label: "Owner"
		description: "Owner of the InfoObject"
		type: string
		sql: ${TABLE}.si_owner ;;
	}
	# FIXME: flag(Properties.si_runnable_object) did not match any rules for conversion
	dimension: is_runnable_object {
		label: "Is Runnable Object"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_runnable_object) ;;
	}
	# FIXME: flag(Properties.SI_SYSTEM_OBJECT) did not match any rules for conversion
	dimension: is_system_object {
		label: "Is System Object"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_SYSTEM_OBJECT) ;;
	}
	# FIXME: flag(Properties.SI_DEFAULT_OBJECT) did not match any rules for conversion
	dimension: is_default_object {
		label: "Is Default Object"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_DEFAULT_OBJECT) ;;
	}
	# FIXME: folderPath(Properties.si_path) did not match any rules for conversion
	dimension: folderpath {
		label: "FolderPath"
		description: ""
		type: string
		sql: folderPath(${TABLE}.si_path) ;;
	}
	# FIXME: LONG_TEXT did not match any rules for conversion
	# FIXME: text(Properties.si_name,"50") did not match any rules for conversion
	dimension: name_longtext {
		label: "Name LongText"
		description: ""
		type: LONG_TEXT
		sql: text(${TABLE}.si_name,50) ;;
	}
	dimension: user_fullname {
		label: "User Fullname"
		description: ""
		type: string
		sql: ${TABLE}.si_userfullname ;;
	}
	# FIXME: flag(Properties.si_nameduser) did not match any rules for conversion
	dimension: is_named_user {
		label: "Is Named User"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_nameduser) ;;
	}
	# FIXME: flag(Properties.si_passwordexpire) did not match any rules for conversion
	dimension: is_password_expire {
		label: "Is Password Expire"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_passwordexpire) ;;
	}
	# FIXME: flag(Properties.PasswordNeverExpires) did not match any rules for conversion
	dimension: password_never_expires {
		label: "Password Never Expires"
		description: ""
		type: yesno
		sql: flag(${TABLE}.PasswordNeverExpires) ;;
	}
	# FIXME: flag(Properties.SI_FORCE_PASSWORD_CHANGE) did not match any rules for conversion
	dimension: force_password_change {
		label: "Force Password Change"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_FORCE_PASSWORD_CHANGE) ;;
	}
	# FIXME: flag(Properties.SI_CHANGEPASSWORD) did not match any rules for conversion
	dimension: change_password {
		label: "Change Password"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CHANGEPASSWORD) ;;
	}
	dimension: email {
		label: "EMail"
		description: ""
		type: string
		sql: ${TABLE}.SI_EMAIL_ADDRESS ;;
	}
	# FIXME: ValueSecurityInfo(Properties.principal) did not match any rules for conversion
	dimension: principalname {
		label: "PrincipalName"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principal) ;;
	}
	# FIXME: ValueSecurityInfo(Properties.principalKind) did not match any rules for conversion
	dimension: principalkind {
		label: "PrincipalKind"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.principalKind) ;;
	}
	# FIXME: ValueSecurityInfo(Properties.access) did not match any rules for conversion
	dimension: access {
		label: "Access"
		description: ""
		type: string
		sql: ValueSecurityInfo(${TABLE}.access) ;;
	}
	dimension: serverkind {
		label: "ServerKind"
		description: ""
		type: string
		sql: ${TABLE}.si_server_kind ;;
	}
	dimension: description {
		label: "Description"
		description: ""
		type: string
		sql: ${TABLE}.si_description ;;
	}
	# FIXME: Text(Properties.SI_CURRENT_COMMAND_LINE,"500") did not match any rules for conversion
	dimension: current_command_line {
		label: "Current Command Line"
		description: ""
		type: string
		sql: Text(${TABLE}.SI_CURRENT_COMMAND_LINE,500) ;;
	}
	# FIXME: flag(Properties.si_disabled_target) did not match any rules for conversion
	dimension: disabled_target {
		label: "Disabled target"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_disabled_target) ;;
	}
	# FIXME: flag(Properties.SI_CURRENT_DISABLED_STATE) did not match any rules for conversion
	dimension: current_disabled_state {
		label: "Current Disabled State"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_CURRENT_DISABLED_STATE) ;;
	}
	# FIXME: int(Properties.si_server_is_alive) did not match any rules for conversion
	dimension: is_alive {
		label: "Is Alive"
		description: ""
		type: number
		sql: int(${TABLE}.si_server_is_alive) ;;
	}
	# FIXME: int(Properties.SI_EXPECTED_RUN_STATE) did not match any rules for conversion
	dimension: expected_run_state {
		label: "Expected Run State"
		description: ""
		type: number
		sql: int(${TABLE}.SI_EXPECTED_RUN_STATE) ;;
	}
	# FIXME: text(Properties.si_webi_doc_properties,"500") did not match any rules for conversion
	dimension: webi_properties {
		label: "Webi_Properties"
		description: ""
		type: string
		sql: text(${TABLE}.si_webi_doc_properties,500) ;;
	}
	# FIXME: flag(Properties.SI_INSTANCE) did not match any rules for conversion
	dimension: scheduling_instance {
		label: "Scheduling Instance"
		description: ""
		type: yesno
		sql: flag(${TABLE}.SI_INSTANCE) ;;
	}
	# FIXME: int(Properties.si_children) did not match any rules for conversion
	dimension: number_of_children {
		label: "Number of Children"
		description: ""
		type: number
		sql: int(${TABLE}.si_children) ;;
	}
	dimension: template_cuid {
		label: "Template_CUID"
		description: ""
		type: string
		sql: ${TABLE}.si_template_cuid ;;
	}
	# FIXME: int(Properties.si_new_job_id) did not match any rules for conversion
	dimension: new_job_id {
		label: "New Job Id"
		description: ""
		type: number
		sql: int(${TABLE}.si_new_job_id) ;;
	}
	# FIXME: flag(Properties.Is_Recurring) did not match any rules for conversion
	dimension: is_recurring {
		label: "Is Recurring"
		description: ""
		type: yesno
		sql: flag(${TABLE}.Is_Recurring) ;;
	}
	# FIXME: int(Properties.SI_SCHEDULE_STATUS) did not match any rules for conversion
	dimension: schedule_status {
		label: "Schedule Status"
		description: ""
		type: number
		sql: int(${TABLE}.SI_SCHEDULE_STATUS) ;;
	}
	# FIXME: int(Properties.si_scheduleinfo.SI_SCHEDULE_TYPE) did not match any rules for conversion
	dimension: schedule_type {
		label: "Schedule Type"
		description: ""
		type: number
		sql: int(${TABLE}.si_scheduleinfo.SI_SCHEDULE_TYPE) ;;
	}
	# FIXME: flag(Properties.si_scheduleinfo.si_has_prompts) did not match any rules for conversion
	dimension: processinginfo_hasprompts {
		label: "ProcessingInfo_HasPrompts"
		description: ""
		type: yesno
		sql: flag(${TABLE}.si_scheduleinfo.si_has_prompts) ;;
	}
	# FIXME: int(Properties.si_scheduleinfo.si_objid) did not match any rules for conversion
	dimension: processinginfo_objectid {
		label: "ProcessingInfo_ObjectId"
		description: ""
		type: number
		sql: int(${TABLE}.si_scheduleinfo.si_objid) ;;
	}
	# FIXME: int(Properties.SI_UISTATUS) did not match any rules for conversion
	dimension: publication_status {
		label: "Publication Status"
		description: ""
		type: number
		sql: int(${TABLE}.SI_UISTATUS) ;;
	}
	# FIXME: int(Properties.SI_NUM_SUCCEEDED_RECIPIENTS) did not match any rules for conversion
	dimension: succeeded_recipients {
		label: "Succeeded Recipients"
		description: ""
		type: number
		sql: int(${TABLE}.SI_NUM_SUCCEEDED_RECIPIENTS) ;;
	}
	# FIXME: int(Properties.si_ancestor) did not match any rules for conversion
	dimension: ancestor {
		label: "Ancestor"
		description: ""
		type: number
		sql: int(${TABLE}.si_ancestor) ;;
	}
	# FIXME: Descendants(Properties.si_name) did not match any rules for conversion
	dimension: descendants {
		label: "Descendants"
		description: ""
		type: string
		sql: Descendants(${TABLE}.si_name) ;;
	}
	# FIXME: DescendantsValue(Properties.si_name) did not match any rules for conversion
	dimension: descendantsvalue {
		label: "DescendantsValue"
		description: ""
		type: string
		sql: DescendantsValue(${TABLE}.si_name) ;;
	}
	# FIXME: relationship(Properties.si_parent_folder) did not match any rules for conversion
	dimension: parentfolder {
		label: "ParentFolder"
		description: ""
		type: number
		sql: relationship(${TABLE}.si_parent_folder) ;;
	}
	# FIXME: relationships(Properties.SI_CORPORATE_CATEGORIES) did not match any rules for conversion
	dimension: tocorporatecategory {
		label: "To-CorporateCategory"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CORPORATE_CATEGORIES) ;;
	}
	# FIXME: relationships(Properties.SI_PERSONAL_CATEGORIES) did not match any rules for conversion
	dimension: topersonalcategory {
		label: "To-PersonalCategory"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONAL_CATEGORIES) ;;
	}
	# FIXME: relationships(Properties.SI_DOCUMENTS) did not match any rules for conversion
	dimension: todoc {
		label: "To-Doc"
		description: "Corporate Category, Personal Category"
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS) ;;
	}
	# FIXME: relationships(Properties.SI_CONNUNIVERSE) did not match any rules for conversion
	dimension: contounv {
		label: "Con-To-Unv"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_CONNUNIVERSE) ;;
	}
	# FIXME: relationships(Properties.si_webi) did not match any rules for conversion
	dimension: unvtowebi {
		label: "Unv-To-Webi"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_webi) ;;
	}
	# FIXME: relationships(Properties.si_dataconnection) did not match any rules for conversion
	dimension: unvtocnx {
		label: "Unv-To-Cnx"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_dataconnection) ;;
	}
	# FIXME: relationships(Properties.si_sl_documents) did not match any rules for conversion
	dimension: unxtowebi {
		label: "Unx-To-Webi"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_sl_documents) ;;
	}
	# FIXME: relationships(Properties.SI_SL_UNIVERSE_TO_CONNECTIONS) did not match any rules for conversion
	dimension: unxtocnx {
		label: "Unx-To-Cnx"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SL_UNIVERSE_TO_CONNECTIONS) ;;
	}
	# FIXME: relationships(Properties.SI_UNIVERSE) did not match any rules for conversion
	dimension: webitounv {
		label: "Webi-To-Unv"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_UNIVERSE) ;;
	}
	# FIXME: relationships(Properties.SI_DSL_UNIVERSE) did not match any rules for conversion
	dimension: webitounx {
		label: "Webi-To-Unx"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DSL_UNIVERSE) ;;
	}
	# FIXME: relationships(Properties.si_doc_common_connection) did not match any rules for conversion
	dimension: webitoolapcnx {
		label: "Webi-To-OLAP-Cnx"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_doc_common_connection) ;;
	}
	# FIXME: relationships(Properties.SI_EXCEL_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitoexcel {
		label: "Webi-To-Excel"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_EXCEL_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Properties.SI_SHARED_DOC) did not match any rules for conversion
	dimension: webitoshareddoc {
		label: "Webi-To-SharedDoc"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHARED_DOC) ;;
	}
	# FIXME: relationships(Properties.SI_SHAREDELEMENTS_OF_DOCUMENT) did not match any rules for conversion
	dimension: webitosharedelementsofdoc {
		label: "Webi-To-SharedElementsOfDoc"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_SHAREDELEMENTS_OF_DOCUMENT) ;;
	}
	# FIXME: relationships(Properties.SI_REFERRING_DOCS) did not match any rules for conversion
	dimension: webitoreferringdoc {
		label: "Webi-To-ReferringDoc"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_REFERRING_DOCS) ;;
	}
	# FIXME: relationships(Properties.SI_DOCUMENTS_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodoc {
		label: "SharedElement-To-Doc"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DOCUMENTS_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Properties.SI_DATASOURCES_OF_SHAREDELEMENT) did not match any rules for conversion
	dimension: sharedelementtodatasource {
		label: "SharedElement-To-Datasource"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_DATASOURCES_OF_SHAREDELEMENT) ;;
	}
	# FIXME: relationships(Properties.si_scheduleinfo.si_dependants) did not match any rules for conversion
	dimension: scheduletodependants {
		label: "Schedule-To-Dependants"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_scheduleinfo.si_dependants) ;;
	}
	# FIXME: relationships(Properties.si_scheduleinfo.SI_DEPENDENCIES) did not match any rules for conversion
	dimension: scheduletodependencies {
		label: "Schedule-To-Dependencies"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_scheduleinfo.SI_DEPENDENCIES) ;;
	}
	# FIXME: relationships(Properties.SI_PERSONALCATEGORY_USER) did not match any rules for conversion
	dimension: personalcategorytouser {
		label: "PersonalCategory-To-User"
		description: ""
		type: number
		sql: relationships(${TABLE}.SI_PERSONALCATEGORY_USER) ;;
	}
	# FIXME: relationships(Properties.si_principals) did not match any rules for conversion
	dimension: publicationtoprincipals {
		label: "Publication-To-Principals"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_principals) ;;
	}
	# FIXME: relationships(Properties.si_publication_documents) did not match any rules for conversion
	dimension: publicationtodocuments {
		label: "Publication-To-Documents"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_publication_documents) ;;
	}
	# FIXME: relationships(Properties.si_usergroups) did not match any rules for conversion
	dimension: usertogroups {
		label: "User-To-Groups"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_usergroups) ;;
	}
	# FIXME: relationships(Properties.si_group_members) did not match any rules for conversion
	dimension: grouptomembers {
		label: "Group-To-Members"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_group_members) ;;
	}
	# FIXME: relationships(Properties.si_connuniverse) did not match any rules for conversion
	dimension: relcnxtounv {
		label: "Rel-Cnx-To-Unv"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_connuniverse) ;;
	}
	# FIXME: relationships(Properties.si_document) did not match any rules for conversion
	dimension: olapcnxtowebi {
		label: "OLAP-Cnx-To-Webi"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_document) ;;
	}
	# FIXME: relationships(Properties.si_documents) did not match any rules for conversion
	dimension: categorytodocuments {
		label: "Category-To-Documents"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_documents) ;;
	}
	# FIXME: relationships(Properties.si_webi) did not match any rules for conversion
	dimension: hasunxtowebi {
		label: "Has-Unx-To-Webi"
		description: ""
		type: number
		sql: relationships(${TABLE}.si_webi) WHERE ${TABLE}.si_kind = 'Universe' ;;
	}
	# FIXME: PropertyValue(Properties.technical) did not match any rules for conversion
	dimension: propertyvalue {
		label: "PropertyValue"
		description: ""
		type: string
		sql: PropertyValue(${TABLE}.technical) ;;
	}
	# FIXME: LONG_TEXT did not match any rules for conversion
	# FIXME: PropertyValueText(Properties.technical,"300") did not match any rules for conversion
	dimension: propertyvaluelongtext {
		label: "PropertyValueLongText"
		description: ""
		type: LONG_TEXT
		sql: PropertyValueText(${TABLE}.technical,300) ;;
	}
	# FIXME: PropertyLevel(Properties.technical) did not match any rules for conversion
	dimension: propertylevel {
		label: "PropertyLevel"
		description: ""
		type: number
		sql: PropertyLevel(${TABLE}.technical) ;;
	}
	# FIXME: LONG_TEXT did not match any rules for conversion
	# FIXME: PropertyPath(Properties.technical) did not match any rules for conversion
	dimension: propertypath {
		label: "PropertyPath"
		description: ""
		type: LONG_TEXT
		sql: PropertyPath(${TABLE}.technical) ;;
	}
	# FIXME: LONG_TEXT did not match any rules for conversion
	# FIXME: Property(Properties.technical) did not match any rules for conversion
	dimension: propertyname {
		label: "PropertyName"
		description: ""
		type: LONG_TEXT
		sql: Property(${TABLE}.technical) ;;
	}
	# FIXME: PropertyIsContainer(Properties.technical) did not match any rules for conversion
	dimension: propertyiscontainer {
		label: "PropertyIsContainer"
		description: ""
		type: yesno
		sql: PropertyIsContainer(${TABLE}.technical) ;;
	}

	# FIXME: datetime(Properties.si_creation_time) did not match any rules for conversion
	dimension_group: creation_timestamp {
		label: "Creation Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datePart(Properties.si_creation_time) did not match any rules for conversion
	dimension_group: creation_date {
		label: "Creation Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_creation_time) ;;
	}
	# FIXME: datetime(Properties.si_update_ts) did not match any rules for conversion
	dimension_group: update_timestamp {
		label: "Update Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_update_ts) ;;
	}
	# FIXME: datetime(Properties.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_timestamp {
		label: "LastLogon Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datePart(Properties.si_lastlogontime) did not match any rules for conversion
	dimension_group: lastlogon_date {
		label: "LastLogon Date"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datePart(${TABLE}.si_lastlogontime) ;;
	}
	# FIXME: datetime(Properties.SI_EXPECTED_RUN_STATE_TS) did not match any rules for conversion
	dimension_group: expected_run_state_timestamp {
		label: "Expected Run State Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_EXPECTED_RUN_STATE_TS) ;;
	}
	# FIXME: datetime(Properties.SI_NEXTRUNTIME) did not match any rules for conversion
	dimension_group: next_runtime_timestamp {
		label: "Next Runtime Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.SI_NEXTRUNTIME) ;;
	}
	# FIXME: datetime(Properties.si_scheduleinfo.si_endtime) did not match any rules for conversion
	dimension_group: end_timestamp {
		label: "End Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_scheduleinfo.si_endtime) ;;
	}
	# FIXME: datetime(Properties.si_scheduleinfo.si_starttime) did not match any rules for conversion
	dimension_group: start_timestamp {
		label: "Start Timestamp"
		description: ""
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: yyyymmdd
		sql: datetime(${TABLE}.si_scheduleinfo.si_starttime) ;;
	}

	# FIXME: mint(Properties.si_size) did not match any rules for conversion
	measure: size {
		description: "Size of the document"
		type: sum
		sql: mint(${TABLE}.si_size) ;;
	}


}
