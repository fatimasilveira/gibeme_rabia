# View file auto-generated with booker
# 2021-06-09 17:54:51.219448

view: salesciti {

	sql_table_name: "Sales - CITI" ;;

	# FIXME: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD field contains double dot
	dimension: fund_code_key {
		label: "Fund Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD ;;
	}
	# FIXME: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD field contains double dot
	dimension: series_code_key {
		label: "Series Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD ;;
	}

	dimension_group: process_date {
		label: "Process Date"
		description: "Process Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: datetime
		sql: ${TABLE}.ACITPRCDAT ;;
	}

	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Fees]']
	measure: fees {
		description: "Fees"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB91NAGNW ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Fees - CAD]']
	measure: fees_cad {
		description: "Fees - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB91DE124 ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Fees - USD]']
	measure: fees_usd {
		description: "Fees - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB91I9M1O ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Gross Purchases]']
	measure: gross_purchases {
		description: "Gross Purchases"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB90SF0T8 ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Gross Purchases - CAD]']
	measure: gross_purchases_cad {
		description: "Gross Purchases - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB90IUVOC ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Gross Purchases - USD]']
	measure: gross_purchases_usd {
		description: "Gross Purchases - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB90NJFTO ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Redemptions]']
	measure: redemptions {
		description: "Redemptions"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB916IGGS ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Redemptions - CAD]']
	measure: redemptions_cad {
		description: "Redemptions - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB90X3KYK ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - CITI].[Redemptions - USD]']
	measure: redemptions_usd {
		description: "Redemptions - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8XHB911S53W ;;
	}


}
