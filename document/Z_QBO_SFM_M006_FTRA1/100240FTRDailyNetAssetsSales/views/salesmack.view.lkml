# View file auto-generated with booker
# 2021-06-09 17:54:50.884253

view: salesmack {

	sql_table_name: "Sales - MACK" ;;

	# FIXME: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD field contains double dot
	dimension: fund_code_key {
		label: "Fund Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD ;;
	}
	# FIXME: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD field contains double dot
	dimension: series_code_key {
		label: "Series Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD ;;
	}

	dimension_group: reporting_date {
		label: "Reporting Date"
		description: "Reporting Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: datetime
		sql: ${TABLE}.A0CALDAY ;;
	}

	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Fees]']
	measure: fees {
		description: "Fees"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS8MEJ5Z ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Fees - CAD]']
	measure: fees_cad {
		description: "Fees - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS8CND6V ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Fees - USD]']
	measure: fees_usd {
		description: "Fees - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS8HFFRB ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Gross Purchases]']
	measure: gross_purchases {
		description: "Gross Purchases"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS7SZRLZ ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Gross Purchases - CAD]']
	measure: gross_purchases_cad {
		description: "Gross Purchases - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS7JOEIV ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Gross Purchases - USD]']
	measure: gross_purchases_usd {
		description: "Gross Purchases - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS7OCYO7 ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Redemptions]']
	measure: redemptions {
		description: "Redemptions"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS87379J ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Redemptions - CAD]']
	measure: redemptions_cad {
		description: "Redemptions - CAD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS7XOBRB ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - MACK].[Redemptions - USD]']
	measure: redemptions_usd {
		description: "Redemptions - USD"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8WLQS82EN47 ;;
	}


}
