# View file auto-generated with booker
# 2021-06-09 17:54:50.712023

view: salesmoneymarket {

	sql_table_name: "Sales - Money Market" ;;

	# FIXME: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD field contains double dot
	dimension: fund_code_key {
		label: "Fund Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD ;;
	}
	# FIXME: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD field contains double dot
	dimension: series_code_key {
		label: "Series Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD ;;
	}

	dimension_group: reporting_date {
		label: "Reporting Date"
		description: "Reporting Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: datetime
		sql: ${TABLE}.A0CALDAY ;;
	}

	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - Money Market].[Fees]']
	measure: fees {
		description: "Fees"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8SSLFCU5C2P ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Gross Sales]']
	measure: gross_sales {
		description: "Gross Sales"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8SSLFCKOPCX ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Net Sales]']
	measure: net_sales {
		description: "Net Sales"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G39KN1PGT3NE5T7 ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[Sales - Money Market].[Redemptions]']
	measure: redemptions {
		description: "Redemptions"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G1JA8SSLFCPD9I9 ;;
	}


}
