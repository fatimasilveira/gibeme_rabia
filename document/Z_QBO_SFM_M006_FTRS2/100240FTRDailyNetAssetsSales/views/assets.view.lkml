# View file auto-generated with booker
# 2021-06-09 17:54:50.714120

view: assets {

	sql_table_name: "Assets" ;;

	# FIXME: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD field contains double dot
	dimension: fund_code_key {
		label: "Fund Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRFNDCD.2ZMPRFNDCD ;;
	}
	# FIXME: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD field contains double dot
	dimension: series_code_key {
		label: "Series Code - Key"
		description: "Key"
		type: string
		sql: ${TABLE}.PAZMPRSRCD.2ZMPRSRCD ;;
	}

	dimension_group: reporting_date {
		label: "Reporting Date"
		description: "Reporting Date"
		type: time
		timeframes: [raw, time, date, week, month, quarter, year, day_of_week, month_name]
		datatype: datetime
		sql: ${TABLE}.A0CALDAY ;;
	}

	# FIXME: extra tables present, likely due joins. Tables: ['[NAV]']
	measure: nav {
		description: "NAV"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G39KN5G16CZ5K2Z ;;
	}
	# FIXME: extra tables present, likely due joins. Tables: ['[SEG]']
	measure: seg {
		description: "SEG"
		type: sum
		sql: ${TABLE}.M7GJZLT4H93G39KN5G16D3U48B ;;
	}


}
