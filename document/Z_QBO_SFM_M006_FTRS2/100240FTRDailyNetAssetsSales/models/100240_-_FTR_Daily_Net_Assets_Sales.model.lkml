# Model file auto-generated with booker
# 2021-06-09 17:54:50.720140
connection: "Z_QBO_SFM_M006_FTRS2"

# include universe views only
include: "/document/Z_QBO_SFM_M006_FTRS2/100240FTRDailyNetAssetsSales/views/*.view"
include: "/dashboards/**/*.dashboard"


explore: salesmack {

	label: "Sales - MACK"
	view_label: "Sales - MACK"

}
explore: salesciti {

	label: "Sales - CITI"
	view_label: "Sales - CITI"

}
explore: salesmoneymarket {

	label: "Sales - Money Market"
	view_label: "Sales - Money Market"

}
explore: assets {

	label: "Assets"
	view_label: "Assets"

}
